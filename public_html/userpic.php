<?php

//echo "AAAA";
$id = (int)$_REQUEST['id'];
if (array_key_exists('w', $_REQUEST) && $_REQUEST['w'] > 0)
    $width = $_REQUEST['w'];
else
    $width = 100;

if (array_key_exists('h', $_REQUEST) && $_REQUEST['h'] > 0)
    $height = $_REQUEST['h'];
else
    $height = 100;

if (function_exists("apache_getenv")) {
    $config_file = apache_getenv('DTL_CONFIG_FILE');
    $config_file_type = apache_getenv('DTL_CONFIG_FILE_TYPE'); // one of 'php' or 'conf'
} else {
    $config_file = $_SERVER["DTL_CONFIG_FILE"];
    if (array_key_exists('DTL_CONFIG_FILE_TYPE', $_SERVER)) {
        $config_file_type = $_SERVER["DTL_CONFIG_FILE_TYPE"];
    } else {
        $config_file_type = 'conf';
    }
}
require_once("../../libdtl/libphp/config.php");

settings::init($config_file);
$libdtl_root = settings::get_value("libdtl_root", "PATHS");
$libpath = "$libdtl_root/libphp";

require_once("$libpath/system.php");
require_once("$libpath/site.php");
require_once("$libpath/utils/image.php");

system::init($config_file);

$blob = New Blob();
$blob->get($id);
//echo $blob->mimetype;
header('Content-Type: '.$blob->mimetype);

$img = image::resize_from_string($blob->data, $width, $height, TRUE);

// Output
imagejpeg($img, null, 100);