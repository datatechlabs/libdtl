<?php

$uploads_dir = "/var/lib/dtl_www/photos/";

$id = $_REQUEST['id'];
$sz = $_REQUEST['size'];
$size = preg_replace("/[^A-Za-z]/", '', $sz);

if(!$id)
    $id = 0;

$image = $uploads_dir."/".$size."/".$id;

if (!file_exists($image))
    return false;

$finfo = pathinfo($image);
$content = file_get_contents($image);

if (strtolower($finfo['extension']) == 'jpg' || strtolower($finfo['extension']) == 'jpeg')
    header('Content-Type: image/jpeg');
elseif (strtolower($finfo['extension']) == 'png')
    header('Content-Type: image/png');
echo $content;