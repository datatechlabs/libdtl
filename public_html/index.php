<?php


if (function_exists("apache_getenv")) {
	$config_file = apache_getenv('DTL_CONFIG_FILE');
	$config_file_type = apache_getenv('DTL_CONFIG_FILE_TYPE'); // one of 'php' or 'conf'
} else {
	$config_file = $_SERVER["DTL_CONFIG_FILE"];
	if (array_key_exists('DTL_CONFIG_FILE_TYPE', $_SERVER)) {
		$config_file_type = $_SERVER["DTL_CONFIG_FILE_TYPE"];
	} else {
		$config_file_type = 'conf';
	}
}

if (file_exists("../../libdtl/libphp/config.php")) {
	require_once("../../libdtl/libphp/config.php");
} elseif (file_exists("../../../libdtl/libphp/config.php")) {
	require_once("../../../libdtl/libphp/config.php");
} elseif (file_exists("../vendor/datatechlabs/libdtl/libphp/config.php")) {
	require_once ("../vendor/datatechlabs/libdtl/libphp/config.php");
} elseif (file_exists("../../vendor/datatechlabs/libdtl/libphp/config.php")) {
	require_once ("../../vendor/datatechlabs/libdtl/libphp/config.php");
}

settings::init($config_file, $config_file_type);
$libdtl_root = settings::get_value("libdtl_root", "PATHS");

$libpath = "$libdtl_root/libphp";

require_once("$libpath/system.php");
require_once("$libpath/site.php");

//require_once("$libpath/hosted.php");
//require_once("$libpath/routes.php");

system::init($config_file);

require_once("$libpath/user.php");

settings::set_value(common::get($_REQUEST['area']), 'area', 'MISC');
settings::set_value(common::get($_REQUEST['script']), 'pagename', 'MISC');
settings::set_value(common::get($_REQUEST['site']), 'site', 'MISC');

sessions::delete_old();
processOptions();

if (login_attempt() || !sessions::valid()) {

	if (login_attempt()) {
		/* try to log in */

		$auth = user::login();

		/* check if login succeeded */
		if ($auth == false) {
			$msg = "Invalid user name and/or password.";
			
			/* Log bad login attempt. */
			$accesslog = New AccessLog();
			$msg = 'Unsuccessful Login Attempt with username: '.$_POST['user_login'];
			$accesslog->append('sec', 'badlogin', $msg, false);
			$ipaddr = common::get_ip();
			common::ndebug(2, 'BADLOGIN: Failed login attempt from '.$ipaddr.' to admin area');
			http_response_code(401);
			exit(1);
		} else {
			header('Content-Type: application/json');
			$data =
				array(
					'result' => 0,
					'data' => array("area" => $auth[0], "authToken" => $auth[1]),
				);
			print json_encode($data);
			exit(0);
		}
	}

	/* show a public page */
	site::init();
	require("$libpath/public.php");
	$pagename = settings::get_value('pagename', 'MISC', '');
	if ($pagename == "/login") {

		echo site::login_page('Please log in');
	} elseif ($pagename == '/ping') {
		http_response_code(401);
		exit(1);
	} else {
		settings::set_value('public', 'area', 'MISC');
		list($result, $res_data) = find_controller();
		if (!$result) {
			reject($res_data);
			exit(1);
		}
		list($controller, $method, $ctrl_args) = $res_data;
		common::ndebug(4,"Public page. Calling controller %s method %s with args %s", $controller, $method, $ctrl_args);
		call_user_func_array( array( $controller, $method ) , $ctrl_args);

		/*
         * Generate main site and dump everything to the browser
         */
		echo site::generate($scriptout);
		exit(0);

	}
	
	exit(0);
}


/*
 * session in progress, retrieve user data
 */
if (!user::login_from_session()) {
	sessions::delete();
	exit(1);
}

site::init();
sessions::update();	/* update session last-modification time */

if (Input::get('request') == 'init') {
	site::init_client();
	exit(0);
}

list($result, $res_data) = find_controller();
if (!$result) {
	reject($res_data);
}

list($controller, $method, $ctrl_args) = $res_data;

common::ndebug(4,"Calling controller %s method %s with args %s", $controller, $method, $ctrl_args);
call_user_func_array( array( $controller, $method ) , $ctrl_args);

/*
 * Generate main site and dump everything to the browser
 */
echo site::generate($scriptout);
exit(0);


/*
 * did someone try to log in?
 * checks whether login variables have been posted
 * returns: true if they have been, false if not
 */
function login_attempt()
{
	if (array_key_exists('user_login', $_POST) &&
	    array_key_exists('user_pwd', $_POST)) {
		return true;
	}

	return false;
}


function find_controller() {
	list($result, $data) = site::pagescript_path();
	if (!$result)
		return array(false, $data);


	list($scriptfile, $ctrlclass, $id, $sub_ids, $method) = $data;

	if (is_file($scriptfile)) {

		$package_root = settings::get_simple_path('package_root');
		$libdtl_root = settings::get_simple_path('libdtl_root');

		/* include appropriate script here */
		if(!require($scriptfile)) {
			common::ndebug(2,"File %s cannot be included!", $scriptfile);
			return array(false, 500);
		}
		if (!class_exists($ctrlclass)) {
			common::ndebug(2,"Class %s does not exist!", $ctrlclass);
			return array(false, 500);
		}

		$controller = New $ctrlclass();
		$method = $controller->route($id, $method);
		if (!method_exists($controller, $method)) {
			common::ndebug(2, "Method %s does not exist for %s", $method, $controller);
			return array(false, 500);
		}
		$request = array();
		if (array_key_exists('HTTP_CONTENT_TYPE', $_SERVER) && $_SERVER['HTTP_CONTENT_TYPE'] === 'application/json') {
			$json = file_get_contents('php://input');
			$res = json_decode($json, true);
			foreach($res as $k => $v) {
				$request[$k] = $v;
			}
		} 
		foreach ($_REQUEST as $rkey => $rval) {
			$request[$rkey] = $rval;
		}
		foreach ($_FILES as $fkey => $fval) {
			$request[$fkey] = $fval;
		}

		$ctrl_args = array($request);

		if ($id) {
			array_push($ctrl_args, $id);
		}

		if (is_array($sub_ids)) {
			foreach ($sub_ids as $id) {
				array_push($ctrl_args, $id);
			}
		}
		$data = array($controller, $method, $ctrl_args);
		return array(true, $data);
	} else {
		$area = settings::get_value('area', 'MISC');
		common::ndebug(2,"No route found for request %s area %s. Scriptfile %s", $_GET['script'], $area, $scriptfile);
		return array(false, 404);
	}
}

function processOptions() {
	if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
		header('Access-Control-Allow-Origin: *'); 
		header('Access-Control-Allow-Headers: Content-Type,Authorization,cache-control,x-requested-with'); 
		header('Access-Control-Allow-Methods: OPTIONS, GET, PUT, POST, DELETE'); 
		header('Allow: OPTIONS, GET, PUT, POST, DELETE');
		exit(0);  
	}
}

function reject($code) {
	header('Access-Control-Allow-Origin: *'); 
	http_response_code($code);
	exit(1);
}
