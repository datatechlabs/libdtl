alter table Users add column fname varchar(80) not null default '' after email_addr;
alter table Users add column lname varchar(80) not null default '' after fname;
alter table Users add column updated_at datetime default null;
alter table Users add column picture_id int(11) default NULL after language;

CREATE TABLE `Blobs` (
  `blob_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `internal_id` int(11) DEFAULT NULL,
  `mimetype` varchar(40) DEFAULT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blob_id`)
) ENGINE=InnoDB;

CREATE TABLE `BlobData` (
  `blob_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `blob_id` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`blob_data_id`)
) ENGINE=InnoDB;