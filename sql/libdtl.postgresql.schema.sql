CREATE TABLE IF NOT EXISTS PermissionGroups (
  permission_group_id serial primary key,
  name varchar(40) not null default ''
);


CREATE TABLE IF NOT EXISTS Users (
  user_id serial primary key,
  web_login varchar(80) not null default '', 
  web_passwd varchar(80) not null default '',
  email_addr varchar(80) not null default '',
  language char(2) not null default 'en', 
  status smallint not null, 
  area varchar(10) not null default '',
  permission_group_id integer REFERENCES PermissionGroups (permission_group_id),
  timezone varchar(40) not null default 'GMT', 
  created_at timestamp not null
);


CREATE TABLE IF NOT EXISTS WebSessions (
  session_id varchar(64) not null default '' unique,
  user_id integer REFERENCES Users (user_id),
  ip_address inet not null,
  create_time timestamp not null,
  last_access_time timestamp not null
);

CREATE TABLE IF NOT EXISTS GlobalParams (
  name varchar(64) not null default '',
  value varchar(200) not null default '',
  section varchar (64) default null,
  UNIQUE (name, section)
);

CREATE TABLE IF NOT EXISTS AccessLog (
  access_id serial primary key,
  type varchar(80) not null default '',
  user_id integer REFERENCES Users (user_id),
  area varchar(16) not null default '',
  ip_address inet not null,
  mode varchar(100) not null default '',
  msg varchar(200) not null default '',
  log_time timestamp not null
);


INSERT INTO GlobalParams (name, value, section) VALUES ('product_buildnumber', '20151231', null);
INSERT INTO GlobalParams (name, value, section) VALUES ('product_version', '1.0', null);

INSERT INTO PermissionGroups (permission_group_id, name) VALUES (1, 'Root users');

INSERT INTO Users
  (user_id, web_login, web_passwd, email_addr, language, status, area, permission_group_id, timezone, created_at)
VALUES
  (1, 'root', '$1$rootroot$mruyWtCm.sr.q.wsFb4uK1', 'aivis@cliffit.es', 'en', '1', 'admin', 1, 'GMT', now());
  

