CREATE TABLE `MailGroups` (
  `mail_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`mail_group_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `MailLog` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `sender` varchar(255) NOT NULL DEFAULT '',
  `recipients` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `mail_group_id` int(11) NOT NULL DEFAULT '0' REFERENCES MailGroups (mail_group_id),
  `service_type` char(5) NOT NULL DEFAULT 'email',
  `service_id` int(11) NOT NULL DEFAULT '1',
  `mass_mail_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
   PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB;

CREATE TABLE `MailText` (
  `mail_text_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL DEFAULT '0'  REFERENCES MailLog (mail_id),
  `TEXT` text,
  PRIMARY KEY (`mail_text_id`)
) ENGINE=InnoDB;