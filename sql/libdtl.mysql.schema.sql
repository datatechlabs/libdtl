CREATE TABLE IF NOT EXISTS PermissionGroups (
  permission_group_id serial primary key,
  name varchar(40) not null default ''
);


CREATE TABLE IF NOT EXISTS Users (
  user_id int(11) NOT NULL AUTO_INCREMENT,
  web_login varchar(80) not null default '',
  web_passwd varchar(80) not null default '',
  email_addr varchar(80) not null default '',
  fname varchar(80) NOT NULL DEFAULT '',
  lname varchar(80) NOT NULL DEFAULT '',
  language char(2) not null default 'en',
  picture_id int(11) DEFAULT NULL,
  twitter varchar(80) NOT NULL DEFAULT '',
  status smallint not null,
  area varchar(10) not null default '',
  permission_group_id integer REFERENCES PermissionGroups (permission_group_id),
  timezone varchar(40) not null default 'GMT',
  created_at timestamp not null,
  updated_at datetime DEFAULT NULL,
  about text,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`)
);


CREATE TABLE IF NOT EXISTS WebSessions (
  session_id varchar(64) not null default '' unique,
  user_id integer REFERENCES Users (user_id),
  ip_address VARCHAR(20) not null,
  create_time DATETIME not null,
  last_access_time DATETIME not null,
  area varchar(10) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS GlobalParams (
  name varchar(64) not null default '',
  value varchar(200) not null default '',
  section varchar (64) default null,
  UNIQUE (name, section)
);

CREATE TABLE IF NOT EXISTS AccessLog (
  access_id serial primary key,
  type varchar(80) not null default '',
  user_id integer REFERENCES Users (user_id),
  area varchar(16) not null default '',
  ip_address VARCHAR(20) not null,
  mode varchar(100) not null default '',
  msg varchar(200) not null default '',
  log_time timestamp not null
);

CREATE TABLE IF NOT EXISTS `Apiclients` (
  `apiclient_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` integer REFERENCES Users (user_id),
  `name` varchar(40) NOT NULL DEFAULT '',
  `auth_method` enum('BASIC','HMAC','IPADDR') DEFAULT 'HMAC',
  `auth_username` varchar(80) NOT NULL DEFAULT '',
  `auth_password` varchar(80) DEFAULT NULL,
  `client_hash` varchar(12) NOT NULL DEFAULT '',
  `hmac_secret` varchar(80) DEFAULT NULL,
  `hmac_algo` enum('md5','sha256') DEFAULT 'sha256',
  PRIMARY KEY (`apiclient_id`),
  UNIQUE KEY `apiclient_id` (`apiclient_id`),
  UNIQUE KEY `auth_username` (`auth_username`),
  UNIQUE KEY `client_hash` (`client_hash`)
);

CREATE TABLE IF NOT EXISTS `Templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(8) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`template_id`)
);

CREATE TABLE IF NOT EXISTS `TemplateFields` (
  `template_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` integer REFERENCES Templates (template_id),
  `special` int(11) NOT NULL DEFAULT '0',
  `order_number` int(11) NOT NULL,
  `page` int(11) NOT NULL DEFAULT '1',
  `target_field` varchar(80) NOT NULL DEFAULT '',
  `pattern` varchar(80) NOT NULL DEFAULT '',
  `default_value` varchar(80) NOT NULL DEFAULT '',
  `option_value` varchar(40) NOT NULL,
  PRIMARY KEY (`template_field_id`),
  UNIQUE KEY `page_col` (`template_id`,`page`,`order_number`)
);

CREATE TABLE IF NOT EXISTS `Blobs` (
  `blob_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `internal_id` int(11) DEFAULT NULL,
  `mimetype` varchar(40) DEFAULT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blob_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `BlobData` (
  `blob_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `blob_id` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`blob_data_id`)
) ENGINE=InnoDB;

CREATE TABLE `MailGroups` (
  `mail_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`mail_group_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `MailLog` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `sender` varchar(255) NOT NULL DEFAULT '',
  `recipients` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `mail_group_id` int(11) NOT NULL DEFAULT '0' REFERENCES MailGroups (mail_group_id),
  `service_type` char(5) NOT NULL DEFAULT 'email',
  `service_id` int(11) NOT NULL DEFAULT '1',
  `mass_mail_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB;

CREATE TABLE `MailText` (
  `mail_text_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL DEFAULT '0'  REFERENCES MailLog (mail_id),
  `TEXT` text,
  PRIMARY KEY (`mail_text_id`)
) ENGINE=InnoDB;

INSERT INTO GlobalParams (name, value, section) VALUES ('product_buildnumber', '20151231', null);
INSERT INTO GlobalParams (name, value, section) VALUES ('product_version', '1.0', null);

INSERT INTO PermissionGroups (permission_group_id, name) VALUES (1, 'Root users');

INSERT INTO Users
  (user_id, web_login, web_passwd, email_addr, language, status, area, permission_group_id, timezone, created_at)
VALUES
  (1, 'root', '$1$rootroot$mruyWtCm.sr.q.wsFb4uK1', 'aivis@cliffit.es', 'en', '1', 'admin', 1, 'GMT', now());


