<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 10/01/2018
 * Time: 10:38
 */

class HostedSite extends Model {


    function __construct()
    {

        $this->table = "hosted_sites";
        $this->id_field = "site_id";
        $this->editable_fields = array(
            'site_id', 'site', 'tblpref', 'sim_calls', 'expiry_date', 'create_time', 'status' ,'allow_remote_ses', 'mode'
        );
        parent::__construct();
    }

    public function get_by_name($name) {

        if (!$name)
            return false;

        $this->where('site', '=', $name)->getOne();
        return $this->data;

    }

}