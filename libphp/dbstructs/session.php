<?php

class Session extends Model {


function __construct()
{

	$this->table = "WebSessions";
	$this->id_field = "session_id";
	$this->editable_fields = array(
		'session_id', 'user_id', 'ip_address', 'create_time', 'last_access_time', 'area'
	);
	parent::__construct();
}


function delete_older_than($timestamp)
{

	$db = Dbconnect::getInstance();

	if ($db->engine == "mysql")

		$query = sprintf(
			"DELETE FROM %s WHERE last_access_time < :timestamp", $this->table);
	else
		$query = sprintf(
			"DELETE FROM %s WHERE last_access_time < to_timestamp(:timestamp)", $this->table);

	$params = array('timestamp' => $timestamp);
	
	return $db->execute($query, $params);

}

}

?>
