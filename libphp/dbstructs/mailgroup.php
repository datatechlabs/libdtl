<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 30/01/2017
 * Time: 16:48
 */

class MailGroup extends Model
{

    protected $table;
    protected $id_field;

    function __construct()
    {
        $this->table = "MailGroups";
        $this->id_field = "mail_group_id";
        $this->editable_fields = array('name');
    }

}