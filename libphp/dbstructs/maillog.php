<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 30/01/2017
 * Time: 15:47
 */

class MailLog extends Model
{

    protected $table;
    protected $id_field;

    function __construct()
    {
        $this->table = "MailLog";
        $this->id_field = "mail_id";
        $this->editable_fields = array(
            'subject', 'sender', 'recipients', 'status', 'mail_group_id', 'service_type', 'service_id', 'mass_mail_id');
        $this->createtime_field = 'create_time';
    }
}