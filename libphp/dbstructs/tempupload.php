<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 02/03/2017
 * Time: 9:53
 */

class TempUpload extends Model
{
    function __construct()
    {
        $this->table = "TempUpload";
        $this->id_field = "template_upload_id";
        $this->editable_fields = array();
    }

}
