<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 30/01/2017
 * Time: 17:01
 */

class MailText extends Model
{

    protected $table;
    protected $id_field;

    function __construct()
    {
        $this->table = "MailText";
        $this->id_field = "mail_text_id";
        $this->editable_fields = array(
            'text');
    }
}