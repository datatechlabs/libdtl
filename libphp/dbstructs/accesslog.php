<?php

class AccessLog extends Model {

protected $table;
protected $id_field;

function __construct()
{
	$this->table = "AccessLog";
	$this->id_field = "access_id";
	$this->editable_fields = array(
		'type', 'user_id', 'area', 'ip_address', 'mode', 'msg', 'log_time');
	parent::__construct();
}

function append($type, $mode, $msg, $user)
{

	$area = settings::get_value('area', 'MISC');
	$ipaddr = common::get_ip();
	$now = New Datetime("NOW");

	if (isset($user)) {
		if (isset($user->webacc)) {
			$user_id = $user->webacc->user_id;
		} else {
			$user_id = null;
		}
	} else {
		$user_id = null;
	}

	$query = array(
		'type' => $type,
		'user_id' => $user_id,
		'area' => $area,
		'ip_address' => $ipaddr,
		'mode' => $mode,
		'msg' => $msg,
		'log_time' => $now
		);
	$this->insert($query);
	return true;	
	
}

}

?>
