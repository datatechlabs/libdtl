<?php

class WebAccount extends Model {

protected $table;
protected $id_field;

	function __construct()
	{
		$this->table = "Users";
		$this->id_field = "user_id";
		$this->editable_fields = array(
			'web_login',
			'web_passwd',
			'fname',
			'lname',
			'email_addr',
			'language',
			'status',
			'area',
			'permission_group_id',
			'about',
			'timezone');
		$this->createtime_field = 'created_at';
		$this->updatetime_field = 'updated_at';
		$this->displayable_fields = array('user_id', 'web_login', 'fname' ,'lname', 'email_addr', 'language', 'timezone', 'picture_id', 'about', 'twitter');
		parent::__construct();
	}

	public function getPicture()
	{
		$base_url = common::base_url();
		return $base_url."/userpic/".$this->picture_id;
	}
}

?>
