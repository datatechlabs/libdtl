<?php

class Picture extends Model {

    protected $table;
    protected $id_field;

    function __construct()
    {
        $this->table = "Picture";
        $this->id_field = "picture_id";
        $this->editable_fields = array(
           );
        $this->createtime_field = 'created_at';
        $this->updatetime_field = 'updated_at';
        parent::__construct();
    }

    public function get_url()
    {
        $this->url ="http://something";

        // if we have a parent, append this as a child
        if (isset($this->parent_object)) {
            $ch_name = get_class($this);
            common::ndebug(4,"ADDING %s to PICTURE", $ch_name);
            $this->parent_object->data->haha = "aaa";
            $this->parent_object->children[]->$ch_name = $this;
        }

        common::ndebug(4,"PICTURE PARENT %s", $this->parent_object);

        return $this;
    }
}

