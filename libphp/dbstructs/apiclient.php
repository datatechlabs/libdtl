<?php

/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 02/02/2016
 * Time: 16:29
 */
class Apiclient extends Model
{
    function __construct()
    {
        $this->table = "Apiclients";
        $this->id_field = "apiclient_id";
        $this->editable_fields = array('name', 'auth_method', 'auth_username', 'auth_password', 'client_hash', 'hmac_secret', 'hmac_algo');
    }

}
