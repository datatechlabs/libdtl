<?php

// each field type has functionality for filling out templates
require_once("$libpath/template.php");

/*
 * Form field base class
 */
class FormField {

var $name;		// fields are referenced by this name inside
			// the form class
var $label;		// label, as it will appear in the HTML document
var $value_type;	// STRING, INT, DATETIME, etc.
var $required = 0;	// is it ok for this field to have no value?
var $error = 0;		// will be set to 1 if some error occurs
			// with the input

// these must be overriden in child classes
function is_posted() {}			// has the field been posted? return: bool
function fill_template(&$templ) {}	// fill HTML template
function set_posted_value() {}		// set value from $_POST array
function set_value($valstr) {}		// set value
function get_value() {}			// return value as a string
	
}

/*
 * TextField class
 */
class TextField extends FormField {

var $text;

function is_posted() { return array_key_exists($this->name, $_POST); }

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_VALUE', htmlspecialchars($this->text));
	$errlabel = $this->error ? 'has-error' : '';
	$templ->assign($fieldname . '_LSTYLE', $errlabel);
}

function set_posted_value() { $this->text = stripslashes($_POST[$this->name]); }

function set_value($valstr) { $this->text = $valstr; }
function get_value() { return $this->text; }

}

/*
 * Selection field class
 */
class SelectionField extends FormField {

var $selected;	// value of the option which should be selected
var $options = array();	// value => description
var $grouped = false;	// are the options grouped?
var $styles = array();

function is_posted() { return array_key_exists($this->name, $_POST); }

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_LSTYLE', $this->error ? 'erroneous' : 'label');
	
	reset($this->options);
	foreach ($this->options as $key => $value ) {
		$key = htmlspecialchars($key);
		if (is_array($value)) {
			// option group
			$templ->append($fieldname . '_OPTIONS', "<optgroup label=\"$key\">");
			
			foreach ($value as $optval => $descr) {
				$optval = htmlspecialchars($optval);
				$descr = htmlspecialchars($descr);
				if (strcmp($optval, $this->selected))
					$templ->append($fieldname . '_OPTIONS', "<option value=\"$optval\">$descr</option>");
				else
					$templ->append($fieldname . '_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
			}
			
			$templ->append($fieldname . '_OPTIONS', '</optgroup>');
		}
		else {
			$value = htmlspecialchars($value);
			if (isset($this->styles[$key]))
				$style = $this->styles[$key];
			else
				$style = '';
			// simple options
			if (strcmp($key, $this->selected))
				$templ->append($fieldname . '_OPTIONS', "<option value=\"$key\" style=\"$style\">$value</option>");
			else
				$templ->append($fieldname . '_OPTIONS', "<option value=\"$key\" style=\"$style\" selected=\"selected\">$value</option>");
		}
	}
}

function set_options($optionarr) { $this->options = $optionarr; }
function set_styles($stylearr) { $this->styles = $stylearr; }

function set_posted_value() { $this->selected = $_POST[$this->name]; }

function set_value($valstr) { $this->selected = $valstr; }
function get_value() { return addslashes($this->selected); }
}

/*
 * Checkbox field class
 */
class CheckboxField extends FormField {

var $enabled = 0;

function is_posted() { return true; }

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_LSTYLE', $this->error ? 'erroneous' : 'label');
	
	$checkstr = $this->enabled ? 'checked="checked"' : '';		
	$templ->assign($fieldname . '_VALUE', $checkstr);
}

function set_posted_value() { $this->enabled = array_key_exists($this->name, $_POST); }

function set_value($val) { $this->enabled = $val; }
function get_value() { return $this->enabled ? 1 : 0; }

}

/*
 * Radio field class
 */
class RadioField extends FormField {

function is_posted() { return true; }

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_LSTYLE', $this->error ? 'erroneous' : 'label');
	
	reset($this->options);
	foreach($this->options as $key => $value) {
		$key = htmlspecialchars($key);
		$value = htmlspecialchars($value);
		// simple options
		if (!strcmp($key, $this->selected))
			$templ->append($fieldname . '_VALUE_'.$key, " checked=\"checked\"");
		else
			$templ->append($fieldname . '_VALUE_'.$key, "");
	}
}

function set_options($optionarr) { $this->options = $optionarr; }

function set_posted_value() { $this->selected = $_POST[$this->name]; }

function set_value($valstr) { $this->selected = $valstr; }
function get_value() { return addslashes($this->selected); }

}

/*
 * Time field class
 */
class TimeField extends FormField {

var $hour;
var $min;
var $sec;

var $type = 1;	// 0 - seconds disabled, 1 - seconds enabled

function is_posted()
{
	return array_key_exists($this->name . '_time', $_POST);
}

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_TIME_VALUE', "{$this->hour}:{$this->min}");
	$templ->assign($fieldname . '_LSTYLE', $this->error ? 'erroneous' : 'label');
}

function set_posted_value()
{
	$time = explode(':', $_POST[$this->name . '_time']);
	$this->hour = $time[0];
	$this->min = $time[1];
	
	if ($this->type == 1)
		$this->sec = $time[2];
}

function set_value($timestr)
{
	$timearr = common::gettime_from_iso($timestr);
	
	$this->hour = $timearr['hours'];
	$this->min = $timearr['minutes'];
	$this->sec = $timearr['seconds'];
}

function get_value()
{
	return str_pad($this->hour, 2, '0', STR_PAD_LEFT) . ':' .
		str_pad($this->min, 2, '0', STR_PAD_LEFT) . ':' .
		str_pad($this->sec, 2, '0', STR_PAD_LEFT);
}

}

/*
 * Date field class
 */
class DateField extends FormField {

var $year;
var $month;
var $day;

var $yearopt = array();
var $monthopt = array();
var $dayopt = array();

function is_posted()
{
	return 	array_key_exists($this->name . '_year', $_POST) &&
			array_key_exists($this->name . '_month', $_POST) &&
			array_key_exists($this->name . '_day', $_POST);
}

function fill_template(&$templ)
{
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_NAME', $this->name);
	$templ->assign($fieldname . '_LABEL', $this->label);
	$templ->assign($fieldname . '_LSTYLE', $this->error ? 'erroneous' : 'label');
	
	$templ->assign($fieldname . '_YEAR_VALUE', $this->year);
	$templ->assign($fieldname . '_MONTH_VALUE', $this->month);
	$templ->assign($fieldname . '_DAY_VALUE', $this->day);
	
	reset($this->yearopt);
	foreach ($this->yearopt as $optval => $descr) {
		if ($optval != $this->year)
			$templ->append($fieldname . '_YEAR_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_YEAR_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
	
	reset($this->monthopt);
	foreach ($this->monthopt as $optval => $descr) {
		if ($optval != $this->month)
			$templ->append($fieldname . '_MONTH_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_MONTH_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
	
	reset($this->dayopt);
	foreach ($this->dayopt as $optval => $descr) {
		if ($optval != $this->day)
			$templ->append($fieldname . '_DAY_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_DAY_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
}

function set_posted_value()
{		
	$this->year = $_POST[$this->name . '_year'];
	$this->month = $_POST[$this->name . '_month'];
	$this->day = $_POST[$this->name . '_day'];
}

function set_value($datestr)
{
	$dtimearr = common::getdate_from_iso($datestr . " 00:00:00");
	
	$this->year = $dtimearr['year'];
	$this->month = $dtimearr['mon'];
	$this->day = $dtimearr['mday'];
}

function set_year_options($years) { $this->yearopt = $years; }
function set_month_options($months) { $this->monthopt = $months; }
function set_day_options($days) {$this->dayopt = $days; }

function get_value()
{
	return  str_pad($this->year, 2, '0', STR_PAD_LEFT) . '-' .
			str_pad($this->month, 2, '0', STR_PAD_LEFT) . '-' .
			str_pad($this->day, 2, '0', STR_PAD_LEFT);
}

}

/*
 * Datetime field class
 */
class DatetimeField extends TimeField {

var $year;
var $month;
var $day;

var $yearopt = array();
var $monthopt = array();
var $dayopt = array();

function DatetimeField() { $this->type = 0; $this->seconds = 0; }

function is_posted()
{
	return 	array_key_exists($this->name . '_year', $_POST) &&
			array_key_exists($this->name . '_month', $_POST) &&
			array_key_exists($this->name . '_day', $_POST) &&
			parent::is_posted();
}

function fill_template(&$templ)
{
	parent::fill_template($templ);
	
	$fieldname = strtoupper($this->name);
	
	$templ->assign($fieldname . '_YEAR_VALUE', $this->year);
	$templ->assign($fieldname . '_MONTH_VALUE', $this->month);
	$templ->assign($fieldname . '_DAY_VALUE', $this->day);
	
	reset($this->yearopt);
	foreach ($this->yearopt as $optval => $descr) {
		if ($optval != $this->year)
			$templ->append($fieldname . '_YEAR_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_YEAR_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
	
	reset($this->monthopt);
	foreach ($this->monthopt as $optval => $descr) {
		if ($optval != $this->month)
			$templ->append($fieldname . '_MONTH_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_MONTH_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
	
	reset($this->dayopt);
	foreach ($this->dayopt as $optval => $descr) {
		if ($optval != $this->day)
			$templ->append($fieldname . '_DAY_OPTIONS', "<option value=\"$optval\">$descr</option>");
		else
			$templ->append($fieldname . '_DAY_OPTIONS', "<option value=\"$optval\" selected=\"selected\">$descr</option>");
	}
}

function set_year_options($years) { $this->yearopt = $years; }
function set_month_options($months) { $this->monthopt = $months; }
function set_day_options($days) {$this->dayopt = $days; }

function set_posted_value()
{
	parent::set_posted_value();
	
	$this->year = $_POST[$this->name . '_year'];
	$this->month = $_POST[$this->name . '_month'];
	$this->day = $_POST[$this->name . '_day'];
}

function set_value($datestr)
{
	$dtimearr = common::getdate_from_iso($datestr);
	
	$this->year = $dtimearr['year'];
	$this->month = $dtimearr['mon'];
	$this->day = $dtimearr['mday'];
	$this->hour = $dtimearr['hours'];
	$this->min = $dtimearr['minutes'];
	$this->sec = $dtimearr['seconds'];
}

function get_value()
{
	return  str_pad($this->year, 2, '0', STR_PAD_LEFT) . '-' .
			str_pad($this->month, 2, '0', STR_PAD_LEFT) . '-' .
			str_pad($this->day, 2, '0', STR_PAD_LEFT) . ' ' .
			str_pad($this->hour, 2, '0', STR_PAD_LEFT) . ':' .
			str_pad($this->min, 2, '0', STR_PAD_LEFT) . ':' .
			str_pad($this->sec, 2, '0', STR_PAD_LEFT);
}

}

?>
