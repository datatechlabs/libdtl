<?php

/*
 * Database connection class.
 */
class db {

    public $connections = array();	// connection array
    public $resarr = array();	// query result handles
    public $selected;		// currently selected connection
    public $connected;
    public $engine;

    public $error;

    // establish a new connection
    public function connect($dbhost, $dbuser, $dbpass, $dbname, $identifier = 'main')
    {
        $conn_str = sprintf("host=%s dbname=%s user=%s password=%s", $dbhost, $dbname, $dbuser, $dbpass);
        $this->selected = $identifier;
        $this->connected = false;
        $this->engine = "postgresql";

        // connect
        $this->connections[$this->selected] = pg_connect($conn_str) or die ("Database connection failed. Please retry\n");

        if ($this->connections[$this->selected] === false)
            return false;
        $this->connected = true;
        return true;
    }

    public function disconnect()
    {
        return pg_close($this->connections[$this->selected]);
    }

	public function set_active($identifier)
	{
		$this->selected = $identifier;
	}

	public function get_active()
	{
		return $this->selected;
	}

	// get connection resource for direct manipulation
	public function get_resource()
	{
		return $this->connections[$this->selected];
	}



	public function prepare_and_exec($query, $params)
	{
		$conn = $this->connections[$this->selected];
		if (!$conn) {
			return false;
		}
		$stmntname = common::rand_string(8);
		if(!$result = pg_prepare($conn, $stmntname, $query)) {
			common::ndebug(2, 'postgresql::prepare_and_exec: query prepare failed %s', $query);
			return false;
		}
		if(!$res = pg_execute($conn, $stmntname, $params)) {
			common::ndebug(2, 'postgresql::prepare_and_exec: query execute failed %s', $query);
			return false;
		}

		return $res;
	}

	// get associative result array
	public function fetch_assoc($stmt, $params)
	{

		$res = $this->prepare_and_exec($stmt, $params);
		$id = 0;
		$assoc = array();
		while( $row = pg_fetch_assoc($res, NULL) ) {

			$assoc[$id] = $row;
			$id++;

		}
		pg_free_result($res);
		return $assoc;
	}

}
