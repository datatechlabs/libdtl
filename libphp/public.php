<?php

//require_once("$libpath/valid.php");
//require_once("$libpath/mail.php");
//require_once("$libpath/sms_lib.php");
//require_once("$libpath/dbstructs/voipacct.php");
//require_once("$libpath/utils/webchat.php");
//require_once("$libpath/utils/currency.php");
//require("$libpath/utils/vat.php");
//require_once("$libpath/utils/public.php");
//require_once("$libpath/dbstructs/publicpage.php");
//require_once("$libpath/dbstructs/ticket.php");
//require_once("$libpath/utils/paymethods.php");
//require_once("$libpath/utils/account.php");
//require_once("$libpath/utils/paymethods/paypal.php");
//require_once("$libpath/utils/paymethods/mollie.php");
//require_once("$libpath/utils/paymethods/paysafecard.php");



/*
 * Various functionality available from the public area
 */

function public_page($pagename)
{
	global $g_site, $tblnames, $g_user;


	common::ndebug(4, "Public page. Request %s", $_REQUEST);


	list($scriptfile, $ctrlclass, $id, $sub_ids, $method) = site::pagescript_path();

	common::ndebug(4, "Scriptfile %s, class %s, ID %s sub_ids %s method %s", $scriptfile, $ctrlclass, $id, $sub_ids, $method);

	if (is_file($scriptfile)) {
		$package_root = settings::get_simple_path('package_root');
		$libdtl_root = settings::get_simple_path('libdtl_root');

		/* include appropriate script here */
		if (!require($scriptfile)) {
			common::ndebug(2, "File %s cannot be included!", $scriptfile);
			http_response_code(500);
			exit(1);
		}

		if (!class_exists($ctrlclass)) {
			common::ndebug(2, "Class %s does not exist!", $ctrlclass);
			http_response_code(500);
			exit(1);
		}

		$controller = New $ctrlclass();
		$method = $controller->route($id, $method);
		if (!method_exists($controller, $method)) {
			common::ndebug(2, "Method %s does not exist for %s", $method, $controller);
			http_response_code(500);
			exit(1);
		}
		//call_user_func_array( array( $controller, $method ) , array($_REQUEST, $ids));
		$ctrl_args = array($_REQUEST);
		if ($id) {
			array_push($ctrl_args, $id);
		}

		if (is_array($sub_ids)) {
			foreach ($sub_ids as $id) {
				array_push($ctrl_args, $id);
			}
		}
		common::ndebug(4, "Calling  %s->%s()", $controller, $method);
		call_user_func_array(array($controller, $method), $ctrl_args);

	} else {
		common::ndebug(2, "No route found for request %s area %s", $_GET['script'], $_GET['area']);
		http_response_code(404);
		exit(1);
	}
}

