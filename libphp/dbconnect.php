<?php

/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 08/02/2016
 * Time: 21:32
 */
class Dbconnect
{

    public $dbh; // handle of the db connexion
    private static $instance;
    public $engine; //mysql, postgresql
    private $stmt;
    public $error;
    public $error_code;

    private function __construct()
    {
        $dbhost = settings::get_value('dbhost', 'SQL', 'localhost');
        $dbuser = settings::get_value('dbuser', 'SQL', false);
        $dbpass = settings::get_value('dbpass', 'SQL', false);
        $dbname = settings::get_value('dbname', 'SQL');
        $engine = settings::get_value('engine', 'SQL');
        $charset = 'utf8';

        switch ($engine) {
            case 'mysql':
                $dsn = $engine.":host=$dbhost;dbname=$dbname;charset=$charset";
                break;
            case 'postgres':
                $dsn = "pgsql:host=$dbhost;dbname=$dbname;";
                break;
            default:
                $dsn = $engine.":host=$dbhost;dbname=$dbname;charset=$charset";

        }

        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        $this->dbh = new PDO($dsn, $dbuser, $dbpass, $opt);
        $this->engine = $engine;
    }

    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    public function execute($query, $params)
    {
        $params = $this->format_params($params);

        common::ndebug(5, "dbconnect::execute query %s, params %s", $query, $params);

        $stmt = $this->dbh->prepare($query);

        try {
            $stmt->execute($params);
        } catch (PDOException $e) {
            common::ndebug(2, "dbconnect::ERROR IN query %s, params %s error %s code %s", $query, $params, $e->getMessage(), $e->getCode());
            $this->error = $this->get_error_translation($e->getCode());
            $this->error_code = $this->get_error_descr($e->getCode());
            return false;
        }
        $this->stmt = $stmt;

        return true;
    }

    public function last_id()
    {
        return $this->dbh->lastInsertId();
    }

    public function begin() {
        $this->dbh->beginTransaction();
    }

    public function commit() {
        $this->dbh->commit();
    }

    public function rollback() {
        $this->dbh->rollBack();
    }
    public function getLock($name, $timeout) {
        $stmt = "SELECT GET_LOCK(:name, :timeout) AS VALUE";
        $params = array('name' => $name, 'timeout' => $timeout);
        $row = $this->fetch_object($stmt, $params);
        if ($row->VALUE == 1)
            return true;
        return false;
    }
    public function releaseLock($name) {
        $stmt = "SELECT RELEASE_LOCK(:name) AS VALUE";
        $params = array('name' => $name);
        if($this->execute($stmt, $params))
            return true;
        return false;
    }

    public function fetch_object($query, $params) {
        $params = $this->format_params($params);
        if (($result = $this->execute($query, $params)) == false)
            return false;

        $data = $this->stmt->fetchAll(PDO::FETCH_OBJ);
        if (count($data) == 0)
            return false;

        return $data[0];
    }

    public function fetch_assoc($query, $params) {
        $params = $this->format_params($params);
        if (($result = $this->execute($query, $params)) == false)
            return false;

        $data = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function fetch_one($query, $params) {
        $params = $this->format_params($params);

        if (($result = $this->execute($query, $params)) == false)
            return false;

        $data = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($data) == 0)

            return false;
        return $data[0];
    }

    private function format_params($params) {

        $ret = array();
        foreach($params as $id => $param) {
            $type = gettype($param);
            switch($type) {
                case "string":
                    $ret[$id] = $param;
                    break;
                case "integer":
                    $ret[$id] = $param;
                    break;
                case "object":
                    $cltype = get_class($param);
                    switch ($cltype) {
                        case "DateTime":
                            $ret[$id] = $param->format('Y-m-d H:i:s');
                            break;
                        default:
                            $ret[$id] = $param->text; // FIXME
                    }
                    break;
                default:
                    $ret[$id] = $param;
            }
        }
        return $ret;
    }

    private function get_error_translation($code) {
        if ($this->engine == "mysql") {
            switch($code) {
                case "23000":
                    return "Duplicate entry";
                case "1366":
                    return "Incorrect data type";
                case "1265":
                    return "Data too large to store";
                default:
                    return "error code " . $code;
            }
        } else {
            return "error code " . $code;
        }
    }

    private function get_error_descr($code) {
        if ($this->engine == "mysql") {
            switch($code) {
                case "23000":
                    return "E_DUPLICATE";
                default:
                    return "E_OTHER";
            }
        } else {
            return "E_OTHER";
        }
    }

}
