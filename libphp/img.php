<?php
// outputs JPG image
// w - width (int)
// h - height (int)
// i - image data id (int)
// o - owner id (int)
// t - data type (str)
// s - site (str)
// a -alt text in case of failure (str)
// q - quality (1-100), default = 75 (int)
if (!function_exists('imagecreatefromstring'))  {
	header('Content-Type: image/jpeg');
	return "";
}

require_once("config.php");
require_once("utils/common.php");

if (function_exists("apache_getenv")) {
	$config_file = apache_getenv('DTL_CONFIG_FILE');
	$config_file_type = apache_getenv('DTL_CONFIG_FILE_TYPE'); // one of 'php' or 'conf'
} else {
	$config_file = $_SERVER["DTL_CONFIG_FILE"];
	if (array_key_exists('DTL_CONFIG_FILE_TYPE', $_SERVER)) {
		$config_file_type = $_SERVER["DTL_CONFIG_FILE_TYPE"];
	} else {
		$config_file_type = 'conf';
	}
}

settings::init($config_file);
$libdtl_root = settings::get_value("libdtl_root", "PATHS");
$libpath = "$libdtl_root/libphp";

require_once("$libpath/system.php");
require_once("$libpath/site.php");
require_once("$libpath/utils/image.php");

system::init($config_file);

$w = (int)$_GET['w'];
$h = (int)$_GET['h'];
$i = (int)$_GET['i'];
$o = (int)$_GET['o'];
if (!isset($_GET['o']))
	$o = 0;
$t = db::escape($_GET['t']);
$site = db::escape($_GET['s']);
$a = db::escape($_GET['a']);
$q = (int)$_GET['q'];

if(!tables::init_tables_from_site($site)) {
	common::ndebug(4,"img: Site Does not exist, disabled or expired",0);
	exit(1);
}
// get site params
$g_site = New stdClass();
$g_site->params = site::get_params();

if (!$t) {  // type
	DisplayImage(0, $q);
	exit(0);
}

if ($t) {
	list($img, $type, $size) = common::retrieve_binary_object($t, $i, $o);
}

if (!$q)
	$q = 75;

if (strlen($img)<1) {
	DisplayImage(0, $q);
	exit(0);
}

DisplayImage($img, $q);
exit(0);

function DisplayImage($imgfile, $quality)
{
	global $w, $h;
	header('Content-Type: image/jpeg');
	$img = LoadJpeg($imgfile);

	/* add watermark */
	//	$img = addWaterMark($img, 'images/no-winners1.png', $w-22, $offset, 22);

	imagejpeg($img, NULL, $quality);
	imagedestroy($img);
}

/*add watermark (small image in the corner actually)*/
function addWaterMark($img, $icon, $dest_x, $dest_y, $size)
{
	$watermark = imagecreatefrompng($icon);  
	$watermark_width = imagesx($watermark);  
	$watermark_height = imagesy($watermark);  
	// resize watermark to fixed width
	$watermark_r = imagecreatetruecolor($size,$size);
	// set bg transparent

	imagealphablending($watermark_r, false);
	$colorTransparent = imagecolorallocatealpha($watermark_r, 0, 0, 0, 127);
	imagefill($watermark_r, 0, 0, $colorTransparent);
	imagesavealpha($watermark_r, true);

	imagecopyresampled  ($img, $watermark, $dest_x, $dest_y, 0, 0, $size, $size, $watermark_width  , $watermark_height  );	
	return $img;
}

function LoadJpeg($imgdata)
{
	global $w, $h, $a;

	/* create image from stream in its original size*/
	if ($imgdata) {
		$src = imagecreatefromstring($imgdata);
		$owidth = imagesx($src);
		$oheight = imagesy($src);
	} else {
		$owidth = $w;
		$oheight = $h;
	}

	$width = ($w) ? ($w) : $owidth;
	$height = ($h) ? ($h) : $oheight;

	/* create white image */
	$im = imagecreatetruecolor ($width, $height);
	$white = imagecolorallocate ($im, 255, 255, 255);
	imagefill ($im, 0, 0, $white);
	if (!is_string($imgdata))
    {
        /* Create a white image */
        imagefilledrectangle($im, 0, 0, $width, $height, $white);

        /* Output an error message */
		if ($a)
			$msg = $a;
		else
			$msg = 'No Image';
		$tc = imagecolorallocate ($im, 0, 0, 0);
        imagestring($im, 5, 5, 5, $msg, $tc);
		return $im;
    }

//	$im = imagecreatetruecolor ($width, $height);
//	imagefill ($im, 0, 0, $white);

//	$src = @imagecreatefromjpeg($imgname);
	/* See if it failed */
	if(!$src)
    {
        /* Create a white image */
        imagefilledrectangle($im, 0, 0, $width, $height, $white);

        /* Output an error message */
		if ($a)
			$msg = $a;
		else
			$msg = 'No Image';
		$tc = imagecolorallocate ($im, 0, 0, 0);
        imagestring($im, 5, 5, 5, $msg, $tc);
		return $im;
    }

	// determine aspect ratio of the image
	$dst_asp = $width / $height;
	$src_asp = $owidth / $oheight;
	if ($dst_asp < $src_asp) { // original is wide
		$dst_w = $width;
		$dst_h = $width / $src_asp;
		$dst_x = 0;
		$dst_y = ($height - $width / $src_asp) / 2 ;
	} else { // original is tall 
		$dst_w = $height * $src_asp;
		$dst_h = $height;
		$dst_x = ($width - $height * $src_asp) /2;
		$dst_y = 0;
	}

	imagecopyresampled($im, $src, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $owidth, $oheight);

    return $im;
}


?>
