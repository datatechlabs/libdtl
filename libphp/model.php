<?php


/**
 * database model
 *
 * use following methods for CRUD
 *
 */


// getById (respects: belongsTo)
// getMany (respects: hasMany)
// fetchAllRaw
// fetchOneRaw
// getAll (respects: hasMany)
// [join->]where->[orWhere->][limit->][orderBy->]get  (respects: belongsTo)
// [join->]where->[orWhere->]getOne    (respects: belongsTo,belongsToMany)
// where->[orWhere->]count
// where->[orWhere->][limit->]->update
// findOne
// insert (SQL:INSERT)
// save   (SQL:UPDATE)
// remove  (SQL:DELETE, by ID)
// removeMany  (SQL:DELETE)
// updateAll (SQL:UPDATE without WHERE)


class Model
{

	protected $table;
	protected $id_field;
	protected $editable_fields = array();
	protected $owner_field;
	protected $createtime_field;
	protected $updatetime_field;
	protected $use_soft_remove = false;
	protected $removetime_field;
	protected $status_field;
	protected $displayable_fields = array();
	public $tbl_prefix;
	protected $prefixable = true;
	public $error;
	public $last_id;
	private $whereClause = array();
	private $orWhereClause = array();
	private $joinClause = array();
	private $groupClause = array();
	private $limit;
	private $offset;
	private $orderby = array();
	private $direction = array();
	protected $has_one = array();
	protected $has_many = array();
	protected $belongs_to = array();
	protected $belongs_to_many = array();

	public function __construct()
	{
		$this->tbl_prefix = settings::get_value('tblpref', 'SQL', '');
	}

	public function getTableName()
	{
		if ($this->prefixable)
			return $this->tbl_prefix.$this->table;
		else
			return $this->table;
	}

	public function db_begin()
	{
		$db = Dbconnect::getInstance();
		$db->begin();
	}

	public function db_commit()
	{
		$db = Dbconnect::getInstance();
		$db->commit();
	}

	public function db_rollback()
	{
		$db = Dbconnect::getInstance();
		$db->rollback();
	}

	public function getById($id)
	{
		if (!(int)$id && !preg_match('/^\{?[a-f\d]{8}-(?:[a-f\d]{4}-){3}[a-f\d]{12}\}?$/i', $id) ) {
			$this->error = "ID invalid";
			return false;
		}
		$this->id = $id;
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		if (isset($this->displayable_fields) && count($this->displayable_fields)) {
			$cols = implode(",", $this->displayable_fields);
		} else {
			$cols = "*";
		}
		$stmt = "SELECT " . $cols . " FROM " . $table;
		$params = array('key' => $id);

		$stmt .= " WHERE " . $table . "." . $this->id_field . " = :key";

		$row = $db->fetch_object($stmt, $params);

		// hasMany
		if (isset($this->has_many) && count($this->has_many)) {
			foreach ($this->has_many as $hm) {
				$one = new $hm[1];
				$idf = $this->id_field;
				$one->where($idf, '=', $this->id)->get();
				$relname = $hm[0];
				$this->$relname = $one->data;
			}
		}

		//belongsTo
		if (isset($this->belongs_to) && count($this->belongs_to)) {
			foreach ($this->belongs_to as $bt) {
				$bel = new $bt[1];
				$fk = $bel->id_field;
				$bel->where($bel->id_field, '=', $row->$fk)->get();
				$relname = $bt[0];
				$this->$relname = $bel->data[0];
			}
		}

		if (!$row)
			return false;

		foreach ($row as $k => $v)
			$this->$k = $v;
		return true;
	}

	public function getMany($query = array())
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$key = key($query);

		$stmt = "SELECT * FROM " . $table . " WHERE " . $key . " = :key";
		$params = array('key' => $query[$key]);

		$this->data = $db->fetch_assoc($stmt, $params);
		return $this->data;

	}

	public function fetchAllRaw($query, $params)
	{
		$db = Dbconnect::getInstance();
		$this->data = $db->fetch_assoc($query, $params);
		return $this->data;
	}

	public function fetchOneRaw($query, $params)
	{
		$db = Dbconnect::getInstance();
		$this->data = $db->fetch_one($query, $params);
		return $this->data;
	}

	public function getAll()
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$stmt = "SELECT * FROM " . $table;
		$params = array();
		$this->data = $db->fetch_assoc($stmt, $params);

		$idf = $this->id_field;

		// hasMany
		if (isset($this->has_many) && count($this->has_many)) {

			// for every row returned
			foreach ($this->data as $k => $row) {

				$r_id = $row[$idf];

				foreach ($this->has_many as $hm) {
					$one = new $hm[1];

					$one->where($idf, '=', $r_id)->get();
					$relname = $hm[0];
					$this->data[$k][$relname] = $one->data;
				}
			}
		}

		$n_data = array();
		foreach ($this->data as $t_row) {
			$n_data[] = $this->_reverse_replace_fieldnames($t_row);
		}
		$this->data = $n_data;

		return $this->data;

	}

	public function where($field, $arg2 = '', $arg3 = false)
	{
		if ($arg3 === false) {
			$expr = '=';
			$val = $arg2;
		} else {
			$expr = $arg2;
			$val = $arg3;
		}
		array_push($this->whereClause, array($field, $expr, $val));
		return $this;
	}
	
    public function where_notnull($field, $arg2 = '', $arg3 = false)
    {
        if ($arg3) {
            $this->where($field, $arg2, $arg3);
        }
        return $this;
    }

	public function orWhere($field, $arg2 = '', $arg3 = '')
	{
		if ($arg3 == '') {
			$expr = '=';
			$val = $arg2;
		} else {
			$expr = $arg2;
			$val = $arg3;
		}
		array_push($this->orWhereClause, array($field, $expr, $val));
		return $this;
	}

	public function join($table, $constr1, $constr2=false, $constr3=false)
	{
		array_push($this->joinClause, array($table, $constr1, $constr2, $constr3));
		return $this;
	}

	public function limit($limit, $offset = 0)
	{
		$this->limit = (int)$limit;
		$this->offset = (int)$offset;
		return $this;
	}

	public function orderBy($orderby, $direction = 'asc')
	{
		$this->orderby[] = $orderby;
		$this->direction[] = $direction;
		return $this;
	}

	public function groupBy($group_by = array()) {
		$this->groupClause = $group_by;
		return $this;
	}

	public function getOne($columns = array('*'), $force_nondisplayables = false)
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		if (isset($this->displayable_fields) && count($this->displayable_fields) && !$force_nondisplayables) {
			foreach ($this->displayable_fields as $flds) {
				if (preg_match('/^\((.*)\)$/', $flds, $matches)) {
					$fullname[] = $matches[1];
				} elseif (count($parts = explode(".", $flds)) == 2) {
					$fullname[] =  $parts[0] . "." . $parts[1];
				} else {
					$fullname[] = $table . "." . $flds;
				}
			}
			$cols = implode(",", $fullname);
		} else {
			foreach ($columns as $flds) {
				if (preg_match('/^\((.*)\)$/', $flds, $matches)) {
					$fullname[] = $matches[1];
				} elseif (count($parts = explode(".", $flds)) == 2) {
					$fullname[] =  $parts[0] . "." . $parts[1];
				} else {
					$fullname[] = $table . "." . $flds;
				}
			}
			$cols = implode(",", $fullname);
		}
		$stmt = "SELECT ".$cols." FROM ".$table;
		$params = array();

		$i = 1;

		if (is_array($this->joinClause) && count($this->joinClause)) {
			foreach ($this->joinClause as $jk => $jv) {
				if ($jv[3] != false)
					$stmt .= " LEFT JOIN " . $jv[0] . " ON " . $jv[1] . " " .$jv[2]. " ".$jv[3]. " ";
				else
					$stmt .= " LEFT JOIN " . $jv[0] . " USING ( " . $jv[1] . " ) ";
			}
		}

		if (is_array($this->whereClause) && count($this->whereClause)) {
			$stmt .= " WHERE ";
			$where = array();
			foreach ($this->whereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$stmt .= implode(" AND ", $where);
		}

		if (is_array($this->orWhereClause) && count($this->orWhereClause)) {
			$stmt .= " OR ";
			$where = array();
			foreach ($this->orWhereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$stmt .= implode(" AND ", $where);
		}
		if (is_array($this->orderby) && count($this->orderby)) {
			$stmt .= " ORDER BY ";
			$i = 0;
			$orderlist = array();
			foreach ($this->orderby as $ord) {
				$orderlist[] = $ord." ".$this->direction[$i];
				$i++;
			}
			$stmt .= implode(",", $orderlist);
		}
		if (isset($this->limit)) {
//			$stmt .= " LIMIT :limit, :offset";
//			$params['offset'] = $this->offset;
//			$params['limit'] = $this->limit;
			// for some reason cannot bind as INT
			$stmt .= " LIMIT ".$this->offset.", ".$this->limit;
		}
		$this->data = $db->fetch_one($stmt, $params);

		//belongsTo - XXX: may need a review. See get()
		if (isset($this->belongs_to) && count($this->belongs_to)) {
			foreach ($this->belongs_to as $bt) {
				$bel = new $bt[1];
				$fk = $bel->id_field;
				$bel->where($bel->id_field, '=', $this->data[$fk])->get();
				$relname = $bt[0];
				$this->data[$relname] = $bel->data[0];
			}
		}
		// belongsToMany

		if (isset($this->belongs_to_many) && count($this->belongs_to_many)) {
			foreach ($this->belongs_to_many as $bm) {
				$bem  = new $bm[1];
				$fk = $bem->id_field;
				$bem_table = $bem->getTableName();
				$ft = $bem_table;
				$inter_table = $table."".$bem_table;
				$bem->join($inter_table, $bem_table.".".$bem->id_field, '=',  $inter_table.".".$bem->id_field)
					->where($this->id_field, '=', $this->data[$this->id_field])
					->get();
				$relname = $bm[0];
				$this->data[$relname] = $bem->data;
			}
		}


		return $this->data;
	}

	public function get($columns=['*'], $force_nondisplayables=false, $select_tags=[], $formats=[])
    {
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		if (isset($this->displayable_fields) && count($this->displayable_fields) && !$force_nondisplayables) {
			foreach ($this->displayable_fields as $flds) {
				if (preg_match('/^\((.*)\)$/', $flds, $matches)) {
					$fullname[] = $matches[1];
				} elseif (count($parts = explode(".", $flds)) == 2) {
					$fullname[] =  $parts[0] . "." . $parts[1];
				} else {
					$fullname[] = $table . "." . $flds;
				}
			}
			$cols = implode(",", $fullname);
		} else {
			foreach ($columns as $flds) {
				if (preg_match('/^\((.*)\)$/', $flds, $matches)) {
					$fullname[] = $matches[1];
				} elseif (count($parts = explode(".", $flds)) == 2) {
					$fullname[] =  $parts[0] . "." . $parts[1];
				} else {
					$fullname[] = $table . "." . $flds;
				}
			}
			$cols = implode(",", $fullname);
		}

		$select_tagstr = '';
		if (is_array($select_tags)) {
			$select_tagstr .= implode(" ", $select_tags);
		}

		$stmt = "SELECT ".$select_tagstr." ".$cols." FROM ".$table;
		$params = array();

		$i = 1;
		if (is_array($this->joinClause) && count($this->joinClause)) {
			foreach ($this->joinClause as $jk => $jv) {
				if ($jv[3] != false)
					$stmt .= " LEFT JOIN " . $jv[0] . " ON " . $jv[1] . " " .$jv[2]. " ".$jv[3]. " ";
				else
					$stmt .= " LEFT JOIN " . $jv[0] . " USING ( " . $jv[1] . " ) ";			}
		}

		if (is_array($this->whereClause) && count($this->whereClause)) {
			$stmt .= " WHERE ";
			$where = array();
			foreach ($this->whereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$stmt .= implode(" AND ", $where);
		}

		if (is_array($this->groupClause) && count($this->groupClause)) {
			$stmt .= " GROUP BY ";
			$grby = array();
			foreach ($this->groupClause as $gk => $gv) {
		//		$params['key'.$i] = $gv[2];
				$grby[] = $gv;
		//		$i++;
			}
			$stmt .= implode(" , ", $grby);
		}

		if (is_array($this->orWhereClause) && count($this->orWhereClause)) {
			$stmt .= " OR ";
			$where = array();
			foreach ($this->orWhereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$stmt .= implode(" AND ", $where);
		}
		if (is_array($this->orderby) && count($this->orderby)) {
			$stmt .= " ORDER BY ";
			$i = 0;
			$orderlist = array();
			foreach ($this->orderby as $ord) {
				$orderlist[] = $ord." ".$this->direction[$i];
				$i++;
			}
			$stmt .= implode(",", $orderlist);
		}

		if (isset($this->limit)) {
//			$stmt .= " LIMIT :limit, :offset";
//			$params['offset'] = $this->offset;
//			$params['limit'] = $this->limit;
			// for some reason cannot bind as INT
			$stmt .= " LIMIT ".$this->offset.", ".$this->limit;
		}

		$this->data = $db->fetch_assoc($stmt, $params);
		if(is_array($this->data)) {
			foreach($this->data as $rowid => $drow) {
				//belongsTo
				if (isset($this->belongs_to) && count($this->belongs_to)) {
					foreach ($this->belongs_to as $bt) {
						$bel = new $bt[1];
						$fk = $bel->id_field;
						$bel->where($bel->id_field, '=', $this->data[$rowid][$bel->id_field])->get();
						$relname = $bt[0];
						$this->data[$rowid][$relname] = $bel->data[0];
					}
				}
                if (isset($formats) && is_array($formats)) {
                    foreach ($formats as $formatCol => $formatFunc) {
                        $this->data[$rowid][$formatCol] = $formatFunc($this->data[$rowid][$formatCol]);
                    }
                }
			}
		}

		return $this->data;
	}

	public function count()
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$stmt = "SELECT count(*) AS COUNT FROM ".$table;
		$params = array();

		$i = 1;
		if (is_array($this->whereClause) && count($this->whereClause)) {
			$stmt .= " WHERE ";
			$where = array();
			foreach ($this->whereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$stmt .= implode(" AND ", $where);
		}
		return (int)$db->fetch_one($stmt, $params)['COUNT'];
	}

	public function update($arg1, $arg2='')
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$newdata = array();
		if ($arg2 != '') {
			$newdata[$arg1] = $arg2;
		} elseif (is_array($arg1)) {
			foreach ($arg1 as $k => $v) {
				$newdata[][$k] = $v;
			}
		} else {
			return false;
		}

		$qry = "UPDATE ".$table." SET ";
		$params = array();

		$sets = array();
		$i = 1;

		foreach ($newdata as $nk => $nv) {
			$sets[$i] = $nk." = :value".$i;
			$params['value'.$i] = $nv;
		}
		$qry .= implode(" , ", $sets);

		if (is_array($this->whereClause) && count($this->whereClause)) {
			$qry .= " WHERE ";

			$where = array();
			foreach ($this->whereClause as $wk => $wv) {
				$params['key'.$i] = $wv[2];
				$where[] = $wv[0] . " " . $wv[1] . " :key" . $i;
				$i++;
			}
			$qry .= implode(" AND ", $where);
		}
		return $db->execute($qry, $params);
	}

	public function findOne($query = array())
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$query = $this->_replace_fieldnames($query);
		$key = key($query);

		$stmt = "SELECT * FROM ".$table." WHERE ".$key." = :key";
		$params = array('key' => $query[$key]);

		$row = $db->fetch_object($stmt, $params);

		$row = $this->_reverse_replace_fieldnames($row);

		if (!$row)
			return false;
		foreach ($row as $k => $v)
			$this->$k = $v;

		return true;
	}

	public function insert($data, $options=array())
	{
		return $this->insert_replace('INSERT', $data, $options);
	}

	public function replace($data, $options=array())
	{
		return $this->insert_replace('REPLACE', $data, $options);
	}


	public function insert_replace($mode, $data, $options=array())
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();
		common::ndebug(4,"GOT TABLE NAME %s", $table);

		$data = $this->_replace_fieldnames($data);

		$cols =  array();
		$params = array();
		$plhold = array();
		$count = 1;
		foreach ($data as $k => $v) {
			if (!in_array($k, $this->editable_fields) && Input::has($k))
				continue;
			$cols[] = $k;
			$params[$k] = $v;
			$plhold[] = ":".$k;
			$count++;
		}
		if ($this->createtime_field != '') {
			$now = New Datetime("NOW");
			$params[$this->createtime_field] = $now;
			$plhold[] = ":".$this->createtime_field;
			$cols[] = $this->createtime_field;
			$count++;
		}
		if ($this->updatetime_field != '') {
			$now = New Datetime("NOW");
			$params[$this->updatetime_field] = $now;
			$plhold[] = ":".$this->updatetime_field;
			$cols[] = $this->updatetime_field;
			$count++;
		}
		if (count($params) == 0) {
			common::ndebug(4, "Cannot Insert. The class which owns %s table does not have editable_fields property", $this->table);
			$this->error = "Database error. No params";
			return false;
		}
		$cc = implode(",", $cols);
		$pl = implode(",", $plhold);

		// options
		$ignore = '';
		foreach($options as $optname => $optval) {
			if ($optname == 'ignore') {
				$ignore = "IGNORE";
 			}
		}

		$stmt = $mode." ".$ignore." INTO ".$table." (".$cc.") VALUES (".$pl.")";
		$res = $db->execute($stmt, $params);
		if ($res) {
			if ($mode == 'INSERT') {
				$this->last_id = $db->last_id();
				$idf = $this->id_field;
				if ($idf)
					$this->$idf = $this->last_id;

			}
			return true;
		}
		$this->error = $db->error;
		return false;
	}

	/* update model with $newdata */
	public function save($newdata = array())
	{
		if (!count($newdata)) {
			common::ndebug(2, "model::save: no fields to update or allowed");
			$this->error = "Save: no fields to update or allowed";
			return false;
		}
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$newdata = $this->_replace_fieldnames($newdata);

		$params = array();
		$id_field = $this->id_field;
		$where = " WHERE ".$id_field." = :key";
		$id_field = $this->id_field;
		$params['key'] = $this->$id_field;

		$qry = '';
		$i = 1;
		foreach ($newdata as $key=> $datarow) {
			$sets[$i] = $key." = :value".$i;
			$params['value'.$i] = $datarow;
			$i++;
		}
		if ($this->updatetime_field != '') {
			$now = New Datetime("NOW");
			$fldname = $this->updatetime_field;
			$params[$fldname] = $now;
			$sets[$fldname] = $fldname." = :".$fldname;
		}
		$qry .= implode(" , ", $sets);


		$set = " SET ".$qry;

		$stmt = "UPDATE ".$table." ".$set." ".$where;
		$res = $db->execute($stmt, $params);
		if ($res) {
			return true;
		} else {
			$this->error = $db->error;
			return false;
		}

	}

	/* update all entries with $newdata */
	public function updateAll($newdata = array()) {
		if (!count($newdata)) {
			common::ndebug(2, "model::save: no fields to update or allowed");
			$this->error = "Save: no fields to update or allowed";
			return false;
		}
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$newdata = $this->_replace_fieldnames($newdata);

		$qry = '';
		$i = 1;
		foreach ($newdata as $key=> $datarow) {
			$sets[$i] = $key." = :value".$i;
			$params['value'.$i] = $datarow;
			$i++;
		}
		if ($this->updatetime_field != '') {
			$now = New Datetime("NOW");
			$fldname = $this->updatetime_field;
			$params[$fldname] = $now;
			$sets[$fldname] = $fldname." = :".$fldname;
		}
		$qry .= implode(" , ", $sets);


		$set = " SET ".$qry;

		$stmt = "UPDATE ".$table." ".$set;
		$res = $db->execute($stmt, $params);
		if ($res) {
			return true;
		} else {
			$this->error = $db->error;
			return false;
		}
	}

	/* remove entry from db */
	public function remove()
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$params = array();
		$id_field = $this->id_field;

		if ($this->use_soft_remove && !$this->status_field) {
			common::ndebug(2, "model::remove: Cannot use softremove on the object with no status field set");
			$this->error = "Cannot use softremove on the object with no status field set";
			return false;
		}

		$where = " WHERE ".$id_field." = :key";
		$params['key'] = $this->$id_field;

		if ($this->use_soft_remove) {
			// soft remove
			$status_field = $this->status_field;
			$tstamp = $this->removetime_field;
			$stmt = "UPDATE ".$table." SET ".$status_field."=NULL, ".$tstamp."=now() ".$where;
		} else {
			$stmt = "DELETE FROM ".$table." ".$where;

		}
		return $db->execute($stmt, $params);
	}


	/**
	 * @param array $attr1 - an array of conditions (field1=>val1, field2=>val2)
	 *                        or only field
	 *  @param mixed $attr2 - (optional) val if $attr1 is field
	 *  @return true on success, false on failure
     */
	public function removeMany($attr1, $attr2 = '')
	{
		$db = Dbconnect::getInstance();
		$table = $this->getTableName();

		$i = 1;
		$params = array();
		$where_part = array();

		$where = array();
		if (!is_array($attr1)) {
			$where[$attr1] = $attr2;
		} else {
			$where = $attr1;
		}

		foreach ($where as $key => $val) {
			$params['key'.$i] = $val;
			$where_part[$i] = $key."= :key".$i;
			$i++;
		}

		if (count($params) == 0) {
			common::ndebug(4, "Cannot Delete. No conditions supplied. Table %s", $table);
			$this->error = "Database error. No params";
			return false;
		}

		$where = " WHERE ".implode(" AND ", $where_part);
		$stmt = "DELETE FROM ".$table." ".$where;
		return $db->execute($stmt, $params);
	}

	public function authUser($user) {
		$fld = $this->owner_field;
		if($this->$fld == $user->account->id) {
			return true;
		} else {
			$this->error = "No Permission";
			return false;
		}
	}

	public function hasMany($child, $foreign_key='', $local_key='') {
		if (!class_exists($child, true)) {
			common::ndebug(2,"Class %s does not exist", $child);
			return false;
		}
		$name = debug_backtrace()[1]['function'];
		$this->has_many[] = array($name, $child, $foreign_key, $local_key);
		return $this;
	}

	public function hasOne($child, $foreign_key='', $local_key='') {
		if (!class_exists($child, true)) {
			common::ndebug(2,"Class %s does not exist", $child);
			return false;
		}
		$this->has_one[] = array($child, $foreign_key, $local_key);
		return $this;
	}

	public function belongsTo($parent, $foreign_key='', $local_key='') {
		if (!class_exists($parent, true)) {
			common::ndebug(2,"Class %s does not exist", $parent);
			return false;
		}
		$name = debug_backtrace()[1]['function'];
		$this->belongs_to[] = array($name, $parent, $foreign_key, $local_key);
		return $this;
	}

	public function belongsToMany($parent, $foreign_key='', $local_key='') {
		if (!class_exists($parent, true)) {
			common::ndebug(2,"Class %s does not exist", $parent);
			return false;
		}
		$name = debug_backtrace()[1]['function'];
		$this->belongs_to_many[] = array($name, $parent, $foreign_key, $local_key);
		return $this;
	}


	public function keyval($attr1, $attr2=false) {
		if (is_array($attr1)) {
			$key = $attr1[0];
			$val = $attr1[1];
		} elseif ($attr2) {
			$key = $attr1;
			$val = $attr2;
		} else {
			return false;
		}
		$ret = array();
		foreach($this->data as $k => $v) {
			$ret[$v[$key]] = $v[$val];
		}
		return $ret;
	}

	public function keyval_zero($attr1, $attr2=false) {
		if (is_array($attr1)) {
			$key = $attr1[0];
			$val = $attr1[1];
		} elseif ($attr2) {
			$key = $attr1;
			$val = $attr2;
		} else {
			return false;
		}
		$ret = array();
		$ret[0] = '-- None --';
		foreach($this->data as $k => $v) {
			$ret[$v[$key]] = $v[$val];
		}
		return $ret;
	}

	private function _replace_fieldnames($query) {
		if (!isset($this->fieldname_replaces))
			return $query;
		if (!is_array($this->fieldname_replaces))
			return $query;
		$replquery = array();

		foreach ($this->fieldname_replaces as $fld => $repl) {
			if (array_key_exists($fld, $query)) {
				$replquery[$repl] = $query[$fld];
			}
		}
		return $replquery;
	}

	private function _reverse_replace_fieldnames($row) {


		if (!isset($this->fieldname_replaces))
			return $row;
		if (!is_array($this->fieldname_replaces))
			return $row;

		foreach ($this->fieldname_replaces as $fld => $repl) {
			if (isset($row->$repl)) {
				$row->$fld = $row->$repl;
			} elseif (is_array($row) && $row[$repl]) {
				$row[$fld] = $row[$repl];
			}
		}
		return $row;
	}

}
