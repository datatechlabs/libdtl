<?php

class Dataset {

protected $table;
protected $id_field;
public $data;
	
public function __construct() {
	
	global $db;
}


public function getAll()
{
	global $db;

	$stmt = "SELECT * FROM ".$this->table;
	$params = array();

	$this->data = $db->fetch_assoc($stmt, $params);
	return $this->data;

}

public function getSome($stmt, $params)
{
	global $db;
	$assoc = $db->fetch_assoc($stmt, $params);
	return $assoc;
}

}

?>
