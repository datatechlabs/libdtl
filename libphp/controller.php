<?php

/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 18/01/2016
 * Time: 11:22
 */

class Controller
{
    public $error;
    public $error_fields = array();
    private $validation_result;

    public function route($id, $method=false)
    {
        if ($method)
            return $method;
        if (isset($_SERVER['REQUEST_METHOD'])) {
            $op = strtolower($_SERVER['REQUEST_METHOD']);
        } else {
            $op = false;
        }
        switch ($op) {
            case "options":
                $this->processOptions();
            break;
            case "get":
                if (!$id)
                    $method = "get_all";
                else
                    $method = "get";
                break;
            case "put":
            case "update":
                $method = "update";
                break;
            case "post":
            case "create":
                $method = "create";
                break;
            case "delete":
            case "remove":
                $method = "delete";
                break;
            default:
                $method = "staticpage";
        }
        return $method;
    }
    public function staticpage()
    {
        $page = new Template();
        $this->output = $page->fetch();
    }

    public function view ($data, $format='json', $response_code=200, $filename='')
    {
        http_response_code($response_code);
        switch ($format) {
            case "json":
                header('Access-Control-Allow-Origin: *'); 
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Allow-Headers: Content-Type');
                header('Content-Type: application/json; charset=UTF-8');
                print json_encode($data);
                break;
            case "json_export":
                ob_start();
                header('Content-Type: application/json; charset=UTF-8');
                header("Content-disposition: attachment; filename=".$filename);
                print json_encode($data);
                echo ob_get_clean();
                break;
            case "csv":
                ob_start();
                header('Content-Type: text/csv; charset=UTF-8');
                $df = fopen("php://output", 'w');
                fputcsv($df, $data['metadata']['columns']);
                foreach ($data['data'] as $row) {
                    fputcsv($df, $row);
                }
                fclose($df);
                echo ob_get_clean();
                break;
            default:
                header('Content-Type: application/json; charset=UTF-8');
                print json_encode($data);
        }
        exit(0);
    }

    public function validate(&$request, $rules, $updateone = false)
    {
        // combine data both from $_REQUEST and $_FILES input
        $allfields = array();
        if (array_key_exists('HTTP_CONTENT_TYPE', $_SERVER) && $_SERVER['HTTP_CONTENT_TYPE'] === 'application/json') {
			$json = file_get_contents('php://input');
			$res = json_decode($json, true);
			foreach($res as $k => $v) {
				$allfields[$k] = $v;
			}
		} 
        foreach ($_REQUEST as $rkey => $rval) {
            $allfields[$rkey] = $rval;
        }
        foreach ($_FILES as $fkey => $fval) {
            $allfields[$fkey] = $fval;
        }

        // $rules array field => rule
        foreach ($rules as $field => $ruleset) {
            if (array_key_exists($field, $allfields))
                $value = $allfields[$field];
            else
                $value = null;
            $this->_set_validation_result("undef"); // to start with

            common::ndebug(4,"OUTER LOOP. CHECKING FIELD %s VALUE %s RESULT AT START IS %s", $field, $value, $this->validation_result);
            // check for required fields
            list($cond, $opt) = explode("|", $ruleset);
            if (!array_key_exists($field, $request) && $cond=='required') {

                if ($updateone == true) // do not check if we only need to check those fields which are supplied
                    continue;

                $this->error = "Missing field: %s";
                $this->error_fields[] = $field;
                common::ndebug(4,"FIELD %s IS MISIING, EXITING", $field);
                $this->_post_filter($request, $rules);
                return false;
            }

            if (!array_key_exists($field, $request) && $cond=='optional') {
                common::ndebug(4,"OPTIONAL FIELD %s IS NOT SET, CONTINUE", $field);
                continue;
            }
            // also optional fields might be present but be empty. That should also pass
            if (array_key_exists($field, $request) && $cond=='optional' && $value==='') {
                common::ndebug(4,"OPTIONAL FIELD %s IS EMPTY, CONTINUE", $field);
                if ($opt == "integer") {
                    $request[$field] = null;
                }
                continue;
            }

            if (is_array($ruleset)) {
                // an array of rules. at least one must satisfy

                $satisfied = false;
                foreach ($ruleset as $item) {
                    if ($this->_satisfy_rule($item, $value, $field, $request)) {
                        common::ndebug(4," SATISFY RULE %s SATISFIED", $item);
                        $satisfied = true;
                   //     break;
                    }
                    common::ndebug(4,"====SATSIFIED %s", $satisfied);
                }
                if (!$satisfied) {
                    common::ndebug(4, " RULE %s NOT SATISFIED", $ruleset);
                    $this->_post_filter($request, $rules);
                    return false;
                }

            } else {
                // one rule. must satisfy

                if (!$this->_satisfy_rule($ruleset, $value, $field, $request)) {
                    $this->_post_filter($request, $rules);
                    return false;
                }
            }
            common::ndebug(4,"OUTER LOOP FIELD %s PASSED", $field);
        }
        $this->_post_filter($request, $rules);
        return true;
    }

    public function getMeta($request) {
        $meta = [];
        if (isset($request['filter'])) {
            $filter = json_decode($request['filter'], true);
            $meta['filter'] = $filter;
        }
        if (isset($request['range'])) {
            $range = json_decode($request['range'], true);
            $meta['range'] = [
                'from' => $range[0],
                'to' => $range[1]
            ];
        }
        return $meta;
    } 

    // filter out all fields which are not in the rules
    private function _post_filter(&$request, $rules)
    {
        foreach($request as $key => $val) {
            if ($key === "meta") {
                continue;
            }
            if (!array_key_exists($key, $rules))
                unset($request[$key]);
        }
        return $request;
    }

    private function _satisfy_rule($ruleset, $value, $field, &$request) {
        // break into rules by pipe
        $rule = explode("|", $ruleset);
        // check if it is optional and set optional flag
        $optional = false;
        foreach ($rule as $r) {
            $rulearr = $this->_parse_rule($r);
            if ($rulearr['type'] === 'optional') {
                $optional = true;
                common::ndebug(4, "  Rule is optional");
            }
        }
        // all must satisfy to get true
        foreach ($rule as $r) {
            $rulearr = $this->_parse_rule($r);
            common::ndebug(4, "  FIELD is %s VALUE IS %s RULE IS %s", $field, $value, $r);
            $this->_validate_rule($rulearr, $value, $field, $request);
            if ($this->validation_result != "pass" && !$optional) {
                common::ndebug(4, "  ERROR %s", $this->error);
                common::ndebug(4, "  PIPED SET %s FAILED", $ruleset);
                return false;
            }

        }
        common::ndebug(4, "  PIPED SET %s PASSED", $ruleset);
        return true;
    }

    private function _validate_rule($rulearr, $value, $field, &$request)
    {
        common::ndebug(4,"   VALIDATING rule %s", $rulearr['type']);
        switch ($rulearr['type']) {
            case "string":
                if (!$this->_is_string($value, $field, $request)) {
                    $this->error = "%s must be a string";
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                    $request[$field] = (string)$value;
                }
                break;
            case "integer":
                if (!preg_match('/^[-+]?\d+$/',$value)) {
                    $this->error = "%s must be integer";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $request[$field] = (int)$value;
                    $this->_set_validation_result("pass");
                }
                break;
            case "decimal":
                if (!$this->_is_decimal($value, $field, $request)) {
                    $this->error = "%s must be decimal";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "email":
                if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',$value)) {
                    $this->error = "%s must be email address";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $request[$field] = (string)$value;
                    $this->_set_validation_result("pass");
                }
                break;
            case "boolean":
                if (!$this->_is_boolean($value, $field, $request)) {
                    $this->error = "%s must be boolean";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    if ($value == '1' || $value == 'true' || $value == 'yes')
                        $request[$field] = 1;
                    else
                        $request[$field] = 0;
                    $this->_set_validation_result("pass");
                }
                break;
            case "array":
                if (gettype($value) != "array") {
                    $this->error = "%s must be array";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "datetime":
                if (!$this->_is_datetime($value, $rulearr, $field, $request)) {
                    $this->error = "%s must be datetime";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "date":
                if (!$this->_is_date($value, $rulearr, $field, $request)) {
                    $this->error = "%s must be date";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "in":
                if (!$this->_validate_in($value, $rulearr)) {
                    $this->error = "%s is not in allowed range";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "required_if":
                $this->_validate_required_if($value, $rulearr, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "This field is required";
                }
                break;
            case "integer_if":
                $this->_validate_integer_if($value, $rulearr, $field, $request);
                 if ($this->validation_result == "fail") {
                     $this->error_fields[] = $field;
                    $this->error = "%s must be integer";
                 }
                break;
            case "string_if":
                $this->_validate_string_if($value, $rulearr, $field, $request);
                 if ($this->validation_result == "fail") {
                     $this->error_fields[] = $field;
                     $this->error = "%s must be string";
                 }
                break;
            case "required":
                $type = gettype($value);
                if ($type == "array" && count($value) > 0)
                    $res = true;
                elseif ($type === "boolean")
                    $res = true;
                elseif (strlen($value) > 0)
                    $res = true;
                else
                    $res = false;

                if (!$res) {
                    $this->error = "%s is required";
                    $this->error_fields[] = $field;
                    $this->_set_validation_result("fail");
                } else {
                    $this->_set_validation_result("pass");
                }
                break;
            case "optional":
                $this->_set_validation_result("pass");
                break;
            case "image":
                $this->_validate_image($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be image";
                }
                break;
            case "csv":
                $this->_validate_csv($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be csv";
                }
                break;
            case "json":
                $this->_validate_json($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be JSON";
                }
                break;
            case "audio":
                $this->_validate_audio($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be audio";
                }
                break;
            case "max":
                $this->_validate_maxsize($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be shorter or equal than ".$rulearr['cond'][0];
                }
                break;
            case "min":
                $this->_validate_minsize($value, $rulearr, $field, $request);
                if ($this->validation_result == "fail") {
                    $this->error_fields[] = $field;
                    $this->error = "%s must be longer or equal than ".$rulearr['cond'][0];
                }
                break;
            default:
                $this->error = "%s is unknown type";
                $this->error_fields[] = $field;
                $this->_set_validation_result("fail");
        }
        common::ndebug(4,"   RULEGROUP DONE WITH RESULT %s" , $this->validation_result);
    }

    private function _is_decimal($value, $field, &$request) {
        if(!preg_match('/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/', $value)) {
            common::ndebug(4, "DECIMAL CHECK FAILED");
            return false;
        }
        return true;
    }

    private function _is_boolean($value, $field, &$request) {
        $type = gettype($value);
        if ($type === "boolean")
            return true;
        if (!preg_match('/^0|1|yes|no|true|false$/',$value)) {
            common::ndebug(4, "BOOLEAN CHECK FAILED");
            return false;
        }
        $request[$field] = (int)$value;
        return true;

    }

    private function _is_integer($value, $field, &$request) {
        if (!preg_match('/^\d+$/',$value)) {
            common::ndebug(4, "INTEGER CHECK FAILED");
            return false;
        }
        $request[$field] = (int)$value;
        return true;

    }

    private function _is_string($value, $field, &$request) {
        if (!is_string($value)) {
            common::ndebug(4, "STRING CHECK FAILED");
            return false;
        }
        $request[$field] = (string)$value;
        return true;

    }

    private function _is_datetime($value, $rulearr, $field, &$request) {
        try {
            $date = new DateTime($value);
        } catch (Exception $e) {
            // try to throw away tz part after whitespace
            try {
                $dt = explode(" ", $value)[0];
                $date = new DateTime($dt);
            } catch (Exception $e) {
                return false;
            }
        }
        $request[$field] = $date->format('Y-m-d H:i:s');
        return true;
    }

    private function _is_date($value, $rulearr, $field, &$request) {
        try {
            $date = new DateTime($value);
        } catch (Exception $e) {
            return false;
        }
        $request[$field] = $date->format('Y-m-d');
        return true;
    }

    private function _parse_rule($rulestr) {
        $a = explode(":", $rulestr);
        if (count($a) == 1) {
            return array('type' => $a[0]);
        }
        $b = explode(",", $a[1]);
        return array('type' => $a[0], 'cond' => $b);
    }

    private function _validate_in($what, $where) {
        foreach($where['cond'] as $w) {
            if ($what == $w)
                return true;
        }
        return false;
    }

    private function _validate_required_if($what, $where, $request)
    {
        // the condition is that $request[$where[0]] == $where[1]
common::ndebug(4,"WHERE %s WHAT %s", $where, $what);
        if ($request[$where['cond'][0]] != $where['cond'][1]) {
            $this->_set_validation_result("nochange"); // because this is not a field concerned
            return;
        }
        if (strlen($what) == 0) {
            common::ndebug(4,"_validate_required_if FAILED because %s is empty", $what);
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _validate_integer_if($what, $where, $field, $request)
    {
        // the condition is that $request[$where[0]] == $where[1]
        if ($request[$where['cond'][0]] != $where['cond'][1]) {
            $this->_set_validation_result("nochange");
            return;
        }
        if (!$this->_is_integer($what, $field, $request)) {
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _validate_string_if($what, $where, $field, $request)
    {
        // the condition is that $request[$where[0]] == $where[1]
        if ($request[$where['cond'][0]] != $where['cond'][1]) {
            $this->_set_validation_result("nochange");
            return;
        }
        if (!$this->_is_string($what, $field, $request)) {
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _validate_image($what, $where, $field, $request)
    {
        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
        $detectedType = exif_imagetype($what['tmp_name']);
        if(!in_array($detectedType, $allowedTypes)) {
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }
    private function _validate_csv($what, $where, $field, $request)
    {
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv','application/octet-stream');
        if(!in_array($what['type'], $mimes)){
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }
    private function _validate_json($what, $where, $field, $request)
    {
        $mimes = array('application/json');
        if(!in_array($what['type'], $mimes)){
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }
    private function _validate_audio($what, $where, $field, $request)
    {
        $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv','application/octet-stream');
        if(!substr($what['type'], 0, 6) == 'audio/'){
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _validate_maxsize($what, $where, $field, $request)
    {
        if (is_string($what)) {
            if (strlen($what) > $where['cond'][0] ) {
                $this->_set_validation_result("fail");
                return;
            }
        } elseif (filesize($what['tmp_name']) > $where['cond'][0]) {
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _validate_minsize($what, $where, $field, $request)
    {
        if (is_string($what)) {
            if (strlen($what) < $where['cond'][0] ) {
                $this->_set_validation_result("fail");
                return;
            }
        } elseif (filesize($what['tmp_name']) < $where['cond'][0]) {
            $this->_set_validation_result("fail");
            return;
        }
        $this->_set_validation_result("pass");
    }

    private function _set_validation_result($result)
    {
        // set the result of validation
        switch ($result) {
            case "undef": // starting state
                $this->validation_result = "undef";
            case "nochange":
                break;
            case "fail":
                common::ndebug(4,"SET VALIDATION RESULT IS NOW FAIL");
                $this->validation_result = "fail";
                break;
            case "pass":
                // cannot pass if it is already failed
                if ( $this->validation_result != "fail")
                    $this->validation_result = "pass";
                break;
            default:
                $this->error = "Validator returned invalid result";
        }
    }

    private function processOptions() {
        header('Access-Control-Allow-Origin: *'); 
        header('Access-Control-Allow-Headers: Content-Type'); 
        header('Allow: OPTIONS, GET, PUT, POST, DELETE');
        exit(0);        
    }
}