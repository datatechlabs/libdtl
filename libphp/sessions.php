<?php

/*
 * class sessions	
 * manipulates user sessions
 */

common::require_class("session.php");

class sessions {

/*
 * Create a web session.
 */
static function create($webacc)
{
	global $db;

	/* Create a session. */
	$sessid = common::rand_string();

	$session = new Session();
    $now = New Datetime("NOW");

    $query = array(
		'session_id' => $sessid,
		'user_id' => $webacc->user_id,
		'ip_address' => common::get_ip(),
		'create_time' => $now,
		'last_access_time' => $now,
		'area' => settings::get_value('area', 'MISC', '')
		);
	$session->insert($query);
	
	/* Set session ID value in a cookie. */
	setcookie('sessid', $sessid, 0, '/');
	
	/* set site cookie */
	$site = array_key_exists('site', $_POST) ? $_POST['site'] : false;
	if (!$site) // we may need to provide GET method if role switching from root interface
		$site = array_key_exists('site', $_GET) ? $_GET['site'] : "";
	setcookie('DTL_SITE', $site, 0, '/');
	return $sessid;
}

static function delete()
{
	global $db;
	$sessid = sessions::submitted_sessid();
	if (!$sessid)
		return false;

	/* Clear the session cookie. */
	setcookie('sessid', '', 0, '/');

	$sess = New Session();
	$id = array('session_id' => $sessid);
	$sess->findOne($id);
	
	$sess->remove();
	
	return true;
}

static function submitted_sessid()
{	
//	if (array_key_exists('sessid', $_COOKIE))
//		return $_COOKIE['sessid'];

	if (array_key_exists('authToken', $_POST))
		return $_POST['authToken'];

	if (array_key_exists('authToken', $_GET))
		return $_GET['authToken'];
	
	if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
		$authHeader = $_SERVER['HTTP_AUTHORIZATION'];
		if (strtolower(substr($authHeader, 0, 5)) === 'token') {
			return substr($authHeader,6);
		}
	}

	return false;
}

static function session_by_id($sessid)
{
	$sess = New Session();
	
	$query = array('session_id' => $sessid);

	if(!$sess->findOne($query))
		return false;

//	if($sess->area != settings::get_value('area', 'MISC', ''))
//		return false;
	return $sess;
}

static function valid()
{
	$sessid = sessions::submitted_sessid();
	if (!$sessid)
		return false;
	/* See if the session ID corresponds to something. */
	
	$session = sessions::session_by_id($sessid);
	if (!$session)
		return false;

	/* Compare IP addresses. */
//	if (common::get_ip() !== $session['ATRIPADDR'])
//		return false;
	return true;
}

/*
 * Set the 'data' column in the session table
 * returns: true on success, false otherwise
 */
static function store_data($data_str)
{
	$sessid = sessions::submitted_sessid();
	if (!$sessid)
		return false;

	$area = settings::get_value('area', 'MISC');
	if ($area == "admin") 
		$sesstable = "Websessions";
	else
		return false;
		
	$data_str = db::escape($data_str, 1024);

	/* Make sure such a session exists. */
	$session = sessions::session_by_id($sessid);
	if (!$session)
		return false;

	$sql = "update $sesstable SET
			data = '$data_str'
		where session_id = '$sessid'";
	if (!db::execute($sql)) {
		echo 'Session error (session::store_data).';
		exit(1);
	}
	return true;		
}

/*
 * get data that was previously stored in a user's session
 * returns: data string
 */
static function retrieve_data()	
{
	$sessid = sessions::submitted_sessid();
	if (!$sessid)
		return false;
	
	$session = sessions::session_by_id($sessid);
	if (!$session) {
		echo 'Session error (sessions::retrieve_data).';
		exit(1);
	}

	return $session['data'];
}


/*
 * Delete expired sessions
 */
static function delete_old()
{
	$sessexp = (int)settings::get_value('session_timeout', 'SECURITY', '600');
	$dt = New Datetime("NOW");
    $int = New DateInterval("PT".$sessexp."S");
    $dt->sub($int);
	$sess = New Session();
	$sess->delete_older_than($dt);
}

/*
 * Update session last modification time
 */
static function update()
{	
	$sessid = sessions::submitted_sessid();

	$id = array('session_id' => $sessid);
    $now = New Datetime("NOW");

    $newdata = array('last_access_time' => $now);

	$sess = New Session();
	$sess->findOne($id);
	$sess->save($newdata);
	
	setcookie('sessid', $sessid, 0, '/');
	if (array_key_exists('DTL_STYLE', $_COOKIE)) {
		setcookie('DTL_STYLE', $_COOKIE['DTL_STYLE'], 0, '/');
	}
}

}

?>
