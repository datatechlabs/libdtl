<?php


require_once("$libpath/model.php");
require_once("$libpath/dataset.php");
require_once("$libpath/controller.php");
require_once("$libpath/input.php");

require_once("$libpath/utils/common.php");

require_once("$libpath/template.php");
require_once("$libpath/sessions.php");
require_once("$libpath/utils/image.php");
require_once("$libpath/blob.php");
require_once("$libpath/mail.php");
require_once("$libpath/crypto.php");


Class System {

static function init($config_file)
{

	$libdtl_root = settings::get_value("libdtl_root", "PATHS");
	$libpath = "$libdtl_root/libphp";

	global $db;

	// set the error reporting levels
	
	if (settings::get_value('report_php_errors', 'DEBUG') == 2) 
		error_reporting(E_ALL);
	elseif (settings::get_value('report_php_errors', 'DEBUG') == 1) 
		error_reporting(E_ALL ^ E_NOTICE);
	else 
		error_reporting(0);

	$db_engine = settings::get_value('engine', 'SQL');
	if ($db_engine == "mysql") {
		require_once("$libpath/dbconnect.php");

	} elseif ($db_engine == "postgres") {
		require_once("$libpath/dbconnect.php");
		
	} else {
		die("Unsupported db engine");
	}

	// set default time zone from config file
	$timezone = settings::get_value('timezone', 'GENERAL', 'GMT');
	date_default_timezone_set($timezone);
}

/*
 * This should be called whenever something really bad happens.
 * It just outputs the error message and terminates script execution
 */
static function fatal_error($error)
{
	echo 'fatal error: ' . $error;
	exit(1);
}

/*
 * This should be called whenever something really bad happens.
 * It just outputs the warning message but does not terminate script execution
 */
static function critical_error($error)
{
	common::ndebug(1,'CRITICAL ERROR: ' . $error, 0);
}

/*
 * This should be called on minor issues.
 * It just outputs the warning message but does not terminate script execution
 */
static function warning($error)
{
	common::ndebug(2,'WARNING: ' . $error, 0);
}

}

?>
