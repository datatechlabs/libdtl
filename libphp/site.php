<?php

/*
 * This class has functions for creating the actual html document,
 * Modify it to change menu and site appearance/behavior
 */

//require_once("$libpath/dbstructs/globalparam.php");

common::require_class("globalparam.php");

class site {

// store file names scheduled for inclusion in the main document
var $files = array();

var $jscript = '';		// accumulate javascript
var $item_map = array();	// menu item translations

/* Permissions that are currently used somewhere on the site and per area. */
var $permissions_admin = array(
	'products',
	'pos_products',   // XXX-move to module
	'rates',
	'routes',
	'accounts',
	'account_groups',
	'resellers',
	'callshops',
	'access',
	'carriers',
	'reports',
	'my_webacct',
	'data_sources',
	'settings',
	'payments',
	'tools',
	'vouchers',
	'admin',
	'dids',
	'pins'
);

var $permissions_reseller = array(
	'products',
	'pos_products',    // XXX-move to module
	'rates',
	'accounts',
	'account_groups',
	'resellers',
	'callshops',
	'access',
	'reports',
	'my_webacct',
	'settings',
	'payments',
	'pins',
	'vouchers',
	'pay_online',
	'active_calls'
);

var $menu_selection = array();
var $current_page = 'none';

static function init()
{
	global $g_site, $area, $g_user, $g_settings;
	$area = settings::get_value('area', 'MISC');
	if (!$area)
		$area = "public";
	// we need to know users language preference to pick correct menu translation 
	$template_path = settings::get_value('templates', 'PATHS'); 
	$root = settings::get_simple_path('package_root');
	$languages = common::get_languages();
	$route_file = settings::get_simple_path('routes');
	if (!is_file($route_file)) {
		common::ndebug(1, "Cannot find route file at %s", $route_file);
		system::fatal_error("Configuration error. Check log file");
	}
	require_once($route_file);
	
	$g_site = new site;
	$g_site->files['css'] = array();
	$g_site->files['js'] = array();
	$g_site->files['js_append'] = array();


//	require_once("$template_path/other/{$area}-menu.php");
//	$g_site->item_map = $translations;
	$g_site->params = site::get_params();

	// load appropriate menu definition
	//if ($area == '')
	//	$g_site->menu = $routes['global'];
	//else
		$g_site->menu = $routes;

	$g_site->permissions = $g_site->permissions_admin;  // XXX


	// topmenu (for text decorations via css)
	$jj = 0;
	$topmenu = array();
	if (is_array($g_site->menu)) {
		foreach ($g_site->menu as $ikey => $idata) {
			$jj++;
			$topmenu[$idata[0]] = $jj;
		}
	}
	$g_site->topmenu = $topmenu;

	/* load any modules which are enabled */
	$modules = settings::get_value('modules', 'PATHS');
	$modlist = explode(",", $modules);
	foreach($modlist as $mod) {
		// require appropriate module conf file
		$mod_conf_file = settings::get_value("mod_".$mod."_conf", 'PATHS');
		if (is_file($mod_conf_file)) {
			$mod_set = config::dtl_parse_ini_file($mod_conf_file);
			foreach ($mod_set['PATHS'] as $name => $val) {
				settings::set_value($val, $name, 'mod_'.$mod);
			}
			$module_base = settings::get_value('module_base', "mod_".$mod);
			$mod_menu = settings::get_value('module_menu', "mod_".$mod);

			// append to menu
			if (file_exists($module_base."/".$mod_menu))
				$mod_menu_file = require_once($module_base."/".$mod_menu);
			if (is_array($g_site->menu) && is_array($menu_admin_module))
				$g_site->menu = array_merge(array_slice($g_site->menu,0,6,true), $menu_admin_module,array_slice($g_site->menu,6,7,true));

			$template_path = settings::get_value('template_path', 'PATHS'); 
			
			// append menu translations provided by module
			$mod_translations = settings::get_value('module_translations', "mod_".$mod);
			$module_item_map = parse_ini_file($module_base."/".$mod_translations, true);

			if (is_array($g_site->item_map['top']) && is_array($module_item_map['top']))
				$g_site->item_map['top'] = array_merge($g_site->item_map['top'], $module_item_map['top']);
			if (is_array($g_site->item_map['sub']) && is_array($module_item_map['sub']))
				$g_site->item_map['sub'] = array_merge($g_site->item_map['sub'], $module_item_map['sub']);
			if (is_array($g_site->item_map['tabs']) && is_array($module_item_map['tabs']))
				$g_site->item_map['tabs'] = array_merge($g_site->item_map['tabs'], $module_item_map['tabs']);
			
			// append permissions provided by module
			$g_site->permissions = array_merge($g_site->permissions, $permissions_module);
		}
	}
}


	static function init_client()
	{
		global $g_site;
		$menu_arr = array();
		for ($i = 0; $i < sizeof($g_site->menu); $i++) {
			$item = $g_site->menu[$i];
			site::create_item($menu_arr, $item, 0, '');
		}
		header('Content-Type: application/json');
		print json_encode($menu_arr);
	}

/* Section names in menu item translation file (*-menu.txt). */
var $trans_sections = array('top', 'sub', 'actions', 'tabs');

/*
 * For a menu item to be rendered, at least one of its children or the item
 * itself, if it's a terminal node, needs to be accessible.
 */
static function is_accessible($item)
{
	global $g_user;
	/* If has children, go through them recursively. */
	if (is_array($item[1])) {
		$children = $item[1];
		foreach ($children as $child) {
			if (site::is_accessible($child))
				return true;
		}
		return false;
	}

	/* Terminal node = no children. */
	$perm_name = $item[3];
	$perm_value = $item[4];
	$access = (int)$g_user->web_access[$perm_name];
	if ($access >= $perm_value)
		return true;
		
	return false;
}

static function create_item(&$menu_arr, $item, $level, $parent_link)
{
	global $g_site, $menu_selection;
	$a_attrs = '';
	$txt_attrs = '';


	if (!site::is_accessible($item))
		return;
	$base = common::base_url();
	$area = settings::get_value('area', 'MISC');


	/* Translate item name. */
	$section = $g_site->trans_sections[$level];
	$name = $g_site->item_map[$section][$item[0]][0];
	$icon = $g_site->item_map[$section][$item[0]][1];

	if (!$name)
		$name = 'X ' . $item[0];

	$href = "$parent_link/{$item[0]}";

	/* if has children && (first child has children || first child is visible) */
	if (is_array($item[1]) && (is_array($item[1][0][1]) || $item[1][0][2] == 1)) {

		$children = $item[1];
		$entry = array('name' => $name, 'link' => $base.'/'.$area.$href, 'icon' => $icon, 'children' => array());
		$menu_arr[] = $entry;

		for ($i = 0; $i < sizeof($children); $i++)
			site::create_item($menu_arr, $children[$i], $level+1, $href);

		return;
	}

	if (array_key_exists(2, $item) )
		if ($level > 0 && $item[2] ===0)
			return;

	end($menu_arr);
	$key = key($menu_arr);

	$entry = array('name' => $name, 'link' => $base.'/'.$area.$href, 'icon' => $icon);
	$menu_arr[$key]['children'][] = $entry;
}

static function generate($scriptout)
{
	global $g_site, $g_user;
	
	$area = settings::get_value('area', 'MISC');
	$sitename = settings::get_value('site', 'MISC');

	$sitefile = $area.'/site.tpl';

	$site = new Template($sitefile);
	
	$site->assign('USERNAME', $g_user->webacc->web_login);
	$site->assign('LOGIN_TIME', common::fmt_date($g_user->session->create_time, DF_FULL, false, true));
	$site->assign('LOGIN_ADDRESS', $g_user->session->ip_address);
	$site->assign('BASE_URL', common::base_url());
	$site->assign('LIB_URL',common::lib_url());
	$site->assign('SITE', $sitename);
	$site->assign('SYSTEM_MESSAGE_CLASS', '');
	$site->assign('SYSTEM_MESSAGE', '');
	if (isset($g_site->system_message_text)) {
		$site->assign('SYSTEM_MESSAGE', $g_site->system_message_text);
		if ($g_site->system_message_type == 0)
			$site->assign('SYSTEM_MESSAGE_CLASS', 'info');
		else
			$site->assign('SYSTEM_MESSAGE_CLASS', 'error');
	}

	$site->assign('MAIN', $scriptout);



	/* Add logout link. */
	$site->assign('LOGOUT_LINK', site::link('/logout'));

	$csslink = '<link rel="stylesheet" type="text/css" href="%" />';
	$jslink = '<script type="%s" src="%s?v=%s"></script>';

	$version = $g_site->params['product_buildnumber'];
	$site->assign('DOCUMENTLINKS', '');
	foreach ($g_site->files['css'] as $cssfile)
		$site->append('DOCUMENTLINKS',
		    str_replace('%', $cssfile, $csslink));
		
	foreach ($g_site->files['js'] as $jsfile)
		$site->append('DOCUMENTLINKS',
		    sprintf($jslink, $jsfile[1], $jsfile[0], $version));
		
	$site->assign('JSCRIPT', $g_site->jscript);
	return $site->fetch();
}

/*
 * Figure out script path from page URL by traversing site's menu structure.
 */
static function pagescript_path()
{
	global $g_site, $menu_selection;
	$area = settings::get_value('area', 'MISC');
	$dir = settings::get_simple_path('controllers');

	$id = false;
	$sub_ids = array();

	if (array_key_exists('script', $_GET)) {
		$page = $_GET['script'];
	} else {
		$page = '/home';
	}

	if ($page == '/logout') {
		return array(true, site::logout());
	}
	if ($page == '/ping') {
		return array(true, site::ping());
	}
	/* XXX: strip prefix here. */
	$page = rtrim($page, '/');
	$item_names = explode('/', $page);
	$name_index = 1;
	$menu = $g_site->menu;
	for ($i = 0; $i < sizeof($menu);) {
		$item = $menu[$i++];
		$name = $item[0];
		if (($name == '{id}') && (strlen($item_names[$name_index]) > 0)) {
			$name = $item_names[$name_index];
			if ($name_index == (count($item_names) - 1)) {
				$id = $name;
			} else {
				$sub_ids[] = $name;
			}
		}

		if ($name != $item_names[$name_index])
			continue;
		$name_index++;
		$menu_selection[] = $name;
		/* if has children */
		if (is_array($item[1])) {
			if (!array_key_exists($name_index, $item_names)) {
				/* Find first child. */
				while (is_array($item[1])) {
					$item = $item[1][0];
					$menu_selection[] = $item[0];
				}
			} else {
				$i = 0;
				$menu = $item[1];
				continue;
			}
		}
		// detect if current page is module or generic page
		foreach ($g_site->menu as $l_id => $base_lvl) {
			if ($base_lvl[0] == $menu_selection[0]) {
				if (isset($base_lvl[2]) && $base_lvl[2]!==0) {
					// we are in module menu item
					$modbase = settings::get_value('module_base', "mod_".$base_lvl[2]);
					$moddir = settings::get_value('module_scriptbase', "mod_".$base_lvl[2]);
					$dir = $modbase."/".$moddir;
					settings::set_value($base_lvl[2], 'active_module', 'MISC');
				}
			}
		}
		$g_site->current_page = '/' . implode('/', $menu_selection);
		$g_site->breadcrumb = $menu_selection;
		if (!array_key_exists(5, $item)) {
			common::ndebug(2, "site::pagescript_path. Route %s is misconfigured. Missing class name", $item);
			return array(true, array("$dir/{$item[1]}", "", $id, $sub_ids, "{$item[6]}"));
		}
		// permissions check
		if ($item_names[1] != $area) {
			common::ndebug(4,"item  %s", $item);
			if (isset($item[7]) && ($item[7]) == $area) {
				common::ndebug(4, "Route allows area %s override to area %s", $area, $item_names[1]);
				settings::set_value($item_names[1], 'area', 'MISC');
			} else {
				common::ndebug(4, "Area not allowed. Asked %s but must be %s", $area, $item_names[1]);
				return array(false, 401);
			}
		}

		if (!array_key_exists(6, $item)) {
			return array(true, array("$dir/{$item[1]}", "{$item[5]}", $id, $sub_ids, ""));
		} else {
			return array(true, array("$dir/{$item[1]}", "{$item[5]}", $id, $sub_ids, "{$item[6]}"));
		}
	}
}


/*
 * breadcrumb navigation link 
 */

static function breadcrumb($delimiter = ' > ', $class='breadcrumb', $escaped=true)
{
	global $g_site;

	$parts = $g_site->breadcrumb;

	$target = array();
	foreach ($parts as $level => $part) {
		$str = $g_site->item_map[$g_site->trans_sections[$level]][$parts[$level]];
		if (strlen($str) < 1)
			continue;
		$target[$level] = $part;
		$link = site::link('/'.implode('/', $target));
		if ($escaped)
			$trans[$level] = '<a class=\"'.$class.'\" href=\"'.$link.'\">'.$str.'</a>';
		else
			$trans[$level] = '<a class="'.$class.'" href="'.$link.'">'.$str.'</a>';
	}
	return implode($delimiter, $trans);
}

/*
 * Provide a link to another page.
 */
static function link($page=false, $vars=array(), $html=true)
{
	/* Do this to start the url correctly with a question mark. */
	reset($vars);		
	$key = key($vars);
	$value = current($vars);
	$url = common::base_url().$page;
	unset($vars[$key]);
					
	foreach ($vars as $key => $val) {
		$url .= "&$key=$val";
	}
	if ($html)
		return htmlspecialchars($url);
	return $url;
}

/*
 * Output location header to do a redirect.
 */
static function refresh($page, $linkvars=array())
{
	header('Location: ' . site::link($page, $linkvars, false));
	exit(0);
}

/*
 * Execute script that displays login page
 */	
static function login_page($msg = '', $passrecover = false)
{
	global $g_site;
	global $libpath;
	$area = settings::get_value('area', 'MISC');

	// get login script path
	$pathname = "{$area}_loginpage";
	

	list($loginscript, $class) = site::pagescript_path();
	if (!file_exists($loginscript)) {
		print('no login script');
		exit(1);
	}
	require($loginscript);
	return execute($msg);
}


	static function logout() {
		global $g_user;
		sessions::delete();

		$accesslog = New AccessLog();
		$accesslog->append('sec', 'login', 'Successful Logout', $g_user);
		http_response_code(200);
		exit(0);
	}

	static function ping() {
		http_response_code(200);
		exit(0);
	}

/*
 * determines where the current script resides
 */
static function scriptfile()
{
	global $g_site;
	$area = settings::get_value('area', 'MISC');
	for ($i = 1; $i <= 4; $i++) {
		$path = settings::get_path("{$area}_scriptfile_$i");
		if (file_exists($path)) 
			return $path;
	}

	return false;
}

/* functions that can be called by page scripts */

/*
 * append javascript to the final page
 */
static function append_js($js)
{
	global $g_site;
	$g_site->jscript .= $js;
}
	
// add a css style sheet to the document.
static function add_stylesheet($filename)
{
	global $g_site;	
	$g_site->files['css'][] = $filename;
}
	
// add a javascript file	
static function add_jsfile($filename, $type="text/javascript")
{
	global $g_site;
	$g_site->files['js'][] = array($filename, $type);
}

// retrieves site-specific parameters
static function get_params() {
	$gparams = New GlobalParam();
	$gparams->getAll();
	$params = array();
	foreach ($gparams->data as $k => $val) {

		if (!$val['section'])
			$params[$val['name']] = $val['value'];
		else
			$params[$val['section']][$val['name']] = $val['value'];
	}
	return $params;
}

static function local_menu($pages='', $vars=array())
{
	

	global $g_site, $area;
	switch ($area) {
	case 'admin':
		global $submenu_admin;
		$submenu = $submenu_admin;
		break;
	case 'reseller':
		global $submenu_reseller;
		$submenu = $submenu_reseller;
		break;
	case 'callshop':
		global $submenu_callshop;
		$submenu = $submenu_callshop;
		break;
	case 'user':
		global $submenu_user;
		$submenu = $submenu_user;
		break;
	case 'carrier':
		global $submenu_carrier;
		$submenu = $submenu_carrier;
		break;
	}
	if (!$pages) {
		foreach ($submenu as $id => $tree) {
			if (in_array($g_site->current_page, $tree)) {
				$pages = $tree;
				break;
			}
		}
	}

	$ul = '<div class="tab_nav">
              <ul class="shadetabs">' . "\n";
	$li = '<li class="%s %s"><a href="%s">%s</a></li>&nbsp;&nbsp;';

	if (!is_array($pages))
		return "";
	
	foreach ($pages as $pg) {
		$class1 = ($pg == $g_site->current_page) ? 'selected' : 'notselected';
		$class2 = ($pg == $g_site->current_page) ? 'current' : '';
		$name = $g_site->item_map['tabs'][$pg];
		$ul .= "\t" . sprintf($li, $class1, $class2, site::link($pg, $vars), $name) . "\n";
	}
	$ul .= '</ul></div>';

	return $ul;
}

}

?>