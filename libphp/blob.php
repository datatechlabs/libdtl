<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 14/04/2016
 * Time: 14:43
 */

class Blob {

    public $name;
    public $mimetype;
    public $data;


    public function get($id)
    {
        if (!$id)
            return false;

        $this->id = $id;

        $db = Dbconnect::getInstance();
        $sql = "SELECT * FROM Blobs
                  WHERE blob_id = :blob_id";
        $params = array('blob_id' => $this->id);
        if(!$row = $db->fetch_one($sql, $params))
            return false;
        $this->name = $row['name'];
        $this->mimetype = $row['mimetype'];
        $this->size = $row['size'];

        $sql = "SELECT * FROM BlobData
                  WHERE blob_id = :blob_id";
        $params = array('blob_id' => $this->id);

        if(!$data = $db->fetch_assoc($sql, $params))
            return false;

        $this->data = "";
        foreach ($data as $drow) {
            $this->data .= base64_decode($drow ['data']);
        }
    }


    public function update($id)
    {
        if (!$id)
            return false;

        $len = strlen($this->data);

        /* object reference entry */
        $db = Dbconnect::getInstance();
        $sql = "
            UPDATE Blobs SET
              `name` = :name,
              `mimetype` = :mimetype,
              `size` = :size
              WHERE `blob_id` = :blob_id";
        $params = array(
            'name' => $this->name,
            'mimetype' => $this->mimetype,
            'size' => $len,
            'blob_id' => $id
        );
        if(!$db->execute($sql, $params))
            return false;


        $sql = "DELETE FROM BlobData WHERE `blob_id` = :blob_id";
        $params = array('blob_id' => $id);
        if(!$db->execute($sql, $params))
            return false;

        $this->blob_data();
        return true;
    }

    public function save()
    {


        $len = strlen($this->data);

        /* object reference entry */
        $db = Dbconnect::getInstance();
        $sql = "
            INSERT INTO Blobs (`name`, `mimetype`, `size`)
            VALUES (:name, :mimetype, :size)";
        $params = array(
            'name' => $this->name,
            'mimetype' => $this->mimetype,
            'size' => $len);
        if(!$db->execute($sql, $params))
            return false;
        $id = $db->last_id();

        // Copy the binary file data to the filedata table in sequential rows each containing MAX_SQL bytes

        $this->blob_data();

        $this->id = $id;
        return true;
    }

    private function blob_data() {
        // Copy the binary file data to the filedata table in sequential rows each containing MAX_SQL bytes

        // Max packet size
        define("MAX_SQL",50000);
        $db = Dbconnect::getInstance();

        $i=0;
        $data = ".";
        while ($data) {
            $start = $i*MAX_SQL;
            $data = base64_encode(substr($this->data, $start, MAX_SQL));
            if(!$data)
                break 1;
            $sql = "
			INSERT INTO BlobData (blob_id, data)
			VALUES (:blob_id, :data)";
            $params = array(
                'blob_id' => $this->id,
                'data' => $data
            );
            if(!$db->execute($sql, $params))
                return false;
            $i++;
        }
        return true;
    }
}