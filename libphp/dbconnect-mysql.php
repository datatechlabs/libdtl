<?php

/*
 * Database connection class.
 */
class db {

	public $connections = array();	// connection array
	public $resarr = array();	// query result handles
	public $selected;		// currently selected connection

	public $error;

	// establish a new connection
	function connect($dbhost, $dbuser, $dbpass, $dbname, $identifier = 'main')
	{

		$this->selected = $identifier;
		$this->connected = false;
		$this->engine = "mysql";

		// connect
		$this->connections[$this->selected] = mysqli_connect($dbhost, $dbuser, $dbpass, '') or die ("Database connection failed. Please retry\n");
		mysqli_set_charset($this->connections[$this->selected], 'utf8');
		mysqli_select_db($this->connections[$this->selected], $dbname) or die('Invalid database name!');

		if ($this->connections[$this->selected] == false)
			return false;
		$this->connected = true;
		return true;
	}

	public function disconnect()
	{
		return mysqli_close($this->connections[$this->selected]);
	}

	public function set_active($identifier)
	{
		$this->selected = $identifier;
	}

	public function get_active()
	{
		return $this->selected;
	}

	// get connection resource for direct manipulation
	public function get_resource()
	{
		return $this->connections[$this->selected];
	}


	// get associative result array
	public function fetch_assoc($stmt, $params)
	{

		$query = $this->parse_query($stmt, $params);

		if (($result = $this->exec_query($query, $this->selected)) == false)
			return false;

		$assocs = array();
		while ($assoc = mysqli_fetch_assoc($result))
			$assocs[] = $assoc;

		mysqli_free_result($result);

		return $assocs;
	}

	public function fetch_one($stmt, $params)
	{
		$query = $this->parse_query($stmt, $params);

		if (($result = $this->exec_query($query, $this->selected)) == false)
			return false;

		$assoc = mysqli_fetch_assoc($result);
		if ($assoc === NULL)
			$assoc = false;
		mysqli_free_result($result);
		return $assoc;
	}

	public function fetch_object($stmt, $params)
	{
		$query = $this->parse_query($stmt, $params);
		if (($result = $this->exec_query($query, $this->selected)) == false)
			return false;

		$row = mysqli_fetch_object($result);
		mysqli_free_result($result);
		return $row;
	}
	/*
 * Execute a query and get a boolean value to see if it
 * succeeded or not.
 *
 * If raw fetching is intended a result identifier string must
 * passed as '$resid'. Later pass the same string to the
 * fetch() function.
 */
	public function execute($stmt, $params, $resid='')
	{
		$query = $this->parse_query($stmt, $params);
		if (!$resid) {
			if ($result = $this->exec_query($query, $this->selected) == false) {
				return false;
			}
			return true;
		}

		if (($result = $this->exec_query($query, $this->selected)) == false)
			return false;

		$this->resarr[$resid] = $result;
		return true;
	}

	public function begin()
	{
		return $this->execute("BEGIN", $this->selected);
	}

	public function commit()
	{
		return $this->execute("COMMIT", $this->selected);
	}

	// returns last insert id or false on failure
	public function last_id($table=false, $column=false)
	{
		return mysqli_insert_id($this->connections[$this->selected]);
	}

	private function parse_query($stmt, $params) {

		foreach($params as $id=>$param) {
			$type = gettype($param);
			switch($type) {
				case "string":
					$stmt = str_replace("$".$id, "'".$this->escape($param)."'", $stmt);
					break;
				case "integer":
					$stmt = str_replace("$".$id, (int)$param, $stmt);
					break;
				case "object":
					$cltype = get_class($param);
					switch ($cltype) {
						case "DateTime":
							$str = "'".$param->format('Y-m-d H:i:s')."'";
							break;
						default:
							$str = "'".$param->text."'"; // FIXME
					}
					$stmt = str_replace("$".$id, $str, $stmt);
					break;
				default:
					$stmt = str_replace("$".$id, "'".$this->escape($param)."'", $stmt);
			}
		}
		return $stmt;
	}

	private function exec_query($sql)
	{
		$this->error = false;
		$result = mysqli_query($this->connections[$this->selected], $sql);
		if ($result == false) {
			// all errors should be sent to log file rather on screen!
			$this->error = mysqli_error($this->connections[$this->selected]);
			common::ndebug(2,"dbconnect-mysql::exec_query error %s in query %s", $this->error, $sql);

		} else {
			if (strtolower(substr(ltrim($sql),0,6)) != "select")
				common::ndebug(5,"SQL query: %s", $sql);
		}
		common::ndebug(5,"dbconnect-mysql::exec_query executed. Query: %s ", $sql);

		return $result;
	}

	public function escape($str, $len=100)
	{
		$keywords = array(
			'\sinsert\s', '\sselect\s', '\sdrop\s', '\sinsert\s', '\sunion\s', '\spg_sleep\s', '\sxmltype\s',
			'\sinformation_schema\s', '\scharacter_sets\s', '\swaitfor\s', ';select\s', '=sleep\(', '\(sleep\(',
			'\ssleep\('
		);
		// generate warning if any of keywords hit
		foreach ($keywords as $kw) {
			if (preg_match("/".$kw."/", strtolower($str)))
				common::ndebug(2, 'SECURITY: Possible SQL Injection detected from %s (string %s)', common::get_ip(), $str);
		}

		return substr(mysqli_real_escape_string($this->connections[$this->selected], $str), 0, $len);
	}






}

?>