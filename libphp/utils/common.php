<?php

/*
 * Just a collection of utility functions.
 */

define('INFMSG', 0);
define('ERRMSG', 1);

define('NOEXTRAOPT', 0);
define('BLANKOPT', 1);

define('DF_DATE', 1);
define('DF_DATETIME', 2);
define('DF_FULL', 3);
define('DF_FULL_TZ', 4);

define('IPC_NAME', 1);
define('IPC_CODE', 2);
define('IPC_BOTH', 3);

define('CL_IDNAME', 1);
define('CL_CODENAME', 2);
define('CL_IDCODE', 3);

define('SET_ALPHAANY', 1);
define('SET_ALPHAUPPER', 2);
define('SET_ALPHALOWER', 3);
define('SET_DIGITS', 4);
define('SET_HEXCHARS', 5);

define('ULOG_INFO', 0);
define('ULOG_WARN', 1);
define('ULOG_ERRO', 2);

class common {

//
// Safe get with default value (does not produce php notice warning).
//   http://stackoverflow.com/a/25205195
//
static function get(&$var, $default=NULL) {
    return isset($var) ? $var : $default;
}
	
/*
 * Generates a string containing random characters,
 * intended for session IDs
 *
 * $len:	string length
 * $digitsonly (bool) 	only digits or alphanumeric
 */
static function rand_string($len = 50, $digitsonly = false)
{ 
	$allchars = '0123456789';
	if (!$digitsonly)
		$allchars .= 'abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ'; 
	$string = ''; 
	
	mt_srand((double) microtime() * 1000000);
	
	for ($i = 0; $i < $len; $i++)
		$string .= $allchars[mt_rand(0, strlen($allchars) - 1)]; 
		
	return $string; 
}

/*
 * Generates a string containing random characters
 *
 * $len:	string length
 * $set:	set of characters to use
 */

static function rand_string_from_set($len = 10, $settype = SET_ALPHAANY)
{
	if ($settype == SET_ALPHAANY)
		$allchars = 'abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ';
	elseif ($settype == SET_ALPHAUPPER)
		$allchars = 'ABCDEFGHIJKLNMOPQRSTUVWXYZ';
	elseif ($settype == SET_ALPHALOWER)
		$allchars = 'abcdefghijklnmopqrstuvwxyz';
	elseif ($settype == SET_DIGITS)
		$allchars = '0123456789';
	elseif ($settype == SET_HEXCHARS)
		$allchars = '0123456789abcdef';
	else 
		$allchars = 'abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789';

	mt_srand((double) microtime() * 1000000);
	
	$string = ''; 
	
	for ($i = 0; $i < $len; $i++)
		$string .= $allchars[mt_rand(0, strlen($allchars) - 1)]; 
		
	return $string; 
}

	/**
	 * Generate a random string, using a cryptographically secure
	 * pseudorandom number generator (random_int)
	 *
	 * For PHP 7, random_int is a PHP core function
	 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
	 *
	 * @param int $length      How many characters do we want?
	 * @param string $keyspace A string of all possible characters
	 *                         to select from
	 * @return string
	 */

	static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		$min = 0;
		for ($i = 0; $i < $length; ++$i) {
			if (function_exists('random_int')) {
				$str .= $keyspace[random_int($min, $max)];
			} else {
				$str .= $keyspace[rand($min,$max)];
			}
		}
		return $str;
	}

/*
 * Get a nice formatted html message.
 *
 * $msg: message txt
 * $type: message type, INFMSG - just information, ERRMSG - error
 * 
 * returns: html string
 */	
static function message($msg, $type = INFMSG)
{
	global $g_site;
	$message = new Template('message.tpl');
	
	switch ($type) {
	case INFMSG:
		$message->assign('STYLE', 'info_message');
		break;
	case ERRMSG:
		$message->assign('STYLE', 'error_message');
		break;
	}
	$message->assign('ID', time());
	$message->assign('MESSAGE_TEXT', $msg);
	$g_site->system_message_text = $message->fetch();
	$g_site->system_message_type = $type;
	//return $message->fetch();
}

/* get users timezone offset from server time in seconds */
static function tz_seconds() {
	global $g_user;
	$dtz = settings::get_value('timezone', 'PATHS');
	if (!$dtz) 
		$dtz = "GMT";
	// offset timezone
    if (isset($g_user->webacc->timezone))
        $tz = $g_user->webacc->timezone;
	else
		$tz = $dtz;
		
	if ($tz) {
		$default_tz = new DateTimeZone($dtz);
		$user_tz = new DateTimeZone($tz);
		$default_time = new DateTime("now", $default_tz);
		$user_tz_offset = $user_tz->getOffset($default_time);
	} else {
		$user_tz_offset = 0;
	}
	return $user_tz_offset;
}

/* this is to offset report queries to get "user" start and end times for reports */
static function tz_offset($timestr)
{
	$tstamp = strtotime($timestr);
	$offtstamp = $tstamp - common::tz_seconds();
	return strftime("%Y-%m-%d %T", $offtstamp);
}

/* prints friendy time (5 minutes ago, 23 days ago etc) from ISO time */

static function fmt_friendly_time($timestamp)
{
	$diff = time() - strtotime($timestamp);
	if ($diff < 60) {
		$tpl = new Template('common/seconds_ago.tpl');
		$tpl->assign('NUM', $diff);
		return $tpl->fetch();
	}
	$diff = (int)( $diff / 60 );
	if ($diff < 60) {
		$tpl = new Template('common/minutes_ago.tpl');
		$tpl->assign('NUM', $diff);
		return $tpl->fetch();
	}
	$diff = (int)( $diff / 60 );
	if ($diff < 24) {
		$tpl = new Template('common/hours_ago.tpl');
		$tpl->assign('NUM', $diff);
		return $tpl->fetch();
	}
	$diff = (int)( $diff / 24 );
	$tpl = new Template('common/days_ago.tpl');
	$tpl->assign('NUM', $diff);
	return $tpl->fetch();
}

/*
 * Format timestamp according to the global setting in config file
 * timestamp - 
 * format, one of DF_DATETIME, DF_FULL, DF_DATE, DF_FULL_TZ
 * reverse - apply time zone in reverse
 * unixtime - timestamp is unixtime (seconds from 1970-01-01)
 */
static function fmt_date($timestamp, $format = DF_FULL, $reverse = false, $unixtime = false)
{
	global $g_user;

	$df = settings::get_value('date_format', 'DISPLAY', 'Y-m-d');
	$dtz = settings::get_value('timezone', 'GENERAL', 'GMT');
	if (!$dtz)
		$dtz = "UTC";
	if (isset($g_user->webacc->timezone))
		$tz = $g_user->webacc->timezone;
	else
		$tz = $dtz;
	
	if ($tz) {
		$default_tz = new DateTimeZone($dtz);
		$user_tz = new DateTimeZone($tz);
		$default_time = new DateTime("now", $default_tz);
		$user_time = new DateTime("now", $user_tz);
		$user_tz_offset = $user_tz->getOffset($default_time);
		$user_time->setTimeZone($user_tz);
	} else {
		$user_tz_offset = 0;
	}
	if ($unixtime)
		$tstamp = $timestamp;
	else
		$tstamp = strtotime($timestamp);
	
	if ($reverse)
		$offtstamp = $tstamp - $user_tz_offset;
	else
		$offtstamp = $tstamp + $user_tz_offset;
	
	if ($format == DF_DATE) // do not offset for DF_DATE, to avoid day jumping
		$timestamp = strftime("%Y-%m-%d %T", $tstamp);
	else
		$timestamp = strftime("%Y-%m-%d %T", $offtstamp);

	/* convert to unix timestamp */
	$timestamp = strtotime(substr($timestamp, 0, 19));

	switch ($format) {
	case DF_DATETIME:
		$fmt = $df . ' H:i';
		break;
	case DF_FULL:
		$fmt = $df . ' H:i:s';
		break;
	case DF_DATE:
		$fmt = $df;
		break;
	case DF_FULL_TZ:
		$fmt = $df . ' H:i:s '.$user_time->format('P');
		break;
	}
	
	return date($fmt, $timestamp);
}

static function fmt_money($number, $precision = 2)
{
	$money_format = settings::get_value('money_format', 'DISPLAY', 'INT'); // should be DISPLAY
	if (!is_numeric($number))
		$number = 0;
	if ($money_format == "USA") {
		if($number < 0) {
			$str = "(".number_format(abs($number),$precision).")";
		} else {
			$str = number_format($number,$precision);
		}
	} elseif ($money_format == "INT") {
		$str = number_format($number,$precision);
	} else { // unknown
		$str = $number;
	}
	return $str;
}

static function fmt_money_db($number)
{
	$number = preg_replace("/\\\'| |,/","",$number);
	return $number;
}

// displays time in mm:ss format with comma as thousand separator
static function fmt_time($seconds)
{
//	$hour = floor($seconds / 3600);
	$hour = 0;
//	$minute = floor(($seconds - $hour*3600) / 60);
	$minute = floor($seconds / 60);
	$second = $seconds - $hour*3600 - $minute*60;
	return sprintf("%s:%02d", number_format($minute,0,".",","), $second);
//	return sprintf("%d:%02d:%02d", $hour, $minute, $second);
}

/*
 * Given a date string in ISO format, return an array in the same form
 * as is returned by the PHP function getdate()
 *
 * $isodate:	date in ISO format (YYYY-MM-DD HH:MM:SS)
 *		The extended postgreSQL version with second
 *		fractions appended is also acceptable although the
 *		the fraction part is ignored.
 *	
 * returns: array
 */
static function getdate_from_iso($isodate)
{
	$arr = array();

	// check if the date is in correct format
	if (!preg_match('/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d/', $isodate)) {
		$now =  getdate();	// if not, return current date
		
		$arr['year'] = $now['year'];
		$arr['mon'] = str_pad($now['mon'], 2, '0', STR_PAD_LEFT);
		$arr['mday'] = str_pad($now['mday'], 2, '0', STR_PAD_LEFT);
		$arr['hours'] = str_pad($now['hours'], 2, '0', STR_PAD_LEFT);
		$arr['minutes'] = str_pad($now['minutes'], 2, '0', STR_PAD_LEFT);
		$arr['seconds'] = str_pad($now['seconds'], 2, '0', STR_PAD_LEFT);
		
		return $arr;
	}
	
	$arr['year'] = substr($isodate, 0, 4);
	$arr['mon'] = substr($isodate, 5, 2);		
	$arr['mday'] = substr($isodate, 8, 2);
	$arr['hours'] = substr($isodate, 11, 2);
	$arr['minutes'] = substr($isodate, 14, 2);
	$arr['seconds'] = substr($isodate, 17, 2);
	
	return $arr;
}

static function gettime_from_iso($isotime)
{
	// check if the time is in correct format
	if (!preg_match('/^\d\d:\d\d:\d\d/', $isotime))
		return '00:00:00';	// something's not right
	
	$arr = array();
	
	$arr['hours'] = substr($isotime, 0, 2);
	$arr['minutes'] = substr($isotime, 3, 2);
	$arr['seconds'] = substr($isotime, 6, 2);
	
	return $arr;
}

/*
 * Produce a csv line from array values
 */
static function getcsv($values, $msexcel = false)
{
	foreach ($values as $key => $val) {
		if (strstr($val, ' ') !== false ||
		    strstr($val, '"') !== false) {
			$val = str_replace('"', '""', $val);
			$values[$key] = "\"$val\"";
			continue;
		}
		/* convert numeric values to ="NNNN" format to help 
		 * MS Excel typecast them as numbers
		 */
		if ($msexcel == true && is_numeric($val))
			$values[$key] = "=\"$val\"";
		else
			$values[$key] = $val;
	}

	return implode(',', $values);
}

/* creates csv line and writes to the file pointer 
 * its a wrapper function for fputcsv function
 * which adjusts for proper delimiter
 */

static function putcsv($fp, $values)
{
	global $g_site;
	$delimiter = $g_site->params['csv_delimiter'];
	if (!$delimiter)
		$delimiter = ",";
	fputcsv($fp, $values, $delimiter);
}

/*
 * generates links to other pages (used for dividing search results)
 *
 * $curpage: current page
 * $pagecount: total number of pages
 * $visible: visible numerical links
 *
 * returns: html string
 */
static function pagelinks($curpage, $pagecount, $visible = 5, $linkvars=array())
{
	$plink = new Template('common/link.tpl');
	$fwlink = new Template('common/forwardlink.tpl'); // forward
	$bwlink = new Template('common/backwardlink.tpl'); // backward
	$ffwlink = new Template('common/fforwardlink.tpl'); // fast forward
	$fbwlink = new Template('common/fbackwardlink.tpl'); // fast backward
	$pstr = '';		// result string

	if ($pagecount < 2)
		return '';
	
	// paging stuff		
	if ($curpage - (int)($visible / 2) > 0) {
		$linkvars['pagenum'] = 0;
		$fbwlink->assign('LINK', site::link(false, $linkvars));
		$fbwlink->assign('STYLE', 'accented');
		$pstr .= $fbwlink->fetch();
	}

	if ($curpage > 0) {
		$linkvars['pagenum'] = $curpage - 1;
		$bwlink->assign('LINK', site::link(false, $linkvars));
		$bwlink->assign('STYLE', 'accented');
		$pstr .= $bwlink->fetch();
	}
	
	
	for ($i = $curpage - (int)($visible / 2);
	    $i <= $curpage + (int)($visible / 2); $i++) {
		if ($i >= 0 && $i < $pagecount) {
			$linkvars['pagenum'] = $i;
			$plink->assign('LINK', site::link(false, $linkvars));
			$plink->assign('DESCR', $i + 1);
			$plink->assign('STYLE', $i == $curpage ?
				'activepage' :
				'accented');
			$pstr .= $plink->fetch();
		}
	}
	
	if ($curpage < $pagecount - 1) {
		$linkvars['pagenum'] = $curpage + 1;
		$fwlink->assign('LINK', site::link(false, $linkvars));
		$fwlink->assign('STYLE', 'accented');
		$pstr .= $fwlink->fetch();
	}
	
	if ($curpage + (int)($visible / 2) + 1 < $pagecount) {
		$linkvars['pagenum'] = $pagecount - 1;
		$ffwlink->assign('LINK', site::link(false, $linkvars));
		$ffwlink->assign('STYLE', 'accented');
		$pstr .= $ffwlink->fetch();
	}
	
	return $pstr;
}


/* displays time in accordance with global config param time_form
 * 0 - HH:MM:SS
 * 1 -  MM:SS
 * 2 -  MM.MMMM
 */

static function fmt_seconds($seconds)
{
	global $g_site;
	switch ($g_site->params['time_form']) {
		case 0: //hh:mm:ss
			$hour = floor($seconds / 3600);
			$minute = floor(($seconds - $hour*3600) / 60);
			$second = $seconds - $hour*3600 - $minute*60;
			return sprintf("%d:%02d:%02d", $hour, $minute, $second);
		case 1: // mm:ss
			$hour = 0;
			$minute = floor($seconds / 60);
			$second = $seconds - $hour*3600 - $minute*60;
			return sprintf("%s:%02d", number_format($minute,0,".",","), $second);
		case 2: //mm.mmmmm
			$minute = $seconds / 60;
			return sprintf("%02.4f", $minute);
	}
}

static function get_ip()
{
	if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	return $_SERVER['REMOTE_ADDR'];
}

// function check IP address
static function check_ip($addr, $with_port = false) 
{
	if ($with_port) {
		$parts = explode(":", $addr);
		$addr = $parts[0];
	}
	list($a,$b,$c,$d) = explode(".",$addr);
	if(!preg_match("/^([0-9]{1,3}\.){3}[0-9]{1,3}$/",$addr))
		return false;
	if ($a <1 || $a>255)
		return false;
	if ($b <0 || $b>255)
		return false;
	if ($c <0 || $c>255)
		return false;
	if ($d <0 || $d>255)
		return false;

	return true;
}

static function ip_to_country($ip, $result = IPC_NAME)
{
	global $g_site;
	$url = $g_site->params['ip2country_uri'];
	$req = str_replace('{$ip}', $ip, $url);
	if (!$req)
		return "ERROR (No Request URI)";

	$res = @file($req);
	
	$line = trim($res[0]);
	if (substr($line,0,2) == "##")
		return substr($line,2);
	
	list($code, $name) = explode("|", $line);
	
	if ($result == IPC_NAME)
		return $name;
	if ($result == IPC_CODE)
		return $code;
	if ($result == IPC_BOTH)
		return $code.",".$name;
}
	
/* alternates style name for even odd rows in result set */
static function rowstyle($i)
{
	if (floor($i / 2) == $i / 2)
		$style = "resultrow1";
	else 
		$style = "resultrow2";
	return $style;
}

/* debug logging function 
 * it should take undefined number of arguments:
 * format:
 * ndebug($level, $format, $arg1 [, $argn])
 */
 
static function ndebug() {

	$ndebug = settings::get_value('ndebug', 'DEBUG');
	$debug_log_file = settings::get_value('debug_log_file', 'PATHS');
	$site = settings::get_value('site', 'MISC');

	$level = func_get_arg(0);
	$format = func_get_arg(1);
	if (!$format)
		return false;
	$arg_count = func_num_args() - 2;
	
	if (!$debug_log_file)
		$debug_log_file = "/var/log/dtl_debug.log";

//	$format = str_replace("%", "%%", $format);
	$eval_str = "\$msg = sprintf(\"".$format."\",";
	
	for ($i = 1; $i <= $arg_count; $i++) {
		$arg = "";
		$arg = func_get_arg($i+1);
		if (is_array($arg))
			$arg = print_r($arg, true);
		elseif (is_object($arg))
			$arg = print_r($arg, true);
		if (is_string($arg))
			$arg = addslashes($arg);
		if ($arg)
			$eval_str .= "'".$arg."',";
		else
			$eval_str .= "'',";
		($arg) ? ($arg) : ("\"0\"");
	}
	$eval_str = trim($eval_str,",");
	$eval_str .= "); return \$msg;";
//	echo $eval_str;
	eval($eval_str);
	if ($ndebug && $level <= $ndebug) {
		$timestr = strftime("%Y-%m-%d %H:%M:%S", time());
		$logmes = $timestr."|".$level."|".$site."|".$msg."\n";
 		// file handle
		$handle = fopen($debug_log_file, "a");
		fwrite($handle, $logmes);
		fclose($handle);
	}
}	


// formats  (BB->Html)
static function fmt_html($text)
{
	$bbcode = array("<", ">",
                "[list]", "[*]", "[/list]", 
                "[img]", "[/img]", 
                "[b]", "[/b]", 
                "[u]", "[/u]", 
                "[i]", "[/i]",
                '[color="', "[/color]",
                "[size=\"", "[/size]",
                '[url="', "[/url]",
                "[mail=\"", "[/mail]",
                "[code]", "[/code]",
                "[quote]", "[/quote]",
                "[h1]", "[/h1]",
                "[h2]", "[/h2]",
                "[h3]", "[/h3]",
                "[h4]", "[/h4]",
                "[h5]", "[/h5]",
                "[h6]", "[/h6]",
                '"]');
	$htmlcode = array("&lt;", "&gt;",
                "<ul>", "<li>", "</ul>",
                "<img src=\"", "\">", 
                "<b>", "</b>", 
                "<u>", "</u>", 
                "<i>", "</i>",
                "<span style=\"color:", "</span>",
                "<span style=\"font-size:", "</span>",
                '<a href="', "</a>",
                "<a href=\"mailto:", "</a>",
                "<code>", "</code>",
                "<table width=100% bgcolor=lightgray><tr><td bgcolor=white>", "</td></tr></table>",
                "<h1>", "</h1>",
                "<h2>", "</h2>",
                "<h3>", "</h3>",
                "<h4>", "</h4>",
                "<h5>", "</h5>",
                "<h6>", "</h6>",
                '">');
  $newtext = str_replace($bbcode, $htmlcode, $text);
  $newtext = nl2br($newtext);//second pass
  return $newtext;
}

// formats BBCode to plain text
static function bbcode_to_plain($text)
{
	$bbcode = array("<", ">",
                "[list]", "[*]", "[/list]", 
                "[img]", "[/img]", 
                "[b]", "[/b]", 
                "[u]", "[/u]", 
                "[i]", "[/i]",
                "[size=\"", "[/size]",
                "[mail=\"", "[/mail]",
                "[code]", "[/code]",
                "[quote]", "[/quote]",
                );
	$htmlcode = array("", "",
                "", "* ", "",
                "", "", 
                "*", "*", 
                "_", "_", 
                "", "",
                "", "",
                "\n", "\n",
                "", "",
                "", "",
                );
	$newtext = str_replace($bbcode, $htmlcode, $text);
	// 

	$bbcode = array(

		"/\[url=\"(.+)\"\](.+)\[\/url\]/" => "$2 $1",
		"/\[color=\"(.+)\"\](.+)\[\/color\]/" => "$2",

	);
	$newtext = preg_replace(array_keys($bbcode), array_values($bbcode), $newtext);
	return $newtext;
}

static function txt2html($txt)
{
	$ret = '';
	$break = "<br>";
	$rows = explode("\n",$txt);
	foreach( $rows as $row)
		$ret .= $row.$break;
	return $ret;
}


static function mime_header_encode($string) {
	if (preg_match('/[^\x20-\x7E]/', $string)) {
		return ' =?UTF-8?B?'. base64_encode($string) ."?=";
	}
	return $string;
}

static function passw_hash($passw, $username)
{
	$salt = '$1$'.$username.$username.$username.'$';
	return crypt($passw, $salt);
}

static function base_url()
{
	global $g_site;
	
	if (isset($g_site) && isset($g_site->params)) {
		$params = $g_site->params;
	} else {
		$params = site::get_params();
	}
	
	if(array_key_exists('base_url', $params) && $params['base_url'] != '' ) {
		$base_url = $params['base_url'];
	} else {
		$base_url = settings::get_value('base_url','PATHS');
	}


	// take care of proxy cases
	if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
		if (strlen($params['proxy_access_pairs']) > 0) {
			$proxypairs = explode(',', $params['proxy_access_pairs']);
			if (is_array($proxypairs)) {
				foreach ($proxypairs as $pair) {
					list($ip, $host) = explode("=", $pair);
					if ($ip && $ip == $_SERVER['REMOTE_ADDR']) {
						$base_url = $host;
					}
				}
			}
		}
	}

	$host = $_SERVER['HTTP_HOST'];
	// should we replace {$HOST} with $_SERVER['HTTP_HOST'] ?
	$base_url = str_replace("{%HOST}", $host, $base_url);
	$base_url = rtrim($base_url, "/");

	// serve pages over https?
	if (array_key_exists('REQUEST_SCHEME', $_SERVER)) {
		if ($_SERVER['REQUEST_SCHEME'] == 'https') {
			$base_url = str_replace("{%PROTOCOL}", 'https', $base_url);
		}
	}

	$base_url = str_replace("{%PROTOCOL}", 'http', $base_url);
	$base_url = rtrim($base_url, "/");

	return $base_url;
}

	static function lib_url()
	{
		$host = $_SERVER['HTTP_HOST'];

		$lib_url = settings::get_value('lib_url','PATHS');
		$lib_url = str_replace("{%HOST}", $host, $lib_url);
		$lib_url = rtrim($lib_url, "/");
		return $lib_url;
	}

static function str_to_bytes($str)
{
	$multiplier = substr($str, strlen($str)-1 ,1);
	$value =  substr($str, 0, strlen($str)-1);
	switch (strtoupper($multiplier)) {
	case "K":
		return $value * 1024;
	case "M":
		return $value * 1024 * 1024;
	case "G":
		return $value * 1024 * 1024 * 1024;
	}
	return $value;
}

static function bytes_to_str($bytes) {
	if ($bytes < 1024)
		return $bytes;
	$kbytes = $bytes/1024;
	if ($kbytes < 1024)
		return $kbytes."K";
	$mbytes = $kbytes/1024;
	if ($bytes < 1024 * 1024 * 1024)
		return $mbytes."M";
	$gbytes = $mbytes/1024;
	return $gbytes."G";
}

static function get_languages()
{
    $package_root = settings::get_simple_path('package_root');
    if (is_file("$package_root/etc/languages.txt")) {
        $langfile = "$package_root/etc/languages.txt";
        return parse_ini_file($langfile, true);
    }

    if (is_file("$package_root/etc/languages.txt.default")) {
        $langfile = "$package_root/etc/languages.txt.default";
        return parse_ini_file($langfile, true);
    }

    return [];
}

static function uuid($data) 
{
	assert(strlen($data) == 16);

	$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
	$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

	return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


	static function require_class($filename) {
		$package_root = settings::get_value("package_root", "PATHS");
		$libdtl_root = settings::get_value("libdtl_root", "PATHS");
		$libpath = "$libdtl_root/libphp";
		if (is_file($package_root."/dbstructs/".$filename)) {
			require_once ($package_root."/dbstructs/".$filename);
		} elseif (is_file($libpath."/dbstructs/".$filename)) {
			require_once ($libpath."/dbstructs/".$filename);
		}
	}
}
?>
