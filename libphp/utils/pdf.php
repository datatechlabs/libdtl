<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 13/07/2017
 * Time: 16:36
 */

function pdf_invoice_parse($template, $field_matches, $field_replaces, $calldet, $debug, $init=true, $details, $outmode="browser", $filename="pdf.pdf") {

    global $pdf;
    
    $libdtl_root = settings::get_simple_path('libdtl_root');
    // disable depreciation warnings
    //error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);

    $package_root = settings::get_value('package_root', 'PATHS');
    $base_dir = $libdtl_root.'/libphp/ezpdf/';
	include_once $base_dir.'class.ezpdf.php';
    $path_to_fonts = $base_dir.'fonts/';
    $fontsize=12; // default size
    $mainFont = $base_dir.'fonts/Helvetica.afm';
    //$mainFont = './fonts/Times-Roman.afm';
    $codeFont = $base_dir.'fonts/Courier.afm';

	$euro_diff = array();
	$euro_diff[94] = "Euro";

    if (!$details['ezpdf_initialized'] && !class_exists('Creport')) {  // we can only initialize ezpdf classes once
        // define a class extension to allow the use of a callback to get the table of contents, and to put the dots in the toc
        class Creport extends Cezpdf {
            var $reportContents = array();
            function Creport($p,$o){
                $this->Cezpdf($p,$o);
            }
        }
        $pdf = new Creport('a4','portrait');
        $pdf -> ezSetMargins(50,70,50,50);
        $details['ezpdf_initialized'] = true; // make sure we dont initialize again it
    }


    if ($init) { // initiate new page only at first loop , i.e. first page in batch
        $pdf = new Creport('a4','portrait');
        $pdf -> ezSetMargins(50,70,50,50);
    } else { // just next page
        $pdf->ezNewPage();
    }

//	$pdf->ezSetDy(-100);

    // select a font
    $pdf->selectFont($mainFont,array('differences'=>$euro_diff));

    // modified to use the local file if it can

    $pdf->openHere('Fit');

    // main loop to read and evaluate template
    $height = $pdf->getFontHeight($fontsize);
    $textOptions = array('justification'=>'full');
    $collecting=0;
    $code='';
    $colnum=0;

    foreach ($template as $line){
        // go through each line, showing it as required, if it is surrounded by '<>' then
        // assume that it is a title
        $line=chop($line);
        preg_match("/^(\#)([a-zA-Z]*)(\d*)(\(.+\))?/",$line, $match);

        if (strlen($line) && $line[0]=='#'){ // control lines
            $cmd = $match[2];
            $objid = $match[3];
            $attrpairs = explode(",", trim($match[4],"()")); // attributes to command, if any
            if (is_array($attrpairs)) {
                $attr = array();
                foreach ($attrpairs as $pair) {
                    $pos = strpos($pair, "=");
                    $key = trim(substr($pair, 0, $pos));
                    $val = trim(substr($pair, $pos+1));
                    if ($objid) {
                        $attr[$objid][$key] =  trim($val, "\"'");
                    } else {
                        $attr[$key] =  trim($val, "\"'");
                    }
                }
            }
            // lets see what requested
            switch($cmd){
                case 'NP':  // new page
                    $pdf->ezNewPage();
                    break;
                case 'TABLE': // start table definition
                    //$pdf->ezText($cmd,$size,$textOptions);
                    $opt[$objid]['showLines'] = $attr[$objid]['border'];
                    $opt[$objid]['shaded'] = $attr[$objid]['shade'];
                    $opt[$objid]['fontSize'] = $attr[$objid]['font-size'];
                    $opt[$objid]['xPos'] = $attr[$objid]['align'];
                    $opt[$objid]['xOrientation'] = $attr[$objid]['x-orientation'];
                    $opt[$objid]['rowGap'] = $attr[$objid]['cellpadding-row'];
                    $opt[$objid]['colGap'] = $attr[$objid]['cellpadding-col'];
                    $opt[$objid]['width'] = $attr[$objid]['width'];
                    break;
                case 'TD': // table row definition
                    $colnames[$objid][$colnum] = $attr[$objid]['title']; // A
                    $opt[$objid]['cols'][$colnum]['justification'] = $attr[$objid]['align'];
                    $opt[$objid]['cols'][$colnum]['width'] = $attr[$objid]['width'];
                    $tdformat = $attr[$objid]['format'];
                    // lets add data to table
                    foreach ($calldet as $row => $callrow) { // each row of call data
                        foreach ($callrow as $cell => $val) {
                            if ($cell == $attr[$objid]['source']){
                                // treat special formatting cases:
                                if ($tdformat == "%HH:MM:SS") { // we do hack by changing params
                                    $params['time_form'] = 0;
                                    $val = common::fmt_seconds($val);
                                } elseif ($tdformat == "%MM:SS") {
                                    $params['time_form'] = 1;
                                    $val = common::fmt_seconds($val);
                                } elseif ($tdformat == "%YYYY-MM-DD") {
                                    $val = dtl_date_format($val,2);
                                } else {
                                    $val = sprintf($tdformat,$val);
                                }
                                $tdata[$row][$colnum] = $val;
                                break 1;
                            }
                        }
                    }
                    $colnum++;
//print "OBJID".$objid;
//print_r($tdata);
                    break;
                case 'TR': // separate table row definition, used for totals row
                    $tr_data = $attr[$objid]['source'];
                    $tr_fmt = $attr[$objid]['format'];
                    // explode
                    $tr_data_arr = explode(";",$tr_data);
                    $tr_fmt_arr = explode(";",$tr_fmt);
                    $rows = count($tdata);
                    $ii=0;
                    foreach ($tr_data_arr as $tr_k => $tr_val) {
                        $tr_val = preg_replace($field_matches, $field_replaces, $tr_val);
                        // treat special formatting cases:
                        if ($tr_fmt_arr[$ii] == "%HH:MM:SS") { // we do hack by changing params
                            $params['time_form'] = 0;
                            $tr_val = common::fmt_seconds($tr_val);
                        } elseif ($tdformat == "%MM:SS") {
                            $params['time_form'] = 1;
                            $tr_val = common::fmt_seconds($tr_val);
                        } else {
                            $tr_val = sprintf($tr_fmt_arr[$ii],$tr_val);
                        }
                        $tdata[$rows][$tr_k] = $tr_val;
                        $ii++;
                    }
                    break;
                case 'ENDTABLE': // end of table definition, display it
//print_r($tcols);
//print_r($tdata);
//print_r($colnames[$objid]);
//print_r($opt[$objid]);
//print_r($ccols[$objid]);
                    $pdf->ezTable($tdata, $colnames[$objid], $attr[$objid]['title'], $opt[$objid]);
                    $colnum=0;
                    // once table is printed, we destroy tdata array
                    $tdata = array();
                    break;
                case 'FONT': // font change
                    $fontface = $attr['face'];
                    $newfont = $path_to_fonts."/".$fontface;
                    if ($attr['size']) { //set size if defined
                        $fontsize = $attr['size'];
                    }
                    $pdf->selectFont($newfont,array('differences'=>$euro_diff));
                    break;
                case 'LOGO':
                    $site = settings::get_value('site', 'MISC');
                    $url = common::base_url()."/img.php?t=logo_tl&q=100";
                    $pdf->ezImage($url, $attr['padding'], $attr['width'], $attr['resize'], $attr['justification']);
                    break;
                case 'IMAGE': // insert image
                    $pdf->ezImage($attr['src'], $attr['padding'], $attr['width'], $attr['resize'], $attr['justification']);
                    break;
                case 'COLUMNS': // 2 column layout start
                    $pdf->ezColumnsStart($attr['num'], $attr['gap']);
                    break;
                case 'ENDCOLUMNS': // 2 column layout stop
                    $pdf->ezColumnsStop();
                    break;
                /*	case '#C':
                        $pdf->selectFont($codeFont);
                        $textOptions = array('justification'=>'left','left'=>20,'right'=>20);
                        $size=10;
                        break;
                      case '#c':
                        $pdf->selectFont($mainFont);
                        $textOptions = array('justification'=>'full');
                        $size=12;
                        break;
                      case '#X':
                        $collecting=1;
                        break;
                      case '#x':
                        $pdf->saveState();
                        eval($code);
                        $pdf->restoreState();
                        $pdf->selectFont($mainFont);
                        $code='';
                        $collecting=0;
                        break;*/
            }
        } elseif ($collecting){
            $code.=$line;
        } else if (((strlen($line)>1 && $line[1]=='<') ) && $line[strlen($line)-1]=='>') {
            // then this is a title
            switch($line[0]){
                case '1': // level 1
                    $tmp = substr($line,2,strlen($line)-3);
                    //$tmp2 = $tmp.'<C:rf:1'.rawurlencode($tmp).'>';
                    $tmp2 = $tmp; // urls dont work, sorry
                    $tmp2 = preg_replace($field_matches, $field_replaces, $tmp2);
                    $pdf -> ezText($tmp2,26,array('justification'=>'centre'));
                    break;
                default: // level 2 and others
                    $tmp = substr($line,2,strlen($line)-3);
                    // add a grey bar, highlighting the change
                    //$tmp2 = $tmp.'<C:rf:2'.rawurlencode($tmp).'>';
                    $tmp2 = $tmp; // urls dont work, sorry
                    $tmp2 = preg_replace($field_matches, $field_replaces, $tmp2);
                    $pdf->transaction('start');
                    $ok=0;
                    while (!$ok){
                        $thisPageNum = $pdf->ezPageCount;
                        $pdf->saveState();
                        $pdf->setColor(0.9,0.9,0.9);
                        $pdf->filledRectangle($pdf->ez['leftMargin'],$pdf->y-$pdf->getFontHeight(18)+$pdf->getFontDecender(18),$pdf->ez['pageWidth']-$pdf->ez['leftMargin']-$pdf->ez['rightMargin'],$pdf->getFontHeight(18));
                        $pdf->restoreState();
                        $pdf->ezText($tmp2,18,array('justification'=>'left'));
                        if ($pdf->ezPageCount==$thisPageNum){
                            $pdf->transaction('commit');
                            $ok=1;
                        } else { // then we have moved onto a new page, bad bad, as the background colour will be on the old one
                            $pdf->transaction('rewind');
                            $pdf->ezNewPage();
                        }
                    }
                    break;
            }
        } else {
            // it is just a text, but see if there are some fields to replace
            $line = preg_replace($field_matches, $field_replaces, $line);
            $pdf->ezText($line,$fontsize,$textOptions);
        }
    }
    if ($debug) {
        $pdfcode = $pdf->ezOutput(1);
        $pdfcode = str_replace("\n","\n<br>",htmlspecialchars($pdfcode));
        echo '<html><body>';
        echo trim($pdfcode);
        echo '</body></html>';
    } else {
        if (!$debug) { // output to browser
            if ($outmode == "browser") {
                header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename='".$filename."'");
                $pdf->ezStream(array("Content-Disposition" => $filename));
            } elseif ($outmode == "stream") {
                return array($pdf->ezOutput(1), $details);
            } else {
                common::ndebug(2,"pdf::pdf_invoice_parse: invalid outmode: %s", $outmode);
                return false;
            }
        } else { // return as string
            $pdfcode = $pdf->ezOutput(1);
            return array($pdfcode, $details); // string and also details array because we have modified it
        }
    }
}
