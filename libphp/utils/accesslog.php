<?php

require_once("$libpath/dbstructs/accesslog.php");

class Accesslog {

// functions to work with access log

static function append ($type, $mode, $msg, $user) 
{
	global $g_user, $db;

	$area = settings::get_value('area', 'MISC');	
	$ipaddr = common::get_ip();
	
	$accesslog = New AccessLog();	
	$query = array(
		'type' => $type,
		'user' => $user,
		'area' => $area,
		'ip_address' => $ipaddr,
		'mode' => $mode,
		'msg' => $msg,
		'log_time' => 'now()'
		);
	$accesslog->insert($query);
	return true;
}

}

?>
