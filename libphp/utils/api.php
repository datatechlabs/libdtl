<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 02/02/2016
 * Time: 15:46
 */

$config_file = apache_getenv('DTL_CONFIG_FILE');
require_once("../../libdtl/libphp/config.php");

settings::init($config_file);
$libdtl_root = settings::get_value("libdtl_root", "PATHS");
$libpath = "$libdtl_root/libphp";

require_once("$libpath/system.php");
require_once("$libpath/site.php");
require_once("$libpath/dbstructs/apiclient.php");


system::init($config_file);

// --- Step 1: Initialize variables and functions

// Define API response codes and their related HTTP response
$api_response_code = array(
    0 => array('HTTP Response' => 200, 'Message' => 'Success'),
    1 => array('HTTP Response' => 400, 'Message' => 'Unknown Error'),
    2 => array('HTTP Response' => 403, 'Message' => 'HTTPS Required'),
    3 => array('HTTP Response' => 401, 'Message' => 'Authentication Required'),
    4 => array('HTTP Response' => 401, 'Message' => 'Authentication Failed'),
    5 => array('HTTP Response' => 404, 'Message' => 'Invalid Request'),
    6 => array('HTTP Response' => 400, 'Message' => 'Invalid Response Format'),
    7 => array('HTTP Response' => 404, 'Message' => 'Not Found'),
    8 => array('HTTP Response' => 500, 'Message' => 'Server Error')
);

/**
 * Deliver HTTP Response
 * @param string $format The desired HTTP response content type: [json, html, xml]
 * @param string $api_response The desired HTTP response data
 * @return void
 **/
function deliver_response($format, $api_response){

    // Define HTTP responses
    $http_response_code = array(
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found'
    );

    // Set HTTP Response
    header('HTTP/1.1 '.$api_response['status'].' '.$http_response_code[ $api_response['status'] ]);

    // Process different content types
    if( strcasecmp($format,'xml') == 0 ){

        // Set HTTP Response Content Type
        header('Content-Type: application/xml; charset=utf-8');

        // Format data into an XML response (This is only good at handling string data, not arrays)
        $xml_response = '<?xml version="1.0" encoding="UTF-8"?>'."\n".
            '<response>'."\n".
            "\t".'<code>'.$api_response['code'].'</code>'."\n".
            "\t".'<data>'.$api_response['data'].'</data>'."\n".
            '</response>';

        // Deliver formatted data
        echo $xml_response;

    } elseif( strcasecmp($format,'http') == 0 ){

        // Set HTTP Response Content Type (This is only good at handling string data, not arrays)
        header('Content-Type: text/html; charset=utf-8');

        // Deliver formatted data
        echo $api_response['data'];

    } else {

        // Set HTTP Response Content Type
        header('Content-Type: application/json; charset=utf-8');

        // Format data into a JSON response
        $json_response = json_encode($api_response);

        // Deliver formatted data
        echo $json_response;
    }

    // End script process
    exit;

}

function http_basic_auth($db_username, $db_password) {

    $username = null;
    $password = null;

    // mod_php
    if (isset($_SERVER['PHP_AUTH_USER'])) {
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];

        // most other servers
    } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {

        if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0)
            list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));

    }

    if ($db_username == $username && $db_password == $password)
        return true;
    return false;

}

function http_hmac_auth($db_username, $db_hmac_secret, $db_hmac_algo)
{

    if ($db_hmac_secret == "") {
        common::ndebug(4, "api::http_hmac_auth: HMAC secret not set in DB");
        return false;
    }

    $headers = getallheaders();

    if (!array_key_exists('Authorization', $headers)) {
        common::ndebug(4, "api::http_hmac_auth: Authorization header missing");
        return false;
    }

    if (!array_key_exists('Date', $headers)) {
        common::ndebug(4, "api::http_hmac_auth: Date header missing");
        return false;
    }

    $avp = explode(" ", $headers['Authorization']);
    if (count($avp) <> 2) {
        common::ndebug(4, "api::http_hmac_auth: Authorization header has no arguments");
        return false;
    }
    if ($avp[0] <> "hmac") {
        common::ndebug(4, "api::http_hmac_auth: No HMAC keyword");
        return false;
    }

    $parts = explode(":", $avp[1]);
    if (count($parts) <> 3) {
        common::ndebug(4, "api::http_hmac_auth: Authorization header has wrong number of arguments");
        return false;
    }

    if ($db_username <> $parts[0]) {
        common::ndebug(4, "api::http_hmac_auth: Wrong username");
        return false;
    }

    $nonce = $parts[1];
    $digest = base64_decode($parts[2]);
    $time = $headers['Date'];

    $tdiff = strtotime($time) - time();
    if ($tdiff > 7200 || $tdiff < -7200) {
        common::ndebug(4, "api::http_hmac_auth: Timestamp is out of range");
        return false;
    }

    $uri = $_SERVER['REQUEST_URI'];
    $method = $_SERVER['REQUEST_METHOD'];
    $data = $method."+".$uri."+".$time."+".$nonce;

    $calc_digest = hash_hmac("sha256", $data, $db_hmac_secret);
    if ($calc_digest != $digest) {
        common::ndebug(4, "api::http_hmac_auth: Digest mismatch");
        return false;
    }
    return true;
}



function reject($code, $response) {
    global $api_response_code, $format;
    $response['code'] = $code;
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
    $response['data'] = $api_response_code[ $response['code'] ]['Message'];

    // Return Response to browser. This will exit the script.
    deliver_response($format, $response);

}

function process()
{

    // Define whether an HTTPS connection is required
    $HTTPS_required = FALSE;



    // Set default HTTP response of 'ok'
    $response['code'] = 1;
    $response['status'] = 400;
    $response['data'] = NULL;

    // set the format
    $format = Input::get('format');

    if (strcasecmp($format, 'xml') == 0) {
        $format = 'xml';
    } elseif (strcasecmp($format, 'http') == 0) {
        $format = 'http';
    } else {
        $format = 'json';
    }


    // --- Step 2: Authorization

    // Optionally require connections to be made via HTTPS
    if ($HTTPS_required && $_SERVER['HTTPS'] != 'on') {
        reject(2, $response);
    }

    // Get user by hash
    $client_hash = Input::get('client_hash');
    $apiclient = New Apiclient();

    if (!$apiclient->findOne(array('client_hash' => $client_hash))) {
        reject(7, $response);
    }


    // --- Step 3: Process Auth
    if ($apiclient->auth_method == 'BASIC') {
        // go forward with HTTP BASIC auth as only method for now
        if (!http_basic_auth($apiclient->auth_username, $apiclient->auth_password)) {
            reject(4, $response);
        }
    } elseif ($apiclient->auth_method == 'HMAC') {
        if (!http_hmac_auth($apiclient->auth_username, $apiclient->hmac_secret, $apiclient->hmac_algo)) {
            reject(4, $response);
        }
    } elseif ($apiclient->auth_method == 'IPADDR') {
        //XXX CHECK IP!
    } else {
        reject(8, $response);
    }

    if (strcasecmp($format, 'xml') == 0) {
        $xmlstr = file_get_contents('php://input');
        $res = new SimpleXMLElement($xmlstr);
    } elseif (strcasecmp($format, 'http') == 0) {
        $res = Input::all();
    } else {
        $json = file_get_contents('php://input');
        $res = json_decode($json, true);
    }

    $module = Input::get('module');

    list($result, $success, $fail) = call_user_func_array($module, array($res, $apiclient));

    if (!$result)
        reject($fail, $response);
    else
        deliver_response($format, $success);


    exit(0);
    // --- Step 4: Process Request



    if (strcasecmp($_GET['module'], 'call') == 0) {


        // Method A: filelist - an URL with the reference to the playlist file
        if (strcasecmp($_GET['method'], 'remotelist') == 0) {
            // The URL of the file list
            require_once("$package_root/api/call/remotelist.php");
            execute($response, $client);
        }


        // Method B: prerecord - prerecorded messages
        if (strcasecmp($_GET['method'], 'prerecorded') == 0) {
            require_once("$package_root/api/call/prerecorded.php");
            execute($response, $client);

        }

        // Method C: filelist - an file list URL with the reference to the playlist file
        if (strcasecmp($_GET['method'], 'directlist') == 0) {
            // The URL of the file list
            require_once("$package_root/api/call/directlist.php");
            execute($response, $client);


        }

    }
    // --- Step 5: Deliver Response

    // Return Response to browser

    reject(5, $response);
}

function install_handler($params) {

}

?>
