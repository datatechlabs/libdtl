<?php

class Image {
    static function  resize($file, $width, $height, $crop=FALSE) {
        list($w, $h) = getimagesize($file);

        // determine aspect ratio of the image
        $dst_asp = $width / $height;
        $src_asp = $w / $h;

        if ($crop) { // crop
            $conv_ratio = max($width / $w, $height / $h);
            if ($dst_asp < $src_asp) { // original is wide
                $src_y = 0;
                $src_x = ($w - ($width / $conv_ratio)) /2 ;
            } else { // original is tall
                $src_y = ($h - ($height / $conv_ratio)) / 2;
                $src_x = 0;
            }
            $dst_h = $height / $conv_ratio;
            $dst_w = $width / $conv_ratio;
        } else { // TODO: check if works as expected
            $conv_ratio = min($width/$w, $height/$h);
            $width = $w * $conv_ratio;
            $height = $h * $conv_ratio;
            $src_x = 0;
            $src_y = 0;
            $dst_h = $h;
            $dst_w = $w;
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($width, $height);
        imagecopyresampled($dst, $src, 0, 0, $src_x, $src_y, $width, $height, $dst_w, $dst_h);
        return $dst;
    }

    static function  resize_from_string($imgdata, $width, $height, $crop=FALSE) {

        $src = imagecreatefromstring($imgdata);
        $w = imagesx($src);
        $h = imagesy($src);

        // determine aspect ratio of the image
        $dst_asp = $width / $height;
        $src_asp = $w / $h;

        if ($crop) {
            $conv_ratio = max($width / $w, $height / $h);
            if ($dst_asp < $src_asp) { // original is wide
                $src_y = 0;
                $src_x = ($w - ($width / $conv_ratio)) /2 ;
            } else { // original is tall
                $src_y = ($h - ($height / $conv_ratio)) / 2;
                $src_x = 0;
            }
            $dst_h = $height / $conv_ratio;
            $dst_w = $width / $conv_ratio;

        } else {
            $conv_ratio = min($width/$w, $height/$h);
            $width = $w * $conv_ratio;
            $height = $h * $conv_ratio;
            $src_x = 0;
            $src_y = 0;
            $dst_h = $h;
            $dst_w = $w;
        }
     //   $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate ($dst, 255, 255, 255);
        imagefill ($dst, 0, 0, $white);
        imagecopyresampled($dst, $src, 0, 0, $src_x, $src_y, $width, $height, $dst_w, $dst_h);
        return $dst;
    }

}