<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 03/02/2016
 * Time: 15:33
 */

require_once ($libdtl_root."/libphp/dbstructs/templatefield.php");

class Upload {

    static public function to_csv_handle($page_num=1)
    {
        // multipgae supported only on XLS for now
        $package_root = settings::get_value('package_root', 'PATHS');
        $filename = $_FILES['userfile']['tmp_name'];
        $filetype = upload::detect_filetype();
        common::ndebug(4,"rate upload filetype %s", $filetype);

        switch ($filetype) {
            case "xls":
                // call MS excel parser and wait for the result
                common::ndebug(4,"rate upload xls format, page %s", $page_num);
                $execstr = $package_root."/scripts/parse_xls.pl ".$filename." ".$page_num;
                $output = array();
                exec($execstr, $output, $return_var);
                // place perl results in temporary file
                $tmpfname = tempnam ("", "");
                $thandle = fopen($tmpfname, "a");
                foreach ($output as $orow)
                    fwrite($thandle, $orow."\n");
                fclose($thandle);
                // open for read
                $handle = fopen($tmpfname, 'r');
                return $handle;
            case "xlsx":
                common::ndebug(4,"rate upload xlsx format");
                $execstr = $package_root."/scripts/parse_xlsx.pl ".$filename;
                $output = array();
                exec($execstr, $output, $return_var);
                // place perl results in temporary file
                $tmpfname = tempnam ("", "");
                $thandle = fopen($tmpfname, "a");
                foreach ($output as $orow)
                    fwrite($thandle, $orow."\n");
                fclose($thandle);
                // open for read
                $handle = fopen($tmpfname, 'r');
                return $handle;
            default:
                // treat it as csv file
                common::ndebug(4,"rate upload csv format");
                $handle = fopen($filename, 'r');
                return $handle;
        }
    }

    static public function detect_filetype()
    {
        $mimetype = $_FILES['userfile']['type']; // MIME
        $extension = pathinfo($_FILES['userfile']['name']); // extension
        common::ndebug(4,"rate upload mimetype %s", $mimetype);
        common::ndebug(4,"rate upload extension %s", $extension['extension']);

        if ( $mimetype == "application/vnd.ms-excel" ) {
            if($extension['extension'] == 'csv')
                return "csv";
            return "xls";
        }
        if ( $mimetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" )
            return "xlsx";

        return "csv";
    }

    static public function guess_delimiter($handle)
    {
        // try to guess line delimiter
        $contents = fread($handle, (10 * 1024));
        rewind ($handle);
        $dlines = explode("\n", $contents);

        // remove last line of array, as this maybe incomplete?
        array_pop($dlines);

        // create a string from the legal lines
        $complete_lines = implode(' ', $dlines);

        // allowed field delimiters
        $delimiters = array(
            'comma'		=> ',',
            'semicolon'	=> ';',
            'tab'		=> "\t",
            'pipe'		=> '|',
            'colon'		=> ':'
        );
        // loop and count each delimiter instance
        foreach ($delimiters as $delimit) {
            $delimiter_result[$delimit] = substr_count($complete_lines, $delimit);
        }
        // sort by largest array value
        arsort($delimiter_result);
        reset($delimiter_result);
        $delimiter = key($delimiter_result);
        common::ndebug(4,"rate upload fields delimiter %s", $delimiter);
        return $delimiter;
    }

    static public function get_page_numbers($template_id)
    {
        if (!$template_id)
            return false;


        /* get all possible page numbers in this template */
        // TODO FIXME

  /*      $sql = "select ATRPAGE from TemplateFields
		where template_id=$template_id group by page";
        $data = db::fetch_all($sql); */
        $res = array();
        foreach ($data as $row) {
            $res[$row['page']] = $row['page'];
        }
        common::ndebug(4,"rate upload pages in template %s", $res);
        return $res;
    }

    static public function get_pattern($template_id, $page_num=1)
    {
        if (!$template_id)
            return false;
        /* Get format patterns. */

        $tmpfld = New Templatefield();
        $tmpfld->getMany(array('template_id' => $template_id));

        $pattern = array();
        foreach ($tmpfld->data as $key => $val) {
            $field_num = $val['order_number'];
            $pattern[$field_num]['regex'] = $val['pattern'];
            $pattern[$field_num]['def_value'] = $val['default_value'];
            $pattern[$field_num]['dest_field'] = $val['target_field'];
            $pattern[$field_num]['special'] = $val['special'];
        }
        return $pattern;
    }
}
?>