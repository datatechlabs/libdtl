<?php

/*
 * Checks whether a given string is of type $type.
 *	
 * For instance
 *
 *	validate('-5632', 'INT');
 *	
 * will succeed, whereas
 *	
 *	validate(')789n', 'INT');
 *	
 * shall fail.
 *
 * $vstr: the string that should be checked for validity
 * $type: the type our string is supposed to be
 * $req: is $vstr required to contain something?
 *	whenever this is set to false, empty strings will be accepted.
 *
 * returns: true if the validation succeeds, false otherwise.
 */
function validate($vstr, $type, $req = true)
{
	if (strlen($vstr) == 0)
		return !$req;
		
	// call validation function
	return VFunctions::$type($vstr);
}

/*
 * Contains validation functions for each supported type,
 * their names correspond to the type they validate.
 * Usually nothing more than a simple regular expression match.
 */
class VFunctions {
	
static function INT($vstr)
{
	return preg_match('/^-?\d+$/', $vstr);
}

static function UINT($vstr)
{
	return preg_match('/^\d+$/', $vstr);
}

static function UINT16($vstr)
{
	return preg_match('/^[0-9a-f]+$/', $vstr);
}

static function EMAIL($vstr)
{
	return preg_match('/^[^@ ]+@[^@ ]+$/', $vstr);
}

static function IP($vstr)
{
	$pat = '/^([1-9]\d{0,2})\.(0|[1-9]\d{0,2})\.(0|[1-9]\d{0,2})\.(0|[1-9]\d{0,2})(\/\d{1,2})?$/';
	if (!preg_match($pat, $vstr, $matches))
		return false;
		
	for ($i = 1; $i <= 4; $i++)	// make sure they're in a byte's range
		if ($matches[$i] < 0 || $matches[$i] > 255)
			return false;
	
	if (array_key_exists(5, $matches)) {
		$mask = substr($matches[5], 1);
		if ($mask < 8 || $mask > 32)
			return false;
	}
				
	return $matches[1] != 0;
}

static function STRING($vstr) { return true; }

static function BOOL($vstr)
{
	if ($vstr === 0 || $vstr === 1 || $vstr === '0' || $vstr === '1')
		return true;
		
	return false;
}

static function PHONE($vstr) { return true; }

static function DATE($vstr)
{
	return db::execute("select '$vstr'");
}

static function DATETIME($vstr)
{
	return db::execute("select '$vstr'::timestamp");
}

static function TIME($vstr)
{
	if (!preg_match('/^(\d\d):(\d\d):(\d\d)$/', $vstr, $match))
		return false;
		
	$hour = $match[1];
	$min = $match[2];
	$sec = $match[3];
	
	if ($hour < 0 || $hour > 23)
		return false;
	if ($min < 0 || $min > 59)
		return false;
	if ($sec < 0 || $sec > 59)
		return false;
	
	return true;
}

static function FLOAT($vstr)
{
	return preg_match('/^-?\d*(\.\d*)?$/', $vstr);
}

static function UFLOAT($vstr)
{
	return preg_match('/^\d*(\.\d*)?$/', $vstr);
}
	
}

?>
