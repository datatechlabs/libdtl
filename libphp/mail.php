<?php

/*
 * Class mail implements mail sending functionality in an object-oriented
 * fashion.
 */

require_once("$libpath/dbstructs/maillog.php");
require_once("$libpath/dbstructs/mailgroup.php");
require_once("$libpath/dbstructs/mailtext.php");
require_once("$libpath/restrequest.php");
require_once("$libpath/site.php");


class Mail {

	/* private */ var $message = '';	/* message content */
	/* private */ var $subject = '';	/* subject */
	/* private */ var $msgtype = '';	/* message type */
	/* private */ var $msgbin = false;	/* whether message is binary */
	
	/* private */ var $attach = array();	/* attachments */

	/* private */ var $sender = array();
	/* private */ var $recipients = array();
	/* private */ var $reply_to = '';
	
	/* private */ var $mailgroup = 'default';
	/* private */ var $delivery_time = 'PT0H0M';
	/* private */ var $params = array();
	
	/*
	 * Set message.
	 *
	 * $msg:	message content
	 * $type:	string that is passed as a MIME Content-Type
	 * 		header (e.g., 'text/html; charset=8859-1')
	 * $bin:	whether the content is binary (in which case
	 * 		it has to be base64 encoded and split into
	 * 		multiple lines).
	 *
	 * Always terminate lines with CRLF (as is required by RFC).
	 */
	function set_message($msg, $type = 'text/plain; charset=us-ascii',
	    $bin = false)
	{
		$this->msgtype = $type;
		$this->msgbin = $bin;
		
		// if not binary, things are very simple
		if (!$bin) {
			$this->message = $msg;
			return;
		}
		
		// encode message for transfer
		$this->message = chunk_split(base64_encode($msg));
	}
	
	/*
	 * Attach something to the message. Argument explanations: see
	 * set_message() function.
	 *
	 * This effectively makes the whole message a MIME multipart message.
	 */
	function attach($msg, $type, $bin = true)
	{
		$att = array();
		
		$att['type'] = $type;
		$att['bin'] = $bin;
	
		if (!$bin) {
			$att['msg'] = $msg;
			$this->attach[] = $att;
			return;
		}
		
		$att['msg'] = chunk_split(base64_encode($msg));		
		$this->attach[] = $att;
	}
	
	/*
	 * Almost the same as attach() with the difference
	 * that you can pass a filename path argument instead of the
	 * actual content.
	 */
	function attach_file($filename, $type, $bin = true)
	{
		if (($msg = file_get_contents($filename)) === false)
			return false;
		
		$this->attach($msg, $type, $bin);
		return true;
	}
	
	/*
	 * Set subject. Must be one line without any LF characters at the end.
	 */
	function set_subject($subj)
	{
		$this->subject = common::mime_header_encode(trim($subj));
	}

	/*
	 * Who sent this mail?
	 */
	function set_sender($email, $name = '')
	{
		if (!Mail::validate_email($email)) {
			common::ndebug(2,"mail::set_sender: sender email invalid %s, failure", $email);
			return false;
		}
		$this->sender = array('email' => $email, 'name' => $name);
		
		return true;
	}

	function set_reply_to($email)
	{
		if (!Mail::validate_email($email)) {
			common::ndebug(2,"mail::set_reply_to: sender email invalid %s, failure", $email);
			return false;
		}
		$this->reply_to = $email;

		return true;
	}
	
	/*
	 * Who is this mail addressed to?
	 */
	function add_recipient($email, $name = '')
	{
		if (!Mail::validate_email($email))
			return false;

		/* strip quotes and slashes from name */
		$name = str_replace('"', '', $name);
		$name = str_replace('\\', '', $name);
		$this->recipients[] = $name ? "\"$name\" <$email>" : "$email";
		$this->to = $email;
		return true;
	}
	
	/*
	 * Remove previously added recipients
	 */
	function clear_recipients()
	{
		$this->recipients = array();
	}
	
	/*
	 * Remove previously added attachments
	 */
	function clear_attachments()
	{
		$this->attach = array();
	}
	
	/*
	 * In order to keep logging organised, we must have some sort
	 * of grouping for the messages, so that later it would be possible
	 * to separate balance warning emails from invoice emails or
	 * something. If you do not specify a group, the mail is added
	 * to a 'default' group.
	 *
	 * This function accepts the group's name as its only argument. If such
	 * a group does not yet exist, it is created (not here though, only
	 * when performing the actual logging - after the mail has been sent).
	 */
	function set_group($group)
	{
		$this->mailgroup = $group;
	}


	/*
	 * Set Mass Mail Id. Avoids logging full message, instead provides reference to mass mail id.
	 */
	function set_mass_mail_id($id)
	{
		$this->mass_mail_id = $id;
	}

	function set_delivery_time($interval = 'PT0H0M') {
		$this->delivery_time = $interval;
	}

	/*
	 * Up up and away!
	 *
	 * returns false on failure, true if mail has been accepted for delivery.
	 */
	function send()
	{
		/* check if we have a sender and at least one recipient */
		$this->params = site::get_params();

		$secure_smtp['use'] = $this->params['secure_smtp_use'];
		if (!$this->sender) {
			common::ndebug(2,"mail::send failed, no sender",0);
			return false;
		}
			
		if (!$this->recipients) {
			common::ndebug(2,"mail::send failed, no recipients",0);
			return false;
		}
		
		// create recipient string
		$to = implode(', ', $this->recipients);
		
		// create sender string
		$from = $this->sender['name'] ?
			    "{$this->sender['name']} <{$this->sender['email']}>" :
			    "{$this->sender['email']}";
		if ($this->sender['name'] == $this->sender['email'])
			$from = $this->sender['email'];

		$reply_to = $this->reply_to;

		// some basic headers
		$headers =	"To: $to\r\n".
				"From: $from\r\n" .
				"Reply-To: $reply_to\r\n" .
				"Date: ".strftime("%a, %d %b %Y %T %z (%Z)")."\r\n" .
				"MIME-Version: 1.0\r\n";
				
		$msg = '';
		
		// check if there are any attachments
		if ($this->attach) {
			// compute some random message boundary
			$bound = md5(uniqid(rand()));
			
			// add a header saying that this is a multipart msg
			$headers .= "Content-Type: multipart/mixed; boundary=\"$bound\"\r\n";
			
			// append main message
			$msg .= "--$bound\r\n";
			$msg .= "Content-Type: {$this->msgtype}\r\n";
			
			if ($this->msgbin)
				$msg .= "Content-Transfer-Encoding: base64\r\n";
			
			$msg .= "\r\n" . $this->message . "\r\n";
			$msg .= "--$bound";
			
			// now append each attachment to the final message
			while (list(, $att) = each($this->attach)) {
				$msg .= "\r\nContent-Type: {$att['type']}\r\n";
				
				if ($att['bin'])
					$msg .= "Content-Transfer-Encoding: base64\r\n";
									
				$msg .= "\r\n" . $att['msg'] . "\r\n";
				$msg .= "--$bound";
			}
			$msg .= "--\r\n";
		} else {
			// append just the message
			$headers .= "Content-Type: {$this->msgtype}\r\n";
			
			if ($this->msgbin)
				$headers .= "Content-Transfer-Encoding: base64\r\n";
			
			$msg .= $this->message;
		}
		
		// add X-Mailer header, just for fun
		$headers .= "X-Mailer: LibDTL";
		
		/*
		 * We force mail to be sent as if from $sender.
		 * The system user that executes this PHP script must be added
		 * as a trusted user in sendmail's configuration file
		 *
		 * /etc/mail/submit.cf
		 *
		 * otherwise an X-Authentication-Warning header will be added
		 * to the message. Some lame mail servers/clients might
		 * therefore consider it as spam and not deliver it.
		 */

		if ($this->params['mail_use_mailgun']) {
			$status = $this->mailgun_send();
		} elseif (!$secure_smtp['use']) {
			$status =  mail($to, $this->subject, $msg, $headers,
				"-f{$this->sender['email']}");
			common::ndebug(4,'Mail::send using local mailer, return %s', $status); 
		} else { 
			$ret = Mail::authSendEmail($this->sender['email'], '', $this->to, '', $this->subject, $msg, $cc=array(), $bcc=array(), $headers, $reply_to);
			common::ndebug(4,'Mail::authSendEmail returned %s', $ret); 
			if ($ret)
				$status = true;
			else 
				$status = false;
		}

		// mass mail id
		if (isset($this->mass_mail_id))
			$mass_mail_id = $this->mass_mail_id;
		else
			$mass_mail_id = 0;

		Mail::log_mail($this->subject, $msg, $from, $to, $status, $mass_mail_id);
	
		return $status;
	}
		


	/* private static */
	function log_mail($subject, $message, $sender, $recipients, $status, $mass_mail_id)
	{
		global $tblnames;

		// better play it safe
		$subject = addslashes($subject);
		$message = addslashes($message);
		$sender = addslashes($sender);
		$recipients = addslashes($recipients);
		
		$groupname = addslashes($this->mailgroup);

		/* if mass mail id is specified, do not log the message body.
		 * instead just save reference to the mass mail id 
		 */
		$maillog = New MailLog();
		$mailgroup = New MailGroup();


		if ($mass_mail_id) {
			$data = array(
				'subject' =>$subject,
				'sender' => $sender,
				'recipients' => $recipients,
				'status' => $status,
				'mail_group_id' => 0,
				'mass_mail_id' => $mass_mail_id
				);
			$maillog->insert($data);
			return true;
		}

		// fetch mail group id
		// check if there was a mailgroup with that name.
		// if not, create it.
		if(!$mailgroup->findOne(array('name' => $groupname))) {
			$mailgroup->insert(array('name' => $this->mailgroup));
			$groupid = $mailgroup->last_id;
		} else {
			$groupid = $mailgroup->mail_group_id;
		}

	
		// log mail
		$newmail = array(
			'subject' => $subject,
			'sender' => $sender,
			'recipients' => $recipients,
			'status' => $status,
			'mail_group_id' => $groupid
		);

		$maillog->insert($newmail);
		$mail_id = $maillog->last_id;

		// log mail text
		$mailtext = New MailText();
		$mailtext->insert(array('mail_id' => $mail_id, 'text' => $message));

		return true;
	}


	/*
	 * Return true if $email looks like a valid e-mail address,
	 * false otherwise.
	 */
	function validate_email($email)
	{
		if (preg_match("/^[^@]+@[^@]+$/", $email))
			return true;
		
		return false;
	}
	
	/* * * * * * * * * * * * * * SEND EMAIL FUNCTIONS * * * * * * * * * * * * * */ 

// Send e-mail to smtp server using smtp auth
// Input: (string) sender's e-mail address; (string) sender's name;
//   (string) recipient's e-mail address, (string) recipient's name;
//   (string) subject;
//   (string) message body;
//   (indexed array) cc recipients. Key: e-mail address, value: recipient's name;
//   (array) bcc recipients. Value: e-mail address;
//   (array) additional headers. Don't include \r and/or \n.

function authSendEmail($from, $namefrom, $to, $nameto, $subject, $message, $cc=array(), $bcc=array(), $userHeaders, $replyto=false)
{
	$smtpServer = $this->params['secure_smtp_host'];
	$port = $this->params['secure_smtp_port'];
	$timeout = (isset($this->params['secure_smtp_timeout']) ? $this->params['secure_smtp_timeout'] : 30);
	$username = $this->params['secure_smtp_username'];
	$password = $this->params['secure_smtp_password'];
	$starttls = (isset($this->params['secure_smtp_starttls']) ? $this->params['secure_smtp_starttls'] : false);
	$newLine = "\r\n";
	// skip authentication if username or password is empty
	$smtp_auth = true;
	if (strlen(trim($username)) == 0 || strlen(trim($password)) == 0)
		$smtp_auth = false;
		
	//Connect to the host on the specified port
	if (!$timeout)
		$timeout = 30;
	if (!$port)
		$port = 25;
	$smtpConnect = fsockopen($smtpServer, $port, $errno, $errstr, $timeout);
	$resp0 = mail::get_lines($smtpConnect, 515);
	common::ndebug(4,"mail:connect response received %s", $resp0);
	if(empty($smtpConnect)) {
		$this->errmsg = "Failed to connect host: $smtpServer port: $port errno: $errno errstr: $errstr response: $smtpResponse";
		return false;
	}

	fwrite($smtpConnect, "EHLO $smtpServer" . $newLine);
	$resp1 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:EHLO response received %s", $resp1);
	$resp1arr = explode("\n", $resp1);

	if ($starttls) {
		foreach($resp1arr as $rr1) {
			if (trim($rr1) ==  '250-STARTTLS' || trim($rr1) ==  '250 STARTTLS') {
				common::ndebug(4,"mail:EHLO writing STARTTLS");
				fwrite($smtpConnect, "STARTTLS" . $newLine);
	
				$resp1a = mail::get_lines($smtpConnect);
				common::ndebug(4,"mail:STARTTLS response received %s", $resp1a);
	
				stream_socket_enable_crypto(
					$smtpConnect,
					true,
					STREAM_CRYPTO_METHOD_TLS_CLIENT);
			}
		}
	}

	if ($starttls) {
		fwrite($smtpConnect, "EHLO $smtpServer" . $newLine);
		$resp2 = mail::get_lines($smtpConnect);
		common::ndebug(4, "mail:EHLO response received %s", $resp2);

	}

	if ($smtp_auth) {
		//Say AUTH LOGIN to SMTP
		fwrite($smtpConnect, "AUTH LOGIN" . $newLine);
		$resp2 = mail::get_lines($smtpConnect);
		common::ndebug(4,"mail:AUTH LOGIN response received %s", $resp2);
		if (substr($resp2,0,3) != '334') {
			$this->errmsg = $resp2;
			return false;
		}
		
		//Send username
		fwrite($smtpConnect, base64_encode($username) . $newLine);
		$resp3 = mail::get_lines($smtpConnect);
		common::ndebug(4,"mail:username response received %s", $resp3);
		if (substr($resp2,0,3) != '334') {
			$this->errmsg = $resp3;
			return false;
		}
		
		//Send password
		fwrite($smtpConnect, base64_encode($password) . $newLine);
		$resp4 = mail::get_lines($smtpConnect);
		common::ndebug(4,"mail:password response received %s", $resp4);
		if (substr($resp4,0,3) != '235') {
			common::ndebug(4,"mail: error %s", $resp4);
			$this->errmsg = $resp4;
			return false;
		}
		
	} else {
		common::ndebug(4,"mail:skip AUTH LOGIN because username and / or password not supplied");
	}

	//Email From
	$smtpRequest = "MAIL FROM: $from" . $newLine;
	fwrite($smtpConnect, $smtpRequest);
	$resp5 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:FROM response received %s", $resp5);
	if (substr($resp5,0,3) != '250') {
		common::ndebug(4,"mail: error %s", $resp5);
		$this->errmsg = $resp5;
		return false;
	}
	
	//Email To
	fwrite($smtpConnect, "RCPT TO: $to" . $newLine);
	$resp6 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:TO response received %s", $resp6);
	if (substr($resp6,0,3) != '250') {
		common::ndebug(4,"mail: error %s", $resp6);
		$this->errmsg = $resp6;
		return false;
	}

	if ($cc) {
		foreach ($cc as $currentCcAddr => $currentCcName) {
			fwrite($smtpConnect, "RCPT TO: $currentCcAddr" . $newLine);
			$resp7 = mail::get_lines($smtpConnect);
			common::ndebug(4,"mail:cc response received %s", $resp7);
		}
	}
	if ($bcc) {
		foreach ($bcc as $currentBcc) {
			fwrite($smtpConnect, "RCPT TO: $currentBcc" . $newLine);
			$resp8 = mail::get_lines($smtpConnect);
			common::ndebug(4,"mail:bcc response received %s", $resp8);
		}
	}

	//The Email body
	fwrite($smtpConnect, "DATA" . $newLine);
	$resp9 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:DATA response received %s", $resp9);
	if (substr($resp9,0,3) != '354') {
		common::ndebug(4,"mail: error %s", $resp9);
		$this->errmsg = $resp9;
		return false;
	}

	$headers = $userHeaders;  

	fwrite($smtpConnect, "Subject: $subject\r\n$headers\r\n\r\n$message\r\n.\r\n");
	$resp10 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:Subject response received %s", $resp10);
	if (substr($resp10,0,3) != '250') {
		common::ndebug(4,"mail: error %s", $resp10);
		$this->errmsg = $resp10;
		return false;
	}

	// Say Bye to SMTP
	fwrite($smtpConnect,"QUIT" . $newLine); 
	$resp11 = mail::get_lines($smtpConnect);
	common::ndebug(4,"mail:QUIT response received %s", $resp11);
	if (substr($resp11,0,3) != '221') {
		common::ndebug(4,"mail: error %s", $resp11);
		$this->errmsg = $resp11;
		return false;
	}
	fclose($smtpConnect);
	return true;
}

	private function mailgun_send() {

		$api_key = $this->params['mailgun_api_key'];
		$domain = $this->params['mailgun_domain'];

		$apirequest = New RestRequest();
		$apirequest->set_base_url('https://api.mailgun.net/v3');
		$apirequest->set_headers(array('Authorization: Basic '.base64_encode("api:".$api_key)));

		$date = new DateTime();
		$date->add(new DateInterval($this->delivery_time));


		// create sender string
		$from = $this->sender['name'] ?
			"{$this->sender['name']} <{$this->sender['email']}>" :
			"{$this->sender['email']}";
		if ($this->sender['name'] == $this->sender['email'])
			$from = $this->sender['email'];

		// send
		$endpoint = $domain.'/messages';
		$data = array(
			'from' => $from,
			'to' => $this->to,
			'subject' => $this->subject,
			'text' => $this->message,
			'tag' => $this->mailgroup,
			'o:deliverytime' => $date->format(DATE_RFC2822)
		);
		$result = $apirequest->send($endpoint, 'POST', $data);
		common::ndebug(4,"Mailgun response %s", $result);
		return true;
	}


/*
 * get_lines()
 *
 * __internal_use_only__: read in as many lines as possible
 * either before eof or socket timeout occurs on the operation.
 * With SMTP we can tell if we have more lines to read if the
 * 4th character is '-' symbol. If it is a space then we don't
 * need to read anything else.
 */
function get_lines($smtpConnect) {
	$data = "";
	while($str = fgets($smtpConnect,515)) {
//		common::ndebug(4,"mail -> get_lines(): data was %s", $data);
//		common::ndebug(4,"mail -> get_lines(): str was %s", $str);
		$data .= $str;
//		common::ndebug(4,"mail -> get_lines(): data is %s", $data);
		# if the 4th character is a space then we are done reading
		# so just break the loop
		if(substr($str,3,1) == " ") { break; }
	}
	return $data;
}

}

?>
