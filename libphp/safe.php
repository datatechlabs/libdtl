<?php

/*
 * A few small routines to make sure the security and integrity of database
 * data remains intact.
 */

/*
 * Page access constants:
 * 	0 = no access
 * 	1 = user can view that but not modify its contents
 * 	2 = user can read and modify page contents
 */
define('PERM_NONE', 0);
define('PERM_READ', 1);
define('PERM_EDIT', 2);

class safe {

	/*
	 * If $type == "relative", determine a child reseller's (specified by
	 * $id) level in relation to the logged in user.
	 * If $type == "absolute", determine resellers absolute level in the
	 * hierarchy (i.e., in relation to root).
	 * A return value of zero means that reseller was not a child.
	 */
	function child_level($id, $type='relative')
	{
		global $tblnames, $g_user, $g_site;
		if ($type == 'relative') {
			$user_id = $g_user->reseller['ATRRESELLERID'];
		} else if ($type == 'absolute') {
			$user_id = 0;
		} else {
			echo 'Invalid type argument for child_level().';
			exit(1);
		}
		if ($id == 0) // for admin
			return 0;

		if (!$user_id) // XXX - is this always safe?
			$user_id = 0; 
		$level = 0;
		for (;;) {
			if ($id != NULL) {
				$sql = "select ATRPARENTID from {$tblnames['ENTRESELLERS']}
					where ATRRESELLERID = $id";
				$id = db::fetch_one_value($sql);
			}
			if ($id === 0)
				return 0;
			/* 10 levels should suffice.. */
			if (++$level == 10)
				return 0;
			if ($id == $user_id)
				return $level;
		}
	}

	/*
	* Is reseller with ID=$parent_id a (direct) parent of reseller with ID=$reseller_id?
	*/
	/*function is_parent($parent_id, $reseller_id)
	{
		global $tblnames;

		$sql = "select * from {$tblnames['ENTRESELLERS']}
			where
				ATRRESELLERID = $reseller_id and
				ATRPARENTID = $parent_id";
		if (!db::fetch_one($sql))
			return false;
		return true;
	}*/

	/*
	 * This routine checks whether product with ID number $ATRPRODUCTID
	 * belongs to reseller $ATRRESELLERID. Intended to be run on IDs
	 * retrieved from (untrusted) user input. If the product does not belong
	 * to specified reseller, script will simply exit() and output an error
	 * message.
	 */
	function product_id_check($ATRPRODUCTID, $ATRRESELLERID)
	{
		global $tblnames;

		if ($ATRPRODUCTID == 0)
			return;

		$sql = "select * from {$tblnames['ENTPRODUCTS']}
			where
				ATRPRODUCTID = $ATRPRODUCTID and
				ATRRESELLERID = $ATRRESELLERID";
		$product = db::fetch_one($sql);
		if (!$product) {
			echo 'Invalid product ID.';
			exit(1);
		}
	}

	/*
	 * This routine checks whether permission group with ID number
	 * $ATRPERMGROUPID belongs to reseller $ATRRESELLERID. Intended to be
	 * run on IDs retrieved from (untrusted) user input. If the product does
	 * not belong to specified reseller, script will simply exit() and
	 * output an error message.
	 */
	function permgroup_id_check($ATRPERMGROUPID, $ATRRESELLERID)
	{
		global $tblnames;

		if ($ATRPERMGROUPID == 0)
			return;
		
		$sql = "select * from {$tblnames['ENTPERMGROUPS']}
			where
				ATRPERMGROUPID = $ATRPERMGROUPID and
				ATRRESELLERID = $ATRRESELLERID";
		$permgroup = db::fetch_one($sql);
		if (!$permgroup) {
			echo 'Invalid permission group ID.';
			exit(1);
		}
	}

	/*
	 * This routine checks whether route group with ID number
	 * $ATRROUTINGGROUPID belongs to reseller $ATRRESELLERID. Intended to be
	 * run on IDs retrieved from (untrusted) user input. If the product does
	 * not belong to specified reseller, script will simply exit() and
	 * output an error message.
	 */
	function routegroup_id_check($ATRROUTINGGROUPID, $ATRRESELLERID)
	{
		global $tblnames;

		if ($ATRROUTINGGROUPID == 0)
			return;

		$sql = "select * from {$tblnames['ENTROUTINGGROUPS']}
			where
				ATRROUTINGGROUPID = $ATRROUTINGGROUPID and
				ATRRESELLERID = $ATRRESELLERID";
		$rg = db::fetch_one($sql);
		if (!$rg) {
			echo 'Invalid route group ID.';
			exit(1);
		}
	}

	/*
	 * This routine checks whether rate (tariff) with ID number
	 * $ATRRATEID belongs to reseller $ATRRESELLERID. Intended to be
	 * run on IDs retrieved from (untrusted) user input. If the rate does
	 * not belong to specified reseller, script will simply exit() and
	 * output an error message.
	 */
	function rate_id_check($ATRRATEID, $ATRRESELLERID)
	{
		global $tblnames;

		if ($ATRRATEID == 0)
			return;

		if ($ATRRESELLERID == 0)
			return;  // admin can see any rates

		$sql = "select * from {$tblnames['ENTRATE']}
			where
				ATRRATEID = $ATRRATEID and
				ATRRESELLERID = $ATRRESELLERID";
		$rate = db::fetch_one($sql);
		if (!$rate) {
			echo 'Invalid tariff ID.';
			exit(1);
		}
	}

	/*
	 * This routine checks whether rate item with ID number
	 * $ATRRTID belongs to reseller $ATRRESELLERID. Intended to be
	 * run on IDs retrieved from (untrusted) user input. If the item does
	 * not belong to specified reseller, function will return false
	 */
	function rate_item_id_check($ATRRTID, $ATRRESELLERID)
	{
		global $tblnames;

		if ($ATRRTID == 0)
			return false;

		$sql = "select ATRRESELLERID 
			from {$tblnames['ENTRATE']}
				inner join {$tblnames['ENTRATEITEMS']} using (ATRRATEID)
			where 
				ATRRTID = $ATRRTID and 
				ATRRESELLERID = $ATRRESELLERID";
		$rate = db::fetch_one($sql);
		if (!$rate)
			return false;
		return true;
	}
	/*
	 * This routine checks whether rate item with ID number
	 * $ATRRTID belongs to reseller $user_id. If the item does
	 * not belong to specified reseller, return  false
	 */
	
	function rate_item_owner_check($ATRRTID, $user_id)
	{
		global $tblnames;

		if ($ATRRTID == 0)
			return false;
	
		$sql = "select * from {$tblnames['ENTRATEITEMS']} i 
			left join {$tblnames['ENTDESTINATIONS']} d on d.ATRDESTINATIONID=i.ATRDESTINATIONID
			left join {$tblnames['ENTCODELISTS']} l on l.ATRCODELISTID=d.ATRCODELISTID 
		where i.ATRRTID = $ATRRTID
			and ATRRESELLERID = $user_id";
		$own = ($ownrow = db::fetch_one($sql)) ? (true): (false);
		return $own;
	}
	/*
	 * This routine checks whether dest code with ID number
	 * $ATRDESTCODEID belongs to reseller $user_id. If the item does
	 * not belong to specified reseller, return  false
	 */
	function dest_code_owner_check($ATRDESTCODEID, $user_id)
	{
		global $tblnames;

		if ($ATRDESTCODEID == 0)
			return false;
	
		$sql = "select * from {$tblnames['ENTDESTCODES']} c 
			left join {$tblnames['ENTDESTINATIONS']} d on d.ATRDESTINATIONID=c.ATRDESTINATIONID
			left join {$tblnames['ENTCODELISTS']} l on l.ATRCODELISTID=d.ATRCODELISTID 
		where c.ATRDESTCODEID = $ATRDESTCODEID
			and ATRRESELLERID = $user_id";
		$own = ($ownrow = db::fetch_one($sql)) ? (true): (false);
		return $own;
	}
	
	/*
	 * This routine checks whether account with ID number $account_id
	 * belongs to reseller $ATRRESELLERID. Intended to be run on IDs
	 * retrieved from (untrusted) user input. If the account does not belong
	 * to specified reseller, script will simply exit() and output an error
	 * message.
	 */
	function account_id_check($account_id, $ATRRESELLERID)
	{
		global $tblnames;

		if ($account_id == 0)
			return;

		$sql = "select * from {$tblnames['ENTVOIPACCT']}
			where
				ATRACCTID = $account_id and
				ATRRESID = $ATRRESELLERID";
		$product = db::fetch_one($sql);
		if (!$product) {
			echo 'Invalid Account ID.';
			exit(1);
		}
		return true;
	}

}

?>
