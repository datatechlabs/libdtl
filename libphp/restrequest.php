<?php
/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 27/02/2019
 * Time: 18:16
 */

class RestRequest {

    var $api_base_url;
    var $headers = array();

    public function set_base_url($api_base_url) {
        $this->api_base_url = $api_base_url;
    }

    public function set_headers($headers) {
        $this->headers = $headers;
    }

    public function send($endpoint, $method, $data) {
        $ch = curl_init();
        $poststring = '';
        $url = $this->api_base_url . '/' . $endpoint;
        $headers = $this->headers;

        if ($method == 'get') {
            $curl_post = false;
            foreach($data as $k => $v) {
                $url .= '&' . $k .'='. $v;
            }
        } else {
            $curl_post = true;
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            foreach($data as $k => $v) {
                $poststring .= '&' . $k .'='. $v;
            }
        }

        common::ndebug(4,"URL %s", $url);
        common::ndebug(4,"HEADERS %s", $headers);
        common::ndebug(4,"poststring %s", $poststring);

        $curlConfig = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => $curl_post,
            CURLOPT_POSTFIELDS     => $poststring,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER     => $headers
        );
        curl_setopt_array($ch, $curlConfig);

        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }


    public function is_posted($fields) {
        foreach($fields as $field) {
            if (!array_key_exists($field['name'], $_POST) && $field['required'])
                return false;
        }
        return true;
    }

    public function get_all($fields) {
        $ret = array();
        foreach ($fields as $field) {
            if (!array_key_exists($field['name'], $_POST))
                $ret = $_POST['name'];
        }
        return $ret;
    }
}