<?php

/**
 * Created by PhpStorm.
 * User: Aivis
 * Date: 22/01/2016
 * Time: 11:48
 */
class Input
{
    static function get($name) {
        if (array_key_exists($name, $_REQUEST)) {
            return trim($_REQUEST[$name]);
        }
    }
    static function all() {
        $data = array();
        foreach($_REQUEST as $key => $val) {
            $data[$key] = trim($val);
        }
        return $data;
    }
    static function has($name) {
        if (array_key_exists($name, $_REQUEST)) {
            return true;
        }
        return false;
    }
    static function file($filename) {
        if (array_key_exists($filename, $_FILES)) {
            return $_FILES[$filename];
        }
        return false;
    }
}