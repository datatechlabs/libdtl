<?php

require_once("$libpath/formfields.php");	// various field types
require_once("$libpath/valid.php");		// input validation

/*
 * Form class
 *	
 * Form is a collection of fields, it
 * has functionality for validating them, filling HTML templates with them,
 * creating PHP variables from posted form data.
 *	
 * Form file structure:
 *	
 * field name, field label, field type, field input type, required (0|1), default value
 * ...
 * ...
 */

class Form {

var $fields = array();	// array of FormField's
	
function Form()
{
}
	
/*
 * Read field information from a csv file
 * returns: false if some error occured, true otherwise.
 */
function read($filename)
{
	$fp = fopen($filename, 'r');
		
	if ($fp === false)
		return false;

	while ($line = fgetcsv($fp, 64)) {
		$classname = $line[3] . 'Field';

		$field = new $classname;
		
		// base class things
		$field->name = $line[0];
		$field->label = $line[1];
		$field->value_type = $line[2];
		$field->required = $line[4];
		
		$field->set_value($line[5]);
		
		$this->fields[$field->name] = $field;
	}

	return true;
}

/*
 * Validates each field according to its type;
 * In case validation fails on some field, its error
 * flag is set to true.
 *
 * returns: true if all fields are ok, false otherwise.
 */
function valid()
{
	$fine = true;
	
	reset($this->fields);
	foreach ($this->fields as $field) {
		$field = $this->fields[$fname];
		
		if (!validate($field->get_value(), $field->value_type,
			      $field->required)) {
			$this->fields[$field->name]->error = 1;
			$fine = false;
		}
	}
	
	return $fine;
}

/* Some functions for manipulating fields directly */

function set_value($fieldname, $value)
{
	$this->fields[$fieldname]->set_value($value);
}
	
function set_error($fieldname)
{
	$this->fields[$fieldname]->error = 1;
}

function &get_ref($fieldname)
{
	if (!array_key_exists($fieldname, $this->fields))
		return false;
	
	return $this->fields[$fieldname];
}

/*
 * Execute a field's member function
 */
function execmem($fieldname, $memfunc, $arguments, $multi_arg = 0)
{
	if (!$multi_arg)
		return $this->fields[$fieldname]->$memfunc($arguments);
	
	return $this->fields[$fieldname]->$memfunc(implode(', ', $arguments));
}

// create a field manually
function add_field($fname, $label, $valtype, $itype, $req, $val)
{
	$fieldclass = $itype . 'Field';
	$field = new $fieldclass;
	
	$field->name = $fname;
	$field->label = $label;
	$field->value_type = $valtype;
	$field->required = $req;
	
	$field->set_value($val);
	$this->fields[$fname] = $field;
}

// delete a field
function remove_field($fname)
{
	unset($this->fields[$fname]);
}
	
/*
 * Create an array where keys are form field names and values
 * are the values that have been input into the fields.
 *
 * Call extract() on the returned array to introduce them
 * as regular variables into local scope.
 */
function fetch_variables()
{
	$vars = '';
	
	reset($this->fields);
	foreach ($this->fields as $field)
		$vars[$field->name] = $field->get_value();
	//pg_escape_string($field->get_value());
	
	return $vars;
}

/*
 * Put posted values from $_POST array into fields.
 */
function set_posted_values()
{
	reset($this->fields);
	foreach ($this->fields as $field)
		$field->set_posted_value();

}

/*
 * Fill HTML template with field data
 */
function fill_template(&$templ)
{
	reset($this->fields);
	foreach ($this->fields as $field)
		$field->fill_template($templ);
}

/*
 * Check if every field has been submitted
 */
function is_submitted()
{
	reset($this->fields);

	foreach ($this->fields as $field) {
		if (!$field->is_posted())
			return false;
	}
	
	return true;
}

}

?>
