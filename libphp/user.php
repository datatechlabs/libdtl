<?php

common::require_class("webaccount_admin.php");
common::require_class("webaccount_user.php");
common::require_class("useraccount.php");

//require_once("$libpath/dbstructs/webaccount.php");
require_once("$libpath/dbstructs/picture.php");
require_once("$libpath/dbstructs/accesslog.php");
require_once("$libpath/dbstructs/site.php");

/*
 * User class holds information about the person browsing our site.
 */

class user {

var $webacc = false;

/*
 * Checks if some user corresponds to the provided login and password; also
 * creates a session.
 *
 * Returns true if login successful, false otherwise. May also exit() on fatal
 * errors.
 */
static function login($login, $passw)
{
	global $g_user;
	if(array_key_exists('language', $_POST))
		$language = $_POST['language'];
	else
		$language = 'en';

	/* Empty usernames and/or passwords are no good. */
	if (!$login || !$passw)
		return false;
	
	$query = array('web_login' => $login);
	$webacc = New WebAccount();
	$webacc->findOne($query);
	if (!$webacc)
		return false;

	/* See if password hashes match. */
	if (common::passw_hash($passw, $login) !== $webacc->web_passwd)
		return false;

	AdminUser::update_language($webacc, $language);
	settings::set_value($webacc->area, 'area', 'MISC');
	/* Create session. */
	if(!$sessid = sessions::create($webacc))
		return false;
	$g_user = new user;
	$g_user->webacc = $webacc;
	$accesslog = New AccessLog();
	$accesslog->append('sec', 'login', 'Successful Login', $g_user);
	
	/* return the redirect url of successful login, and the auth token */
	return array($webacc->area, $sessid);
}

/*
 * This routine fetches user's session from database and then all user account
 * data (including access permissions).
 *
 * Returns true if successful, false otherwise; may exit() on fatal errors.
 */
static function login_from_session()
{
	global $g_user;

	$g_user = new user;

	/* Get user's session data. */
	$session = sessions::session_by_id(sessions::submitted_sessid());
	if (!$session) {
		common::ndebug(4, "Cannot find valid session.");

		sessions::delete();
		echo 'Cannot find valid session.';
		exit(1);
	}
	$webacct_id = $session->user_id;
	settings::set_value($session->area, 'area', 'MISC');

	if(property_exists('session', 'site_id')) {
		$site = user::get_site_from_site_id($session->site_id);
		settings::set_value($site->site, 'site', 'MISC');
		settings::set_value($site->tblpref, 'tblpref', 'SQL');
	}
	/* get user */
	if ($session->area == 'admin') {
		$query = array('user_id' => $webacct_id);
		$webacc = New WebAccountAdmin();
		$webacc->findOne($query);
		if (!$webacc) {
			return false;
		}
		$g_user->account = new stdClass();
		$g_user->account->id = $webacct_id;
		$g_user->session = $session;
		$g_user->web_access = array('admin' => 2, 'no_perm_required' => 2);
		$g_user->webacc = $webacc;
		return true;
	} elseif ($session->area == 'user') {

		$query = array('id' => $webacct_id);
		$webacc = New WebAccountUser();
		
		$webacc->findOne($query);
		if (!$webacc) {
			return false;
		}
		// account - this parent user
		$account = New UserAccount();
		$account->findOne(array('user_id' => $webacc->client_id));
		if (!$account) {
			return false;
		}

		$g_user->account = $account;
		$g_user->session = $session;
		$g_user->webacc = $webacc;

		return true;
	} else {
		return false;
	}
}

static function get_site_from_site_id($site_id)
{
	$site = New HostedSite();
	$site->getById($site_id);
	return $site;
}


/* 
 * logins user from is trying to switch identity from root interface */
static function login_from_root($login, $key1, $key2, $site)
{
	global $g_user, $tblnames;
	$g_user = new user;

	$base_url = common::base_url();
	$area = settings::get_value('area', 'MISC');
	$site = settings::get_value('site', 'MISC');
	$system_type = settings::get_value('system_type', 'PATHS');
	
	/* its only possible for resellers and callshops currently */
	switch ($area) {
		case "reseller":
			$login_line = "%s/reseller.php?site=%s&user_login=%s&key1=%s";
		break;
		case "callshop":
			$login_line = "%s/callshop.php?site=%s&user_login=%s&key1=%s";
		break;
		default:
			common::ndebug(4, "login from root: invalid area", 0);
			return false;
	}

	$login_url = sprintf($login_line, $base_url, $site, $login, urlencode($key1));
	/* verify supplied hash */
	if ( sha1($login_url) !== $key2) {
		common::ndebug(4, "login from root: invalid hash", 0);
		return false;
	}

		/* Try to get web account by username. */
	$sql = "select ATRWEBACCTID, ATRWEBPASSW, ATRSHOPID, ATRRESELLERID
		from {$tblnames['ENTWEBACCT']}
		where ATRWEBLOGIN = '$login' and
		ATRSTATUS > 0";
	$webacc = db::fetch_one($sql);

	if ($area == "reseller" && $webacc['ATRRESELLERID'] == 0)  {
		common::ndebug(4, "login from root: invalid area (user is not reseller)", 0);
		return false;
	}

	if ($area == "callshop" && $webacc['ATRSHOPID'] == 0)  {
		common::ndebug(4, "login from root: invalid area (user is not reseller)", 0);
		return false;
	}
	if ($webacc) {
		/* Store web account data in session. */
		$user_id = $webacc['ATRWEBACCTID'];
		$id_column = 'ATRWEBACCTID';
	} else {
		common::ndebug(4, "login from root: user not found", 0);
		return false;
	}

	/* See if password hashes match. */
	if ($key1 !== $webacc['ATRWEBPASSW']) {
		common::ndebug(4, "login from root: invalid password", 0);
		return false;
	}
	/* Create session. */
	sessions::create($user_id, $id_column);

	$g_user = new user;
	$g_user->webacc = $webacc;
	append_accesslog('sec', 'login', 'Successful Login');
	return true;
}

/* update users language preference in case he / she chose it on login
 * add here all areas which support language setting 
 */
static function update_language($webacc, $language)
{
	global $db;
	if ($webacc->language == $language)
		return true;

	$newdata = array('language' => $language);
	$webacc->update($newdata);

	return true;
}

}

?>
