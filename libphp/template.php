<?php

if (file_exists("$libpath/dbstructs/dbtemplate.php"))
	require_once("$libpath/dbstructs/dbtemplate.php");

class Template {

var $assignments = array();
var $subitems = array();
var $filestr;
var $language;

	
// constructor
function __construct($file = "", $autopath = true, $custom_path = "")
{
	global $g_user;

	$this->assignments = array();
	$this->subitems = array();
	$langpath = '';
	if (isset($g_user)) {
		$langpath = 'i18n/'.$g_user->webacc->language;
		if (isset($g_user->webacc->language))
			$this->language = $g_user->webacc->language;
		else
			$this->language = 'en';
	} else {
		$this->language = 'en'; //should be configurable
	}

	$template_base = settings::get_simple_path('templates');
	$mod = settings::get_value('active_module','MISC');
	if ($mod && $file != 'admin/site.tpl') {
		// modules have their own template locations
		$modbase = settings::get_value('module_base', 'mod_'.$mod);
		$moddir = settings::get_value('module_template_path', 'mod_'.$mod);
		$template_base = $modbase."/".$moddir;
	}
	if ($file == "")
		return;

	// try customization from database
	if (list($filestr, $mimetype) = template::fetch_from_db($file)) {
		// escape double-quote characters and slashes
		$filestr = str_replace('\\', '\\\\', $filestr);
		$filestr = str_replace('"', '\\"', $filestr);
		$this->filestr = $filestr;
		$this->mimetype = $mimetype;
		return;
	}
	
	// try custom path - XXX: Fixme- allow arbitrary path
	if ($custom = settings::get_value('custom_template', "MISC")) {
		// default theme in case custom does not exist
		if (!$def_theme = settings::get_value('default_theme', "MISC"))
			$def_theme = 'bw';
		// in every attempt try language specific first
		if (is_file($template_base . "/$langpath/$custom/$file")) {
			$file = $template_base . "/$langpath/$custom/$file"; // lang , customized
		} elseif (is_file($template_base . "/layout/$custom/$file")) {
			$file = $template_base . "/layout/$custom/$file";  // no lang, customized
		} elseif (is_file($template_base . "/$langpath/$file")) { 
			$file = $template_base . "/$langpath/$file"; // lang, not customized
		} elseif (is_file($template_base . "/layout/$def_theme/$file")) {
			$file = $template_base . "/layout/$def_theme/$file"; // lang, customized, not found, use default
		} elseif (is_file($template_base . "/$file")) { 
			$file = $template_base . "/$file";  // no lang, no customized
		} else {
			return;
		}
		// escape double-quote characters and slashes
		$filestr = str_replace('\\', '\\\\', file_get_contents($file));	
		$filestr = str_replace('"', '\\"', $filestr);
		$this->filestr = $filestr;
		return;
	}

	if ($autopath)
		// try language specific first
		if (is_file($template_base . "/$custom_path/$file"))
			$file = $template_base . "/$custom_path/$file";
		else 
			$file = $template_base . "/$file";

	if (!is_file($file))
		return;
	// escape double-quote characters and slashes
	$filestr = str_replace('\\', '\\\\', file_get_contents($file));	
	$filestr = str_replace('"', '\\"', $filestr);

	$this->filestr = $filestr;
}
	
function clear()
{
	$this->assignments = array();
	$this->subitems = array();
}
	
function assign($name, $value)
{
	$this->assignments[$name] = $value;
}
		
function append($name, $value)
{		
	if (!array_key_exists($name, $this->assignments))
		$this->assignments[$name] = '';
		
	$this->assignments[$name] .= "$value\n";
}

/*
 * You can insert text that contains additional markers using
 * this function. But remember:
 *
 * DO NOT PASS USER INPUT TO THIS FUNCTION.
 */	
function insert($name, $value)
{
	$this->subitems[$name] = $value;
}

/*
 * Same as insert() but appends content. Heed the warning above!
 */
function insert_add($name, $value)
{
	$this->subitems[$name] .= $value . "\n";
}

function insert_file($name, $filename)
{

	global $g_user;
	// we need to know users language preference to pick correct template 
	$langpath = 'i18n/'.$g_user->webacc['language'];

	if (is_file(settings::get_simple_path('templates') ."/$langpath/$filename"))
		$file = settings::get_simple_path('templates') ."/$langpath/$filename";
	else 
		$file = settings::get_simple_path('templates') ."/$filename";

	$this->subitems[$name] = file_get_contents($file);
}

function fill($template)
{
	/* parse assignments */

	reset($this->assignments);
	foreach ($this->assignments as $key => $value) {
		// make sure values can not escape from the string
		$value = str_replace('\\', '\\\\', $value);
		$value = str_replace('\'', '\\\'', $value);
		    
		eval("\${$key} = '$value';");
	}

	/* parse subitems */
	
	$revsub = array_reverse($this->subitems, true);
	
	foreach ($revsub as $key => $value) {
		// make sure values can not escape from the string
		$value = str_replace('\\', '\\\\', $value);
		$value = str_replace('"', '\\"', $value);
		    
		eval("\${$key} = \"$value\";");
	}
	eval("\$parsed = \"$template\";");
	return $parsed;
}

function exec_callbacks($template)
{
	$matches = array();
	if(preg_match_all('/{\$FUNC:([A-Za-z0-9:]+)}/U', $template, $matches,  PREG_SET_ORDER )) {
		foreach ($matches as $match) {
			if (is_callable($match[1])) {
				$template = preg_replace('/{\$FUNC:'.$match[1].'}/', call_user_func($match[1]), $template);
			} else {
				$template = preg_replace('/{\$FUNC:'.$match[1].'}/', '', $template);
			}
		}
	}
	return $template;
}

function fetch($file = '', $autopath = true)
{
	if ($file == '') {
//		$this->filestr = $this->translate($this->filestr);
		$this->filestr = $this->exec_callbacks($this->filestr);
		return $this->fill($this->filestr);
	}

	// try customization from database
	if ($filestr = template::fetch_from_db($file)) {
		// escape double-quote characters and slashes
		$filestr = str_replace('\\', '\\\\', $filestr);
		$filestr = str_replace('"', '\\"', $filestr);
		// translate
		$this->filestr = $this->translate($this->filestr);
		$this->filestr = $this->exec_callbacks($this->filestr);
		return $this->fill($filestr);
	}

	// use global template path setting
	if ($autopath)
		$file = settings::get_simple_path('templates') . "/$file";

	// escape double-quote characters and slashes
	$filestr = str_replace('\\', '\\\\', file_get_contents($file));
	$filestr = str_replace('"', '\\"', $filestr);

	$this->filestr = $this->translate($this->filestr);
	$this->filestr = $this->exec_callbacks($this->filestr);
	return $this->fill($filestr);
}

function fetch_raw()
{
	// unescape double-quote characters and slashes
	$filestr = $this->filestr;
	$filestr = str_replace('\"', '"', $filestr);
	return $filestr;
}

function fetch_from_db($file)
{
	if (!class_exists("dbTemplate"))
		return false;
	
	$dbtemplate = New dbTemplate();
	if(!$dbtemplate->get_by_name($file))
		return false;
	return array($dbtemplate->body, $dbtemplate->mimetype);
}

}

?>
