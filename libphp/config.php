<?php

/*	
 * a class for reading configuration files
 */
class Config {
	
/*
 * $_settings Contains all the settings.
 * Keys are the same as setting keys in the config file
 * values are the arrays (variable => value) corresponding to keys
 * (again, same as in the file)
 */

var $_settings = array();

/*
 * this function is for reading php- style config files
 * should be replaced with common style later
 */
 
function dtl_parse_ini_file($filename)
{
	$file = file_get_contents($filename);
	//print $file;
	$ini_pattern = '/\n\s*\$([a-zA-Z0-9_]*)\s*\=\s*([^;.]*\n*.*)\;/';
	$section_pattern = '/\s*\_section_[0-9]+\s*/';
	$section_name = '';
	preg_match_all ($ini_pattern, $file, $match);
	$internal = $match[0]; // one dimension of array

	foreach ($internal as $setting_id => $setting_val) { // inner loop with actual keys and values

		$key = $match[1][$setting_id];
		$settings['PATHS'][$key] = trim($match[2][$setting_id], "\",'");

		// also create a 2-dim array for conf-compatible setting array
		if (preg_match($section_pattern, $key, $smatch)) {
			$section_name = trim($match[2][$setting_id], "\",'");
		} elseif ($section_name != '') {
			$settings[$section_name][$key] = trim($match[2][$setting_id], "\",'");
		}

	}
	$settings['MISC']['misc'] = "misc"; // dumb entry for misc section
	return $settings;
}
	
	
/*
 * reads a configuration file, returns false on failure
 */
function read_file($filename, $config_file_type='conf')
{
	if (!($fp = fopen($filename, 'r')))
		return false;

	// read configuration file
	if ($config_file_type == 'conf')
		$settings = parse_ini_file($filename, true);
	else
		$settings = config::dtl_parse_ini_file($filename);

	// compose the php code string
	foreach ($settings as $sectname => $section) {
		$declarations = '';
		foreach ($section as $key => $value) {
			if (substr($key ,0, 1) == "#")
				continue;
			eval("\$$key = \"$value\";\n");
			$this->_settings[$sectname][$key] = $$key;
		}
	}
	fclose($fp);
	return true;
}

/*
 * returns an array of variables corresponding to $skey, false on
 * failure
 */
function get_group($groupname)
{
	if ($this->_settings) {
		if (array_key_exists($groupname, $this->_settings)) {
			return $this->_settings[$groupname];
		} else {
			$this->set_group($groupname);
			return $this->_settings[$groupname];
		}
	}
	$this->set_group($groupname);
	return $this->_settings[$groupname];
}

function set_group($groupname)                                   
{
	$this->_settings[$groupname] = array();
}


}


/*
 * global settings class
 */
class settings {

var $conf;
var $paths = array();	/* store paths for fast retrieval */

/* read the global configuration file */
static function init($config_file, $config_file_type = 'conf')
{
	global $g_settings;
	if (!is_file($config_file)) {
		echo 'fatal error: configuration file not found';
		exit();
	}
	if (!$config_file_type)
		$config_file_type = 'conf';
	$g_settings = new settings;
	$g_settings->conf = new Config;
	if (!$g_settings->conf->read_file($config_file, $config_file_type)) {
		// not calling system::fatal_error() here because
		// that thing hasn't been loaded yet!
		echo 'fatal error: configuration file error';
		exit();
	}

	if (($g_settings->paths = $g_settings->conf->get_group('PATHS')) === false) {
		echo 'fatal error: no paths section';
		exit();
	}
	settings::set_value($config_file, 'config_file', 'MISC');
}
	
static function get_value($skey, $groupname, $default=false)
{
	global $g_settings;
	$group = $g_settings->conf->get_group($groupname);

	if (!array_key_exists($skey, $group))
		return $default;
	if (strtolower($group[$skey]) == "false")
		return false;
	if (strtolower($group[$skey]) == "no")
		return false;
	return $group[$skey];
}

static function get_list($skey, $groupname, $sep = ',', $trimchars = ' ')
{
	$value_list = settings::get_value($skey, $groupname);
	$values = array();
	foreach (explode($sep, $value_list) as $value)
		$values[] = trim($value, $trimchars);
	return $values;
}

static function set_value($val, $skey, $groupname)
{
	global $g_settings;
	$group = $g_settings->conf->get_group($groupname);
	$group[$skey] = $val;
	$g_settings->conf->_settings[$groupname] = $group;
}

/*
 * get a path name without any fancy browsing mode replacement
 */
static function get_simple_path($pathkey)
{
	global $g_settings;
	return $g_settings->paths[$pathkey];
}


}


?>